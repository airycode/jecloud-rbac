/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service;

import com.je.auth.check.RbacInterface;
import com.je.common.auth.AuthAccount;
import com.je.common.base.util.SecurityUserHolder;
import com.je.rbac.rpc.AccountRpcService;
import com.je.rbac.rpc.PermissionRpcService;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 自定义权限验证接口扩展
 *
 * @author Auster
 */
@Service
public class RbacInterfaceImpl implements RbacInterface {

    @Autowired
    private PermissionRpcService permissionRpcService;
    @Autowired
    private AccountRpcService accountRpcService;

    @Override
    public boolean isLogin() {
        return SecurityUserHolder.getCurrentAccount() != null;
    }

    @Override
    public String getLoginId() {
        return SecurityUserHolder.getCurrentAccountId();
    }

    @Override
    public String getTenantId() {
        return SecurityUserHolder.getCurrentAccountTenantId();
    }

    @Override
    public AuthAccount getLoginAccount() {
        return SecurityUserHolder.getCurrentAccount();
    }

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId,String tenantId) {
        return permissionRpcService.findAccountPermissionsWithLoginId(loginId.toString(),tenantId);
    }

    /**
     * 返回一个账号所拥有的角色标识集合
     */
    @Override
    public List<String> getRoleList(Object loginId,String tenantId) {
        return accountRpcService.findAccountRoleIds(loginId.toString(),tenantId);
    }

    @Override
    public List<String> getDeptList(Object loginId,String tenantId) {
        return accountRpcService.findAccountDeptIds(loginId.toString(),tenantId);
    }

    @Override
    public List<String> getOrgList(Object loginId) {
        return accountRpcService.findAccountOrgIds(loginId.toString());
    }

}
