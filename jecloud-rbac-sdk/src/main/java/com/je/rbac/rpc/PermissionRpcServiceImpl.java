/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PermissionRpcServiceImpl implements PermissionRpcService {

    @RpcReference(microserviceName = "rbac", schemaId = "permissionRpcService")
    private PermissionRpcService permissionRpcService;

    @Override
    public List<String> findAccountPermissionsWithLoginId(String loginId, String tenantId) {
        return permissionRpcService.findAccountPermissionsWithLoginId(loginId,tenantId);
    }

    @Override
    public List<String> findAccountPermissions(String departmentId,String accountId, String tenantId) {
        return permissionRpcService.findAccountPermissions(departmentId,accountId, tenantId);
    }

    @Override
    public List<String> findAccountPluginPermissions(String departmentId, String accountId, String tenantId, String pluginCode) {
        return permissionRpcService.findAccountPluginPermissions(departmentId,accountId,tenantId,pluginCode);
    }

    @Override
    public List<String> findAccountFuncPermissions(String departmentId,String accountId, String tenantId, String funcCode) {
        return permissionRpcService.findAccountFuncPermissions(departmentId,accountId, tenantId, funcCode);
    }

    @Override
    public List<String> findAccountMenuPermissions(String departmentId,String accountId, String tenantId) {
        return permissionRpcService.findAccountMenuPermissions(departmentId,accountId, tenantId);
    }

    @Override
    public List<String> findAccountTopMenuPermissions(String departmentId,String accountId, String tenantId) {
        return permissionRpcService.findAccountTopMenuPermissions(departmentId,accountId, tenantId);
    }

    @Override
    public void quickGrantMenu(String productId, String menuId) {
        permissionRpcService.quickGrantMenu(productId, menuId);
    }

    @Override
    public void quickGranFunc(String productId, String funcCode) {
        permissionRpcService.quickGranFunc(productId, funcCode);
    }

    @Override
    public void quickGranSubFunc(String productId, String subFuncRelationId) {
        permissionRpcService.quickGranSubFunc(productId, subFuncRelationId);
    }

    @Override
    public void quickGranPlugin(String productId, String pluginCode) {
        permissionRpcService.quickGranPlugin(productId, pluginCode);
    }

    @Override
    public void quickGrantFuncButton(String productId, Map<String, List<String>> funcButtonMap) {
        permissionRpcService.quickGrantFuncButton(productId, funcButtonMap);
    }

    @Override
    public void clearRoleAccountPermissions(String tenantId, List<String> roleIdList) {
        permissionRpcService.clearRoleAccountPermissions(tenantId, roleIdList);
    }

    @Override
    public void clearDeptAccountPermissions(String tenantId, List<String> deptIdList) {
        permissionRpcService.clearDeptAccountPermissions(tenantId, deptIdList);
    }

    @Override
    public void clearOrgAccountPermissions(String tenantId, List<String> orgIdList) {
        permissionRpcService.clearOrgAccountPermissions(tenantId, orgIdList);
    }

    @Override
    public void clearAccountPermissions(String tenantId, List<String> acountIdList) {
        permissionRpcService.clearAccountPermissions(tenantId, acountIdList);
    }

    @Override
    public void clearPermGroupAccountPermissions(String tenantId, List<String> permGroupIdList) {
        permissionRpcService.clearPermGroupAccountPermissions(tenantId, permGroupIdList);
    }

    @Override
    public void transferFuncPermission(String originalFuncCode, String targetFuncCode) {
        permissionRpcService.transferFuncPermission(originalFuncCode, targetFuncCode);
    }

    @Override
    public void modifyFuncButtonPermCode(String funcCode, String oldButtonCode, String newButtonCode) {
        permissionRpcService.modifyFuncButtonPermCode(funcCode, oldButtonCode, newButtonCode);
    }

    @Override
    public void modifySubFuncPermCode(String funcCode, String oldSubFuncCode, String newSubFuncCode) {
        permissionRpcService.modifySubFuncPermCode(funcCode, oldSubFuncCode, newSubFuncCode);
    }

}
