/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.je.common.base.DynaBean;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.exception.AccountException;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AccountRpcServiceImpl implements AccountRpcService {

    @RpcReference(microserviceName = "rbac", schemaId = "accountRpcService")
    private AccountRpcService accountRpcService;

    @Override
    public String getAddedOrDeleteIds(String type, String originalIds, String currentIds) {
        return accountRpcService.getAddedOrDeleteIds(type, originalIds, currentIds);
    }

    @Override
    public DynaBean findAccountById(String id) {
        return accountRpcService.findAccountById(id);
    }

    @Override
    public DynaBean findAccountByPhone(String account) {
        return accountRpcService.findAccountByPhone(account);
    }

    @Override
    public DynaBean findAccountByType(String type, String account) {
        return accountRpcService.findAccountByType(type, account);
    }

    @Override
    public List<DynaBean> findAllTenantAccount(String type, String account) {
        return accountRpcService.findAllTenantAccount(type, account);
    }

    @Override
    public DynaBean findTenantAccount(String type, String tenantId, String account) {
        return accountRpcService.findTenantAccount(type,tenantId,account);
    }

    @Override
    public DynaBean findLoginAccountByType(String type, String account) throws AccountException {
        return accountRpcService.findLoginAccountByType(type, account);
    }

    @Override
    public List<DynaBean> findAccountByAssociationId(String associationId) {
        return accountRpcService.findAccountByAssociationId(associationId);
    }

    @Override
    public List<DynaBean> findAccountByCompanyId(String companyId) {
        return accountRpcService.findAccountByCompanyId(companyId);
    }

    @Override
    public boolean isExpire(String id) throws AccountException {
        return accountRpcService.isExpire(id);
    }

    @Override
    public boolean isLocked(String id) throws AccountException {
        return accountRpcService.isLocked(id);
    }

    @Override
    public boolean checkDisable(String accountId) {
        return accountRpcService.checkDisable(accountId);
    }

    @Override
    public List<String> findAccountRoleIds(String tenantId, String accountId) {
        return accountRpcService.findAccountRoleIds(tenantId, accountId);
    }

    @Override
    public List<String> findAccountDeptIds(String tenantId, String accountId) {
        return accountRpcService.findAccountDeptIds(tenantId, accountId);
    }

    @Override
    public List<String> findAccountOrgIds(String accountId) {
        return accountRpcService.findAccountOrgIds(accountId);
    }

    @Override
    public List<DynaBean> findBatchAccount(String tableCode, String ids, String column) {
        return accountRpcService.findBatchAccount(tableCode, ids, column);
    }

    @Override
    public JSONTreeNode getAccountDeptTreeNode(List<Map<String, Object>> deptMapList) {
        return accountRpcService.getAccountDeptTreeNode(deptMapList);
    }

}
