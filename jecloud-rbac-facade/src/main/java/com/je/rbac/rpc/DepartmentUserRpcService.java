/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import java.util.List;

public interface DepartmentUserRpcService {

    /**
     * 查找人员直接领导
     * @param departmentId
     * @param userId
     * @return
     */
    String findDirectLeaderId(String departmentId, String userId);

    /**
     * 查找直接下属ID集合
     * @param departmentId
     * @param userId
     * @return
     */
    List<String> findDirectUserIds(String departmentId,String userId);

    /**
     * 查找用户主管的部门ID集合
     * @param departmentId
     * @return
     */
    List<String> findDeptLeaderIds(String departmentId);

    /**
     * 查找用户主管的公司ID集合
     * @param companyId
     * @return
     */
    List<String> findCompanyLeaderIds(String companyId);

    /**
     * 查找监管的部门ID集合
     * @param departmentId
     * @return
     */
    List<String> findMonitorDeptIds(String departmentId,String userId);

    /**
     * 查找部门监管部门
     * @param departmentId
     * @return
     */
    List<String> findDeptMonitorDeptIds(String departmentId);

    /**
     * 查找公司监管公司ID
     * @param companyId
     * @return
     */
    List<String> findCompanyMonitorCompanyIds(String companyId);

}
