/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.model.syncdata;

import java.io.Serializable;
import java.util.List;

/** 同步用户对象 */
public class SyncUser implements Serializable {

    /** 必填项,用户数据Id */
    private String userId;
    /** 必填项， 姓名 */
    private String userName;
    /** 必填项， 账号 */
    private String userCode;

    /** 必填项， 所属公司_ID */
    private String syCompanyId;
    /** 必填项， 所属公司_NAME */
    private String syCompanyName;
    /** 必填项， 所属公司_CODE */
    private String syCompanyCode;
    /** 必填项， 所属集团公司_ID */
    private String syGroupCompanyId;
    /** 必填项， 所属集团公司_NAME */
    private String syGroupCompanyName;

    /** 必填项， 所属部门_ID */
    private String departmentId;
    /** 必填项， 所属部门_NAME */
    private String departmentName;

    /** 必填项， 是否主管  是：1，否：0 */
    private String deptuserSfzgCode;
    /**  行政职务_CODE */
    private String userPostCode;
    /**  行政职务_NAME */
    private String userPostName;
    /**  直属领导_ID */
    private String deptuserDirectleaderId;
    /**  直属领导_NAME */
    private String deptuserDirectleaderName;
    /** 必填项， 手机号 */
    private String userPhone;
    /** 必填项， 公司邮箱 */
    private String userMail;
    /**  座机 */
    private String userTelephone;
    /**  工位 */
    private String userStation;
    /** 必填项， 入职日期 */
    private String userEntryDate;
    /**  离职日期 */
    private String userLeaveDate;
    /**  工号 */
    private String userJobnum;

    /**  监管部门_Id */
    private String userMonitordeptId;
    /**  监管部门_Name */
    private String userMonitordeptName;

    /** 个人信息 --------*/

    /**  头像 */
    private String userAvatar;
    /**  手写签名 */
    private String userPhoto;
    /**  性别 男：MAN 女：WOMAN */
    private String userSexCode;
    /**  国籍_CODE */
    private String userNationalityCode;
    /**  国籍_NAME */
    private String userNationalityName;
    /**  出生日期 */
    private String userBirth;
    /**  年龄 */
    private String userAge;
    /**  籍贯 */
    private String userNativePlace;

    /**  现居住地址 */
    private String userNowaddress;
    /**  户籍性质_CODE */
    private String userHouseholdCode;
    /**  户籍性质_NAME */
    private String userHouseholdName;
    /**  政治面貌_CODE */
    private String userPoliticalCode;
    /**  政治面貌_NAME */
    private String userPoliticalName;
    /**  婚姻状况 */
    private String userMarriedCode;
    /**  身份证号 */
    private String userCardnum;
    /**  民族_CODE */
    private String userNationCode;
    /**  民族_NAME */
    private String userNationName;
    /**  文化程度_ID */
    private String userEducationCode;
    /**  文化程度_NAME */
    private String userEducationName;
    /**  紧急联系人 */
    private String userContactorName;
    /**  联系人电话 */
    private String userContactorPhone;

    /** 子节点数据集合 */
    private List<SyncUser> childList;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getSyCompanyId() {
        return syCompanyId;
    }

    public void setSyCompanyId(String syCompanyId) {
        this.syCompanyId = syCompanyId;
    }

    public String getSyCompanyName() {
        return syCompanyName;
    }

    public void setSyCompanyName(String syCompanyName) {
        this.syCompanyName = syCompanyName;
    }

    public String getSyCompanyCode() {
        return syCompanyCode;
    }

    public void setSyCompanyCode(String syCompanyCode) {
        this.syCompanyCode = syCompanyCode;
    }

    public String getSyGroupCompanyId() {
        return syGroupCompanyId;
    }

    public void setSyGroupCompanyId(String syGroupCompanyId) {
        this.syGroupCompanyId = syGroupCompanyId;
    }

    public String getSyGroupCompanyName() {
        return syGroupCompanyName;
    }

    public void setSyGroupCompanyName(String syGroupCompanyName) {
        this.syGroupCompanyName = syGroupCompanyName;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }



    public String getDeptuserSfzgCode() {
        return deptuserSfzgCode;
    }

    public void setDeptuserSfzgCode(String deptuserSfzgCode) {
        this.deptuserSfzgCode = deptuserSfzgCode;
    }

    public String getUserPostCode() {
        return userPostCode;
    }

    public void setUserPostCode(String userPostCode) {
        this.userPostCode = userPostCode;
    }

    public String getUserPostName() {
        return userPostName;
    }

    public void setUserPostName(String userPostName) {
        this.userPostName = userPostName;
    }

    public String getDeptuserDirectleaderId() {
        return deptuserDirectleaderId;
    }

    public void setDeptuserDirectleaderId(String deptuserDirectleaderId) {
        this.deptuserDirectleaderId = deptuserDirectleaderId;
    }

    public String getDeptuserDirectleaderName() {
        return deptuserDirectleaderName;
    }

    public void setDeptuserDirectleaderName(String deptuserDirectleaderName) {
        this.deptuserDirectleaderName = deptuserDirectleaderName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getUserTelephone() {
        return userTelephone;
    }

    public void setUserTelephone(String userTelephone) {
        this.userTelephone = userTelephone;
    }

    public String getUserStation() {
        return userStation;
    }

    public void setUserStation(String userStation) {
        this.userStation = userStation;
    }

    public String getUserEntryDate() {
        return userEntryDate;
    }

    public void setUserEntryDate(String userEntryDate) {
        this.userEntryDate = userEntryDate;
    }

    public String getUserLeaveDate() {
        return userLeaveDate;
    }

    public void setUserLeaveDate(String userLeaveDate) {
        this.userLeaveDate = userLeaveDate;
    }

    public String getUserJobnum() {
        return userJobnum;
    }

    public void setUserJobnum(String userJobnum) {
        this.userJobnum = userJobnum;
    }


    public String getUserMonitordeptId() {
        return userMonitordeptId;
    }

    public void setUserMonitordeptId(String userMonitordeptId) {
        this.userMonitordeptId = userMonitordeptId;
    }

    public String getUserMonitordeptName() {
        return userMonitordeptName;
    }

    public void setUserMonitordeptName(String userMonitordeptName) {
        this.userMonitordeptName = userMonitordeptName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserSexCode() {
        return userSexCode;
    }

    public void setUserSexCode(String userSexCode) {
        this.userSexCode = userSexCode;
    }

    public String getUserNationalityCode() {
        return userNationalityCode;
    }

    public void setUserNationalityCode(String userNationalityCode) {
        this.userNationalityCode = userNationalityCode;
    }

    public String getUserNationalityName() {
        return userNationalityName;
    }

    public void setUserNationalityName(String userNationalityName) {
        this.userNationalityName = userNationalityName;
    }

    public String getUserBirth() {
        return userBirth;
    }

    public void setUserBirth(String userBirth) {
        this.userBirth = userBirth;
    }

    public String getUserAge() {
        return userAge;
    }

    public void setUserAge(String userAge) {
        this.userAge = userAge;
    }

    public String getUserNativePlace() {
        return userNativePlace;
    }

    public void setUserNativePlace(String userNativePlace) {
        this.userNativePlace = userNativePlace;
    }

    public String getUserNowaddress() {
        return userNowaddress;
    }

    public void setUserNowaddress(String userNowaddress) {
        this.userNowaddress = userNowaddress;
    }

    public String getUserHouseholdCode() {
        return userHouseholdCode;
    }

    public void setUserHouseholdCode(String userHouseholdCode) {
        this.userHouseholdCode = userHouseholdCode;
    }

    public String getUserHouseholdName() {
        return userHouseholdName;
    }

    public void setUserHouseholdName(String userHouseholdName) {
        this.userHouseholdName = userHouseholdName;
    }

    public String getUserPoliticalCode() {
        return userPoliticalCode;
    }

    public void setUserPoliticalCode(String userPoliticalCode) {
        this.userPoliticalCode = userPoliticalCode;
    }

    public String getUserPoliticalName() {
        return userPoliticalName;
    }

    public void setUserPoliticalName(String userPoliticalName) {
        this.userPoliticalName = userPoliticalName;
    }

    public String getUserMarriedCode() {
        return userMarriedCode;
    }

    public void setUserMarriedCode(String userMarriedCode) {
        this.userMarriedCode = userMarriedCode;
    }

    public String getUserCardnum() {
        return userCardnum;
    }

    public void setUserCardnum(String userCardnum) {
        this.userCardnum = userCardnum;
    }

    public String getUserNationCode() {
        return userNationCode;
    }

    public void setUserNationCode(String userNationCode) {
        this.userNationCode = userNationCode;
    }

    public String getUserNationName() {
        return userNationName;
    }

    public void setUserNationName(String userNationName) {
        this.userNationName = userNationName;
    }

    public String getUserEducationCode() {
        return userEducationCode;
    }

    public void setUserEducationCode(String userEducationCode) {
        this.userEducationCode = userEducationCode;
    }

    public String getUserEducationName() {
        return userEducationName;
    }

    public void setUserEducationName(String userEducationName) {
        this.userEducationName = userEducationName;
    }

    public String getUserContactorName() {
        return userContactorName;
    }

    public void setUserContactorName(String userContactorName) {
        this.userContactorName = userContactorName;
    }

    public String getUserContactorPhone() {
        return userContactorPhone;
    }

    public void setUserContactorPhone(String userContactorPhone) {
        this.userContactorPhone = userContactorPhone;
    }

    public List<SyncUser> getChildList() {
        return childList;
    }

    public void setChildList(List<SyncUser> childList) {
        this.childList = childList;
    }

    @Override
    public String toString() {
        return "SyncUser{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", userCode='" + userCode + '\'' +
                ", syCompanyId='" + syCompanyId + '\'' +
                ", syCompanyName='" + syCompanyName + '\'' +
                ", syCompanyCode='" + syCompanyCode + '\'' +
                ", departmentId='" + departmentId + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", deptuserSfzgCode='" + deptuserSfzgCode + '\'' +
                ", userPostName='" + userPostName + '\'' +
                ", deptuserDirectleaderId='" + deptuserDirectleaderId + '\'' +
                ", deptuserDirectleaderName='" + deptuserDirectleaderName + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userMail='" + userMail + '\'' +
                ", userTelephone='" + userTelephone + '\'' +
                ", userStation='" + userStation + '\'' +
                ", userEntryDate='" + userEntryDate + '\'' +
                ", userLeaveDate='" + userLeaveDate + '\'' +
                ", userJobnum='" + userJobnum + '\'' +
                ", userMonitordeptId='" + userMonitordeptId + '\'' +
                ", userMonitordeptName='" + userMonitordeptName + '\'' +
                ", userAvatar='" + userAvatar + '\'' +
                ", userPhoto='" + userPhoto + '\'' +
                ", userSexCode='" + userSexCode + '\'' +
                ", userNationalityCode='" + userNationalityCode + '\'' +
                ", userBirth='" + userBirth + '\'' +
                ", userAge='" + userAge + '\'' +
                ", userNativePlace='" + userNativePlace + '\'' +
                ", userNowaddress='" + userNowaddress + '\'' +
                ", userHouseholdCode='" + userHouseholdCode + '\'' +
                ", userPoliticalCode='" + userPoliticalCode + '\'' +
                ", userMarriedCode='" + userMarriedCode + '\'' +
                ", userCardnum='" + userCardnum + '\'' +
                ", userNationCode='" + userNationCode + '\'' +
                ", userEducationCode='" + userEducationCode + '\'' +
                ", userContactorName='" + userContactorName + '\'' +
                ", userContactorPhone='" + userContactorPhone + '\'' +
                ", childList=" + childList +
                '}';
    }
}
