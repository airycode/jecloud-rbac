/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.model.syncdata;

import java.io.Serializable;
import java.util.List;

/** 同步部门对象 */
public class SyncDepartment implements Serializable {

    /** 必填项, 部门数据Id */
    private String departmentId;
    /** 必填项，部门名称 */
    private String departmentName;

    /** 必填项，部门编码 */
    private String departmentCode;

    /**  简称 */
    private String departmentSimpleName;

    /** 必填项, 部门级别Code 部门：DEPT 车间：WORKSHOP 班组:TEAM */
    private String departmentLevelCode;

    /**  电话 */
    private String departmentTelephone;

    /**  办公地点 */
    private String departmentAddress;

    /**  职能描述 */
    private String departmentFuncDesc;

    /**  备注 */
    private String departmentRemark;

    /**  主管领导Id */
    private String departmentMajorId;

    /**  主管领导名称 */
    private String departmentMajorName;

    /**  监管部门Id */
    private String departmentMonitordeptId;

    /**  监管部门名称 */
    private String departmentMonitordeptName;

    /** 必填项，所属公司Id */
    private String syCompanyId;

    /** 必填项，所属公司名称 */
    private String syCompanyName;

    /**  上级部门名称 */
    private String superiorDepartment;

    /** 必填项， 父级Id：顶级节点父级为: "ROOT" */
    private String syParent;

    /** 子节点数据集合 */
    private List<SyncDepartment> childList;

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentSimpleName() {
        return departmentSimpleName;
    }

    public void setDepartmentSimpleName(String departmentSimpleName) {
        this.departmentSimpleName = departmentSimpleName;
    }

    public String getDepartmentLevelCode() {
        return departmentLevelCode;
    }

    public void setDepartmentLevelCode(String departmentLevelCode) {
        this.departmentLevelCode = departmentLevelCode;
    }

    public String getDepartmentTelephone() {
        return departmentTelephone;
    }

    public void setDepartmentTelephone(String departmentTelephone) {
        this.departmentTelephone = departmentTelephone;
    }

    public String getDepartmentAddress() {
        return departmentAddress;
    }

    public void setDepartmentAddress(String departmentAddress) {
        this.departmentAddress = departmentAddress;
    }

    public String getDepartmentFuncDesc() {
        return departmentFuncDesc;
    }

    public void setDepartmentFuncDesc(String departmentFuncDesc) {
        this.departmentFuncDesc = departmentFuncDesc;
    }

    public String getDepartmentRemark() {
        return departmentRemark;
    }

    public void setDepartmentRemark(String departmentRemark) {
        this.departmentRemark = departmentRemark;
    }

    public String getDepartmentMajorId() {
        return departmentMajorId;
    }

    public void setDepartmentMajorId(String departmentMajorId) {
        this.departmentMajorId = departmentMajorId;
    }

    public String getDepartmentMajorName() {
        return departmentMajorName;
    }

    public void setDepartmentMajorName(String departmentMajorName) {
        this.departmentMajorName = departmentMajorName;
    }

    public String getDepartmentMonitordeptId() {
        return departmentMonitordeptId;
    }

    public void setDepartmentMonitordeptId(String departmentMonitordeptId) {
        this.departmentMonitordeptId = departmentMonitordeptId;
    }

    public String getDepartmentMonitordeptName() {
        return departmentMonitordeptName;
    }

    public void setDepartmentMonitordeptName(String departmentMonitordeptName) {
        this.departmentMonitordeptName = departmentMonitordeptName;
    }

    public String getSyCompanyId() {
        return syCompanyId;
    }

    public void setSyCompanyId(String syCompanyId) {
        this.syCompanyId = syCompanyId;
    }

    public String getSyCompanyName() {
        return syCompanyName;
    }

    public void setSyCompanyName(String syCompanyName) {
        this.syCompanyName = syCompanyName;
    }

    public String getSuperiorDepartment() {
        return superiorDepartment;
    }

    public void setSuperiorDepartment(String superiorDepartment) {
        this.superiorDepartment = superiorDepartment;
    }

    public String getSyParent() {
        return syParent;
    }

    public void setSyParent(String syParent) {
        this.syParent = syParent;
    }

    public List<SyncDepartment> getChildList() {
        return childList;
    }

    public void setChildList(List<SyncDepartment> childList) {
        this.childList = childList;
    }

    @Override
    public String toString() {
        return "SyncDepartment{" +
                "departmentId='" + departmentId + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", departmentCode='" + departmentCode + '\'' +
                ", departmentSimpleName='" + departmentSimpleName + '\'' +
                ", departmentLevelCode='" + departmentLevelCode + '\'' +
                ", departmentTelephone='" + departmentTelephone + '\'' +
                ", departmentAddress='" + departmentAddress + '\'' +
                ", departmentFuncDesc='" + departmentFuncDesc + '\'' +
                ", departmentRemark='" + departmentRemark + '\'' +
                ", departmentMajorId='" + departmentMajorId + '\'' +
                ", departmentMajorName='" + departmentMajorName + '\'' +
                ", departmentMonitordeptId='" + departmentMonitordeptId + '\'' +
                ", departmentMonitordeptName='" + departmentMonitordeptName + '\'' +
                ", syCompanyId='" + syCompanyId + '\'' +
                ", syCompanyName='" + syCompanyName + '\'' +
                ", superiorDepartment='" + superiorDepartment + '\'' +
                ", syParent='" + syParent + '\'' +
                ", childList=" + childList +
                '}';
    }
}
