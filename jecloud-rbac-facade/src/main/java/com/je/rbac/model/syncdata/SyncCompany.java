/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.model.syncdata;

import java.io.Serializable;
import java.util.List;
/** 同步公司对象 */
public class SyncCompany implements Serializable {

    /** 必填项, 公司数据Id */
    private String companyId;
    /** 必填项， 公司名称 */
    private String companyName;
    /** 必填项， 公司编码 */
    private String companyCode;
    /** 必填项， 公司层级Code: 集团公司：GROUP_COMPANY 公司：COMPANY */
    private String companyLevelCode;
    /** 公司简称 */
    private String companySimplename;
    /** 公司办公地点*/
    private String companyAddress;
    /** 公司电话 */
    private String companyTelephone;
    /** 备注信息 */
    private String companyRemark;
    /** 职能描述 */
    private String companyFuncDesc;

    /** 管理员Id */
    private String companyManagerId;
    /** 管理员名称 */
    private String companyManagerName;
    /** 主管Id */
    private String companyMajorId;
    /** 主管名称 */
    private String companyMajorName;
    /** 监管公司Id */
    private String companyJgcompanyId;
    /** 监管公司名称 */
    private String companyJgcompanyName;

    /** 必填项， 父级Id：顶级节点父级为: "ROOT" */
    private String syParent;

    /** 子节点数据集合 */
    private List<SyncCompany> childList;

    public String getCompanyManagerId() {
        return companyManagerId;
    }

    public void setCompanyManagerId(String companyManagerId) {
        this.companyManagerId = companyManagerId;
    }

    public String getCompanyManagerName() {
        return companyManagerName;
    }

    public void setCompanyManagerName(String companyManagerName) {
        this.companyManagerName = companyManagerName;
    }

    public String getCompanyMajorId() {
        return companyMajorId;
    }

    public void setCompanyMajorId(String companyMajorId) {
        this.companyMajorId = companyMajorId;
    }

    public String getCompanyMajorName() {
        return companyMajorName;
    }

    public void setCompanyMajorName(String companyMajorName) {
        this.companyMajorName = companyMajorName;
    }

    public String getCompanyJgcompanyId() {
        return companyJgcompanyId;
    }

    public void setCompanyJgcompanyId(String companyJgcompanyId) {
        this.companyJgcompanyId = companyJgcompanyId;
    }

    public String getCompanyJgcompanyName() {
        return companyJgcompanyName;
    }

    public void setCompanyJgcompanyName(String companyJgcompanyName) {
        this.companyJgcompanyName = companyJgcompanyName;
    }

    public String getSyParent() {
        return syParent;
    }

    public void setSyParent(String syParent) {
        this.syParent = syParent;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyLevelCode() {
        return companyLevelCode;
    }

    public void setCompanyLevelCode(String companyLevelCode) {
        this.companyLevelCode = companyLevelCode;
    }

    public String getCompanySimplename() {
        return companySimplename;
    }

    public void setCompanySimplename(String companySimplename) {
        this.companySimplename = companySimplename;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyTelephone() {
        return companyTelephone;
    }

    public void setCompanyTelephone(String companyTelephone) {
        this.companyTelephone = companyTelephone;
    }

    public String getCompanyRemark() {
        return companyRemark;
    }

    public void setCompanyRemark(String companyRemark) {
        this.companyRemark = companyRemark;
    }

    public String getCompanyFuncDesc() {
        return companyFuncDesc;
    }

    public void setCompanyFuncDesc(String companyFuncDesc) {
        this.companyFuncDesc = companyFuncDesc;
    }

    public List<SyncCompany> getChildList() {
        return childList;
    }

    public void setChildList(List<SyncCompany> childList) {
        this.childList = childList;
    }

    @Override
    public String toString() {
        return "SyncCompany{" +
                "companyId='" + companyId + '\'' +
                ", companyName='" + companyName + '\'' +
                ", companyCode='" + companyCode + '\'' +
                ", companyLevelCode='" + companyLevelCode + '\'' +
                ", companySimplename='" + companySimplename + '\'' +
                ", companyAddress='" + companyAddress + '\'' +
                ", companyTelephone='" + companyTelephone + '\'' +
                ", companyRemark='" + companyRemark + '\'' +
                ", companyFuncDesc='" + companyFuncDesc + '\'' +
                ", companyManagerId='" + companyManagerId + '\'' +
                ", companyManagerName='" + companyManagerName + '\'' +
                ", companyMajorId='" + companyMajorId + '\'' +
                ", companyMajorName='" + companyMajorName + '\'' +
                ", companyJgcompanyId='" + companyJgcompanyId + '\'' +
                ", companyJgcompanyName='" + companyJgcompanyName + '\'' +
                ", syParent='" + syParent + '\'' +
                ", childList=" + childList +
                '}';
    }
}
