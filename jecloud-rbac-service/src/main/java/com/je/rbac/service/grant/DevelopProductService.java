/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant;

import com.je.common.base.DynaBean;
import com.je.rbac.exception.RoleException;

/**
 * 产品开发
 */
public interface DevelopProductService {

    /**
     * 创建产品开发人员角色
     * @param productId
     * @param productName
     */
    DynaBean createProductRole(String productId, String productName);

    /**
     * 创建产品开发人员角色
     * @param productId
     * @param productName
     */
    void delteProductRole(String productId, String productName) throws RoleException;

    /**
     * 对产品开发角色授权
     * @param productId
     */
    void grant(String productId) throws RoleException;


    /**
     * 设置产品开发人员
     * @param productBean
     */
    void assignProductRoleToUser(String productId,DynaBean productBean) throws RoleException;
}
