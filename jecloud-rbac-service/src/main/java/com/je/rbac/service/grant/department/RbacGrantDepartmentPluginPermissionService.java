/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.department;

import com.je.rbac.service.permission.GrantTypeEnum;

import java.util.List;

public interface RbacGrantDepartmentPluginPermissionService {

    /**
     * 授予插件权限
     * @param departmentId
     * @param pluginCode
     * @param grantType
     */
    void saveDepartmentPluginPermission(String departmentId, String pluginCode, GrantTypeEnum grantType);

    /**
     * 移除开发菜单权限
     *
     * @param departmentId
     * @param pluginCode
     */
    void removeDepartmentPluginPermission(String departmentId, String pluginCode, GrantTypeEnum grantType);

    /**
     * 保存开发菜单权限
     *
     * @param departmentId
     * @param pluginCodeList
     */
    void saveDepartmentPluginPermissions(String departmentId, List<String> pluginCodeList, GrantTypeEnum grantType);

    /**
     * 移除开发菜单权限
     *
     * @param departmentId
     * @param pluginCodeList
     */
    void removeDepartmentPluginPermissions(String departmentId, List<String> pluginCodeList, GrantTypeEnum grantType);
}
