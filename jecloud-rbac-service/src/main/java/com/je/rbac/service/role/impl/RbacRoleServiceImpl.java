/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.role.impl;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.role.RbacRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class RbacRoleServiceImpl implements RbacRoleService {

    public static final String FOLDER_TYPE_CODE = "folder";
    public static final String FOLDER_TYPE_NAME = "文件夹";

    public static final String ROLE_TYPE_CODE = "role";
    public static final String ROLE_TYPE_NAME = "角色";

    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean findById(String id) {
        return metaService.selectOne("JE_RBAC_ROLE", ConditionsWrapper.builder().eq("JE_RBAC_ROLE_ID", id));
    }

    @Override
    public List<DynaBean> findByIds(List<String> roleIds) {
        return metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder().in("JE_RBAC_ROLE_ID", roleIds));
    }

    @Override
    public Set<String> findExtendRoleIds(List<String> roleIds) {
        List<DynaBean> roleBeanList = metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder()
                .in("JE_RBAC_ROLE_ID", roleIds).ne("ROLE_TYPE_CODE", "folder").ne("SY_NODETYPE", "ROOT"));
        Set<String> extendRoleIdList = new HashSet<>();
        for (DynaBean eachRoleBean : roleBeanList) {
            String path = eachRoleBean.getStr("SY_PATH");
            if (!Strings.isNullOrEmpty(path)) {
                extendRoleIdList.addAll(Splitter.on("/").splitToList(path));
            }
        }
        extendRoleIdList.removeAll(roleIds);
        extendRoleIdList.remove("ROOT");
        return extendRoleIdList;
    }

    @Override
    public List<DynaBean> findExtendRoles(List<String> roleIds) {
        Set<String> extendRoleIdList = findExtendRoleIds(roleIds);
        return metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder().in("JE_RBAC_ROLE_ID", extendRoleIdList));
    }

    @Override
    public List<String> findChildrenRoleIds(List<String> roleIds) {
        ConditionsWrapper wrapper = ConditionsWrapper.builder();
        for (int i = 0; i < roleIds.size(); i++) {
            if (i != 0) {
                wrapper.or();
            }
            wrapper.like("SY_PATH", roleIds.get(i));
        }
        List<DynaBean> childrenRoleList = metaService.select("JE_RBAC_ROLE", wrapper);
        List<String> childrenRoleIdList = new ArrayList<>();
        for (DynaBean eachRoleBean : childrenRoleList) {
            if (!roleIds.contains(eachRoleBean.getStr("JE_RBAC_ROLE_ID"))) {
                childrenRoleIdList.add(eachRoleBean.getStr("JE_RBAC_ROLE_ID"));
            }
        }
        return childrenRoleIdList;
    }

    @Override
    public List<String> findRoleUserIds(List<String> roleIds) {
        List<DynaBean> roleBeans = metaService.select("JE_RBAC_VACCOUNTROLE",ConditionsWrapper.builder()
                .in("ACCOUNTROLE_ROLE_ID",roleIds));
        List<String> userIdList = new ArrayList<>();
        for (DynaBean eachRoleBean : roleBeans) {
            userIdList.add(eachRoleBean.getStr("USER_ASSOCIATION_ID"));
        }
        return userIdList;
    }
}
