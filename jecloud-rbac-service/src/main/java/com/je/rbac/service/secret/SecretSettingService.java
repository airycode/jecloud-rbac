/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.secret;

import com.je.common.base.DynaBean;
import com.je.core.entity.extjs.JSONTreeNode;

import java.util.List;
import java.util.Map;

public interface SecretSettingService {

    /**
     * 初始化密级上下文
     */
    void initSecretContext(DynaBean secretSettingBean);

    /**
     * 清除密级上下文
     */
    void clearSecretContext();

    /**
     * 获取系统加密信息
     * @return
     */
    Map<String,String> requireSystemSecretInfo();

    /**
     * 获取用户上下文密级
     * @param ip
     * @param accountId
     * @return
     */
    Map<String,String> requireUserContextSecretInfo(String ip,String accountId);

    /**
     * 获取系统功能密级关系树形结构
     * @return
     */
    JSONTreeNode requireSystemFuncSecretInfo(String relation);

    /**
     * 获取功能表单密级关系树形结构
     * @param funcId
     * @return
     */
    JSONTreeNode requireFuncFormSecretInfo(String funcId);

    /**
     * 获取系统流程密级关系树形结构
     * @return
     */
    JSONTreeNode requireSystemFlowSecretInfo(String relation);

    /**
     * 获取数据附件关系树形结构
     * @return
     */
    JSONTreeNode requireDataAttachmentSecretInfo(String relation,String dataSecretCode);
    
    /**
     * 获取上下文密级可操作的数据密级列表
     * @return
     */
    List<String> requireContextDataSecretInfo(String contextSecretCode);

    /**
     * 根据数据密级获取可处理的流程节点密级
     * @param dataSecretCode
     * @return
     */
    List<String> requireDataFlowSecretInfo(String dataSecretCode);

}
