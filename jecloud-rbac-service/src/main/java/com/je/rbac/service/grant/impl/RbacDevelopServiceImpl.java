/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.impl;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.common.base.util.TreeUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.model.dd.DicInfoVo;
import com.je.rbac.service.grant.RbacDevelopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("rbacDevelopService")
public class RbacDevelopServiceImpl implements RbacDevelopService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private SystemSettingRpcService systemSettingRpcService;

    @Override
    public JSONTreeNode buildDevelopUsers(DicInfoVo dicInfoVo) {
        String developId = systemSettingRpcService.findSettingValue("JE_SYS_ADMIN");
        if(Strings.isNullOrEmpty(developId)){
            return TreeUtil.buildRootNode();
        }
        List<DynaBean> developBeanList = metaService.select("JE_RBAC_VACCOUNTDEPT", ConditionsWrapper.builder()
                .in("JE_RBAC_ACCOUNTDEPT_ID", Splitter.on(",").splitToList(developId)));
        if(developBeanList == null || developBeanList.isEmpty()){
            return TreeUtil.buildRootNode();
        }

        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        developBeanList.forEach(each -> {
            JSONTreeNode eachChildNode = new JSONTreeNode();
            eachChildNode.setId(each.getStr("JE_RBAC_ACCOUNTDEPT_ID"));
            eachChildNode.setCode(each.getStr("ACCOUNT_CODE"));
            eachChildNode.setText(each.getStr("ACCOUNT_NAME"));
            eachChildNode.setChecked(false);
            eachChildNode.setDisabled("0");
            eachChildNode.setParent(rootNode.getId());
            eachChildNode.setNodeInfo(each.getStr("JE_RBAC_ACCOUNTDEPT_ID"));
            eachChildNode.setNodeInfoType("accountDept");
            eachChildNode.setExpanded(true);
            eachChildNode.setExpandable(true);
            eachChildNode.setLeaf(true);
            eachChildNode.setBean(each.getValues());
            eachChildNode.setIcon("fal fa-user");
            eachChildNode.setLayer("1");
            eachChildNode.setNodePath("ROOT/" + each.getStr("JE_RBAC_ACCOUNTDEPT_ID"));
            rootNode.getChildren().add(eachChildNode);
        });
        return rootNode;
    }

}
