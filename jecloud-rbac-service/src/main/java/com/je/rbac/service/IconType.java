/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service;

public enum IconType {
    TCONTYPE_TENANT("","#3265f5", "fas fa-layer-group"),
    TCONTYPE_ORG("", "","fal fa-landmark"),
    TCONTYPE_GROUP("", "#32a862","fas fa-university"),
    TCONTYPE_COMPANY("","#f3752d", "fas fa-building"),
    TCONTYPE_DEPART("","#3265f5", "fas fa-layer-group"),
    TCONTYPE_ROLE("", "","fal fa-user");

    // 成员变量
    private String code;
    private String iconColor;
    private String val;
    // 构造方法
    private IconType(String code,String iconColor, String val) {
        this.code = code;
        this.iconColor = iconColor;
        this.val = val;
    }

    public String getCode() {
        return code;
    }

    public String getVal() {
        return val;
    }

    public String getIconColor() {
        return iconColor;
    }

    //覆盖方法
    @Override
    public String toString() {
        return this.code+"_"+this.val+"_"+this.iconColor;
    }
}
