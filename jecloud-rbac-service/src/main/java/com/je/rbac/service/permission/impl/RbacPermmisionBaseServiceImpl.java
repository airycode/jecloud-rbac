/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.impl;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.permission.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RbacPermmisionBaseServiceImpl implements RbacPermmisionBaseService {

    @Autowired
    private MetaService metaService;


    @Override
    public DynaBean findById(String id) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("JE_RBAC_PERM_ID", id));
    }

    @Override
    public DynaBean findByCode(String code) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", code));
    }

    @Override
    public DynaBean findName(String name) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_NAME", name));
    }

    @Override
    public List<Map<String, Object>> findByIds(List<String> idList) {
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_PERM").in("JE_RBAC_PERM_ID", idList));
    }

    @Override
    public List<Map<String, Object>> findByCodes(List<String> codeList) {
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_PERM").in("PERM_CODE", codeList));
    }

    @Override
    public List<DynaBean> findByType(PermissionTypeEnum type) {
        return metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder().in("PERM_TYPE_CODE", type.name()));
    }

    @Override
    public List<DynaBean> findByTypeAndOutput(PermissionTypeEnum type, boolean output) {
        return metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder()
                .in("PERM_TYPE_CODE", type.name()).eq("PERM_OUTPUT_CODE", "1"));
    }

    @Override
    public List<DynaBean> findByTenantId(String tenantId) {
        return metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder().eq("SY_TENANT_ID", tenantId));
    }

    @Override
    public List<DynaBean> findByTenantName(String tenantName) {
        return metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder().eq("SY_TENANT_NAME", tenantName));
    }

    @Override
    public List<Map<String, Object>> findDepartmentPermissions(List<String> departmentIdList) {
        if (departmentIdList == null || departmentIdList.isEmpty()) {
            return null;
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VDEPTPERM").in("JE_RBAC_DEPARTMENT_ID", departmentIdList));
    }

    @Override
    public List<Map<String, Object>> findDepartmentFuncPermissions(List<String> departmentIdList, String funcCode) {
        if (departmentIdList == null || departmentIdList.isEmpty()) {
            return null;
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VDEPTPERM")
                .eq("PERM_OBJECT", funcCode)
                .in("PERM_TYPE_CODE", "FUNC_PC_BASE", "BUTTON_PC", "FUNC_PC_CONFIG","SUBFUNC_PC")
                .in("JE_RBAC_DEPARTMENT_ID", departmentIdList));
    }

    @Override
    public List<Map<String, Object>> findUserPermissions(String userId) {
        if (Strings.isNullOrEmpty(userId)) {
            return null;
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VUSERPERM").eq("JE_RBAC_USER_ID", userId));
    }

    @Override
    public List<Map<String, Object>> findUserFuncPermissions(String userId, String funcCode) {
        if (Strings.isNullOrEmpty(userId)) {
            return null;
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VUSERPERM")
                .eq("PERM_OBJECT", funcCode)
                .in("PERM_TYPE_CODE", "FUNC_PC_BASE", "BUTTON_PC", "FUNC_PC_CONFIG","SUBFUNC_PC")
                .eq("JE_RBAC_USER_ID", userId));
    }

    @Override
    public List<Map<String, Object>> findRolePermissions(List<String> roleIds) {
        List<DynaBean> roleBeans = metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder().in("JE_RBAC_ROLE_ID", roleIds));
        if (roleBeans != null && !roleBeans.isEmpty()) {
            for (DynaBean eachRoleBean : roleBeans) {
                if(!Strings.isNullOrEmpty(eachRoleBean.getStr("ROLE_BASE_ID"))){
                    roleIds.add(eachRoleBean.getStr("ROLE_BASE_ID"));
                }
            }
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VROLEPERM").in("JE_RBAC_ROLE_ID", roleIds));
    }

    @Override
    public List<Map<String, Object>> findRoleFuncPermissions(List<String> roleIds, String funcCode) {
        List<DynaBean> roleBeans = metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder().in("JE_RBAC_ROLE_ID", roleIds));
        if (roleBeans != null && !roleBeans.isEmpty()) {
            for (DynaBean eachRoleBean : roleBeans) {
                if(!Strings.isNullOrEmpty(eachRoleBean.getStr("ROLE_BASE_ID"))){
                    roleIds.add(eachRoleBean.getStr("ROLE_BASE_ID"));
                }
            }
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VROLEPERM")
                .eq("PERM_OBJECT", funcCode)
                .in("PERM_TYPE_CODE", "FUNC_PC_BASE", "BUTTON_PC", "FUNC_PC_CONFIG","SUBFUNC_PC")
                .in("JE_RBAC_ROLE_ID", roleIds));
    }

    @Override
    public List<Map<String, Object>> findOrgPermissions(String orgId) {
        if (Strings.isNullOrEmpty(orgId)) {
            return null;
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VORGPERM").eq("JE_RBAC_ORG_ID", orgId));
    }

    @Override
    public List<Map<String, Object>> findOrgFuncPermissions(String orgId, String funcCode) {
        if (Strings.isNullOrEmpty(orgId)) {
            return null;
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VORGPERM")
                .eq("PERM_OBJECT", funcCode)
                .in("PERM_TYPE_CODE", "FUNC_PC_BASE", "BUTTON_PC", "FUNC_PC_CONFIG","SUBFUNC_PC")
                .eq("JE_RBAC_ORG_ID", orgId));
    }

    @Override
    public List<Map<String, Object>> findPermGroupPermissions(List<String> permGroupIdList) {
        if (permGroupIdList == null || permGroupIdList.isEmpty()) {
            return null;
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VPERMGROUP").in("JE_RBAC_PERMGROUP_ID", permGroupIdList));
    }

    @Override
    public List<Map<String, Object>> findPermGroupFuncPermissions(List<String> permGroupIdList, String funcCode) {
        if (permGroupIdList == null || permGroupIdList.isEmpty()) {
            return null;
        }
        return metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VPERMGROUP")
                .eq("PERM_OBJECT", funcCode)
                .in("PERM_TYPE_CODE", "FUNC_PC_BASE", "BUTTON_PC", "FUNC_PC_CONFIG","SUBFUNC_PC")
                .in("JE_RBAC_PERMGROUP_ID", permGroupIdList));
    }

}
