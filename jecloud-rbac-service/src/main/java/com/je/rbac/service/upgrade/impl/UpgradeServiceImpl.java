/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.upgrade.impl;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaRbacService;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.upgrade.UpgradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UpgradeServiceImpl implements UpgradeService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaResourceService metaResourceService;

    @Override
    public void insertPackageRoles(String upgradeId, String roleIds) {
        List<String> roleIdList = new ArrayList<>(Splitter.on(",").splitToList(roleIds));
        List<DynaBean> roleBeanList = metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder()
                .in("JE_RBAC_ROLE_ID", roleIdList));
        List<String> pathIdList;
        for (DynaBean eachMenu : roleBeanList) {
            String path = eachMenu.getStr("SY_PATH");
            if (Strings.isNullOrEmpty(path)) {
                continue;
            }
            pathIdList = Splitter.on(",").splitToList(path);
            for (String eachId : pathIdList) {
                if (!"ROOT".equals(eachId) && !roleIdList.contains(eachId)) {
                    roleIdList.add(eachId);
                }
            }
        }

        //查找全部
        roleBeanList = metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder().in("JE_RBAC_ROLE_ID", roleIdList));
        deleteResourceByType(upgradeId,"role");
        for (DynaBean eachRoleBean : roleBeanList) {
            DynaBean detailBean = new DynaBean("JE_META_UPGRADERESOURCE", false);
            detailBean.set("UPGRADERESOURCE_TYPE_NAME", "角色");
            detailBean.set("UPGRADERESOURCE_TYPE_CODE", "role");
            detailBean.set("UPGRADERESOURCE_CONTENT_NAME", eachRoleBean.getStr("ROLE_NAME"));
            detailBean.set("UPGRADERESOURCE_CONTENT_CODE", eachRoleBean.getStr("JE_RBAC_ROLE_ID"));
            detailBean.set("UPGRADERESOURCE_CONTENT_BM", eachRoleBean.getStr("ROLE_CODE"));
            detailBean.set("JE_META_UPGRADEPACKAGE_ID", upgradeId);
            commonService.buildModelCreateInfo(detailBean);
            metaResourceService.insert(detailBean);
        }
    }

    @Override
    public void insertPackageMenus(String upgradeId, String menuIds) {
        List<String> menuIdList = new ArrayList<>(Splitter.on(",").splitToList(menuIds));
        List<DynaBean> menuBeanList = metaService.select("JE_CORE_MENU", ConditionsWrapper.builder()
                .in("JE_CORE_MENU_ID", menuIdList));
        List<String> pathIdList;
        for (DynaBean eachMenu : menuBeanList) {
            String path = eachMenu.getStr("SY_PATH");
            if (Strings.isNullOrEmpty(path)) {
                continue;
            }
            pathIdList = Splitter.on(",").splitToList(path);
            for (String eachId : pathIdList) {
                if (!"ROOT".equals(eachId) && !menuIdList.contains(eachId)) {
                    menuIdList.add(eachId);
                }
            }
        }
        //查找全部
        menuBeanList = metaService.select("JE_CORE_MENU", ConditionsWrapper.builder()
                .in("JE_CORE_MENU_ID", menuIdList));

        deleteResourceByType(upgradeId,"menu");

        for (DynaBean eachMenuBean : menuBeanList) {
            DynaBean detailBean = new DynaBean("JE_META_UPGRADERESOURCE", false);
            detailBean.set("UPGRADERESOURCE_TYPE_NAME", "菜单");
            detailBean.set("UPGRADERESOURCE_TYPE_CODE", "menu");
            detailBean.set("UPGRADERESOURCE_CONTENT_NAME", eachMenuBean.getStr("MENU_MENUNAME"));
            detailBean.set("UPGRADERESOURCE_CONTENT_CODE", eachMenuBean.getStr("JE_CORE_MENU_ID"));
            detailBean.set("JE_META_UPGRADEPACKAGE_ID", upgradeId);
            detailBean.set("UPGRADERESOURCE_REMARK",eachMenuBean.getStr("MENU_HELP"));
            commonService.buildModelCreateInfo(detailBean);
            metaResourceService.insert(detailBean);
        }
    }

    @Override
    public void insertPackageTopMenus(String upgradeId, String topMenuIds) {
        List<String> menuIdList = new ArrayList<>(Splitter.on(",").splitToList(topMenuIds));
        List<DynaBean> menuBeanList = metaService.select("JE_RBAC_HEADMENU", ConditionsWrapper.builder()
                .in("JE_RBAC_HEADMENU_ID", menuIdList));

        deleteResourceByType(upgradeId,"topMenu");
        for (DynaBean eachMenuBean : menuBeanList) {
            DynaBean detailBean = new DynaBean("JE_META_UPGRADERESOURCE", false);
            detailBean.set("UPGRADERESOURCE_TYPE_NAME", "顶部菜单");
            detailBean.set("UPGRADERESOURCE_TYPE_CODE", "topMenu");
            detailBean.set("UPGRADERESOURCE_CONTENT_NAME", eachMenuBean.getStr("HEADMENU_NAME"));
            detailBean.set("UPGRADERESOURCE_CONTENT_CODE", eachMenuBean.getStr("JE_RBAC_HEADMENU_ID"));
            detailBean.set("UPGRADERESOURCE_CONTENT_BM", eachMenuBean.getStr("HEADMENU_CODE"));
            detailBean.set("JE_META_UPGRADEPACKAGE_ID", upgradeId);
            commonService.buildModelCreateInfo(detailBean);
            metaResourceService.insert(detailBean);
        }
    }

    public void deleteResourceByType(String upgradeId,String type){
        metaResourceService.deleteByTableCodeAndNativeQuery("JE_META_UPGRADERESOURCE",
                NativeQuery.build().eq("JE_META_UPGRADEPACKAGE_ID",upgradeId).eq("UPGRADERESOURCE_TYPE_CODE",type));
    }

}
