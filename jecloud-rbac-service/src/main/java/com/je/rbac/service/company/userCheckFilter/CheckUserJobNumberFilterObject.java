/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.company.userCheckFilter;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 规则拦截对象
 */
@Component
@Order(4) //校验顺序排第4
public class CheckUserJobNumberFilterObject extends AbstractHandler {

    @Override
    public void doFilter(HttpServletRequest request, Response response) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        long count = 0;
        //工号为空不进行校验
        if (!Strings.isNullOrEmpty(dynaBean.getStr("USER_JOBNUM"))) {
            if (Strings.isNullOrEmpty(dynaBean.getStr("JE_RBAC_USER_ID"))) {
                count = metaService.countBySql(ConditionsWrapper.builder().table("JE_RBAC_USER").eq("USER_JOBNUM", dynaBean.getStr("USER_JOBNUM")));
            } else {
                count = metaService.countBySql(ConditionsWrapper.builder().apply("SELECT * FROM JE_RBAC_USER WHERE JE_RBAC_USER_ID != {0} AND (USER_JOBNUM = {1})", dynaBean.getStr("JE_RBAC_USER_ID"), dynaBean.getStr("USER_JOBNUM")));
            }
        }
        if (count > 0) {
            response.setSuccess(false).setMessage("人员工号重复，请修改！");
        }
    }
}