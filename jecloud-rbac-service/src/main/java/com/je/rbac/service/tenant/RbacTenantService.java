/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.tenant;

import com.je.common.base.DynaBean;
import com.je.rbac.exception.AccountException;
import com.je.rbac.exception.TenantException;
import com.je.rbac.model.Tenant;

import java.util.List;

/**
 * 租户管理服务
 */
public interface RbacTenantService {

    /**
     * 查找所有租户
     * @return
     */
    List<DynaBean> findAllTenants(int... startAndLimit);

    /**
     * 查找租户
     *
     * @param tenantId
     * @return
     */
    DynaBean findTenantById(String tenantId);

    /**
     * 查找租户
     *
     * @param code
     * @return
     */
    DynaBean findTenantByCode(String code);

    /**
     * 查找租户
     *
     * @param name
     * @return
     */
    DynaBean findTenantByName(String name);

    /**
     * 查找租户
     *
     * @param phone
     * @return
     */
    DynaBean findTenantByPhone(String phone);

    /**
     * 查找租户
     *
     * @param email
     * @return
     */
    DynaBean findTenantByEmail(String email);

    /**
     * 查找租户
     *
     * @param fax
     * @return
     */
    DynaBean findTenantByFax(String fax);

    /**
     * 根据来源查找租户
     * @param sourceCode
     * @return
     */
    List<DynaBean> findTenantBySouce(String sourceCode);

    /**
     * 开通租户
     * @param tenant
     */
    void openTenant(Tenant tenant) throws TenantException, AccountException;

}
