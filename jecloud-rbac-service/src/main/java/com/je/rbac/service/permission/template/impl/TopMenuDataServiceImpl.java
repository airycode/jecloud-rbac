/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template.impl;

import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.permission.PermissionOperationTypeEnum;
import com.je.rbac.service.permission.PermissionTypeEnum;
import com.je.rbac.service.permission.template.PermissionTopMenuTemplateEnum;
import com.je.rbac.service.permission.template.TopMenuDataService;
import org.springframework.stereotype.Service;

@Service
public class TopMenuDataServiceImpl extends AbstractPermissionTemplateService implements TopMenuDataService {

    @Override
    public String formatTopMenuShowTemplate(String menuCode) {
        return formatTemplate(PermissionTopMenuTemplateEnum.TOP_MENU_SHOW.getTemplate(), Entry.build("menuCode", menuCode));
    }

    @Override
    public DynaBean writeTopMenuShowPermission(String menuCode, boolean checkDb) {
        if (checkDb) {
            DynaBean bean = metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatTopMenuShowTemplate(menuCode)));
            if (bean != null) {
                return bean;
            }
        }
        return writePcFuncPermissionWithoutOutput(PermissionTypeEnum.TOPMENU_DATA, PermissionTopMenuTemplateEnum.TOP_MENU_SHOW.getName(),
                formatTopMenuShowTemplate(menuCode), menuCode, PermissionOperationTypeEnum.SHOW);
    }

    @Override
    public DynaBean findPcPluginConfigPermission(String menuCode) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatTopMenuShowTemplate(menuCode)));
    }

}
