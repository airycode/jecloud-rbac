/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template.impl;

import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.permission.PermissionOperationTypeEnum;
import com.je.rbac.service.permission.PermissionTypeEnum;
import com.je.rbac.service.permission.template.PcSubFuncRelationService;
import com.je.rbac.service.permission.template.PermissionPcFuncRelationTemplateEnum;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class PcSubFuncRelationServiceImpl extends AbstractPermissionTemplateService implements PcSubFuncRelationService {

    @Override
    public String formatPcSubFuncRelationShowTemplate(String funcCode, String subFuncCode) {
        return formatTemplate(PermissionPcFuncRelationTemplateEnum.SUBFUNC_PC_SHOW.getTemplate(), Entry.build("funcCode", funcCode), Entry.build("subFuncCode", subFuncCode));
    }

    @Override
    public String formatPcSubFuncRelationUpdateTemplate(String funcCode, String subFuncCode) {
        return formatTemplate(PermissionPcFuncRelationTemplateEnum.SUBFUNC_PC_UPDATE.getTemplate(), Entry.build("funcCode", funcCode), Entry.build("subFuncCode", subFuncCode));
    }

    @Override
    public String formatPcSubFuncRelationDeleteTemplate(String funcCode, String subFuncCode) {
        return formatTemplate(PermissionPcFuncRelationTemplateEnum.SUBFUNC_PC_DELETE.getTemplate(), Entry.build("funcCode", funcCode), Entry.build("subFuncCode", subFuncCode));
    }

    @Override
    public DynaBean writePcSubFuncRelationShowPermission(String funcCode, String subFuncCode, boolean checkDb) {
        if (checkDb) {
            DynaBean bean = metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatPcSubFuncRelationShowTemplate(funcCode, subFuncCode)));
            if (bean != null) {
                return bean;
            }
        }
        return writePcFuncPermissionWithoutOutput(PermissionTypeEnum.SUBFUNC_PC, PermissionPcFuncRelationTemplateEnum.SUBFUNC_PC_SHOW.getName(),
                formatPcSubFuncRelationShowTemplate(funcCode, subFuncCode), funcCode, PermissionOperationTypeEnum.SHOW);
    }

    @Override
    public DynaBean findPcSubFuncRelationShowPermission(String funcCode, String subFuncCode) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatPcSubFuncRelationShowTemplate(funcCode, subFuncCode)));
    }

    @Override
    public DynaBean writePcSubFuncRelationUpdatePermission(String funcCode, String subFuncCode, boolean checkDb) {
        if (checkDb) {
            DynaBean bean = metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatPcSubFuncRelationUpdateTemplate(funcCode, subFuncCode)));
            if (bean != null) {
                return bean;
            }
        }
        return writePcFuncPermissionWithoutOutput(PermissionTypeEnum.SUBFUNC_PC, PermissionPcFuncRelationTemplateEnum.SUBFUNC_PC_UPDATE.getName(),
                formatPcSubFuncRelationUpdateTemplate(funcCode, subFuncCode), funcCode, PermissionOperationTypeEnum.UPDATE);
    }

    @Override
    public DynaBean findPcSubFuncRelationUpdatePermission(String funcCode, String subFuncCode) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatPcSubFuncRelationUpdateTemplate(funcCode, subFuncCode)));
    }

    @Override
    public DynaBean writePcSubFuncRelationDeletePermission(String funcCode, String subFuncCode, boolean checkDb) {
        if (checkDb) {
            DynaBean bean = metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatPcSubFuncRelationDeleteTemplate(funcCode, subFuncCode)));
            if (bean != null) {
                return bean;
            }
        }
        return writePcFuncPermissionWithoutOutput(PermissionTypeEnum.SUBFUNC_PC, PermissionPcFuncRelationTemplateEnum.SUBFUNC_PC_DELETE.getName(),
                formatPcSubFuncRelationDeleteTemplate(funcCode, subFuncCode), funcCode, PermissionOperationTypeEnum.DELETE);
    }

    @Override
    public DynaBean findPcSubFuncRelationDeletePermission(String funcCode, String subFuncCode) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatPcSubFuncRelationDeleteTemplate(funcCode, subFuncCode)));
    }

    @Override
    public List<DynaBean> writePcSubFuncRelationPermission(String funcCode, String subFuncCode, boolean update, boolean delete) {
        //如果没有配置权限，则不给此功能的更改和删除权限
        List<String> permCodeList = Lists.newArrayList(
                formatPcSubFuncRelationShowTemplate(funcCode, subFuncCode)
        );
        if (update) {
            permCodeList.add(formatPcSubFuncRelationUpdateTemplate(funcCode, subFuncCode));
        }
        if (delete) {
            permCodeList.add(formatPcSubFuncRelationDeleteTemplate(funcCode, subFuncCode));
        }

        List<DynaBean> beanList = metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder().in("PERM_CODE", permCodeList));

        //此处逻辑在于校验单个权限的添加是否还需要校验，用于提升性能，不用每个添加都查询数据库
        DynaBean showBean = null;
        DynaBean updateBean = null;
        DynaBean deleteBean = null;
        for (DynaBean eachBean : beanList) {
            if (formatPcSubFuncRelationShowTemplate(funcCode, subFuncCode).equals(eachBean.getStr("PERM_CODE"))) {
                showBean = eachBean;
                continue;
            }
            //如果没有配置权限，则只操作展示权限
            if (update && formatPcSubFuncRelationUpdateTemplate(funcCode, subFuncCode).equals(eachBean.getStr("PERM_CODE"))) {
                updateBean = eachBean;
                continue;
            }
            if (delete && formatPcSubFuncRelationDeleteTemplate(funcCode, subFuncCode).equals(eachBean.getStr("PERM_CODE"))) {
                deleteBean = eachBean;
                continue;
            }
        }

        List<DynaBean> resultList = new ArrayList<>();
        if (showBean != null) {
            resultList.add(showBean);
        } else {
            resultList.add(writePcSubFuncRelationShowPermission(funcCode, subFuncCode, false));
        }

        if (update) {
            if (updateBean != null) {
                resultList.add(updateBean);
            } else {
                resultList.add(writePcSubFuncRelationUpdatePermission(funcCode, subFuncCode, false));
            }
        }

        if (delete) {
            if (deleteBean != null) {
                resultList.add(deleteBean);
            } else {
                resultList.add(writePcSubFuncRelationDeletePermission(funcCode, subFuncCode, false));
            }
        }

        return resultList;
    }

    @Override
    public List<DynaBean> findPcSubFuncRelationPermission(String funcCode, String subFuncCode, boolean update, boolean delete) {
        List<String> permCodeList = Lists.newArrayList(formatPcSubFuncRelationShowTemplate(funcCode, subFuncCode));
        if (update) {
            permCodeList.add(formatPcSubFuncRelationUpdateTemplate(funcCode, subFuncCode));
        }
        if (delete) {
            permCodeList.add(formatPcSubFuncRelationDeleteTemplate(funcCode, subFuncCode));
        }
        return metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder().in("PERM_CODE", permCodeList));
    }

    @Override
    public void modifyPermCode(String funcCode, String oldSubFuncCode, String newSubFuncCode) {
        List<String> permCodeList = Lists.newArrayList(formatPcSubFuncRelationShowTemplate(funcCode, oldSubFuncCode),
                formatPcSubFuncRelationUpdateTemplate(funcCode, oldSubFuncCode),
                formatPcSubFuncRelationDeleteTemplate(funcCode, oldSubFuncCode));
        List<DynaBean> oldSubFuncPermBeanList = metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder().in("PERM_CODE", permCodeList));
        for (int i = 0;oldSubFuncPermBeanList != null && !oldSubFuncPermBeanList.isEmpty() && i < oldSubFuncPermBeanList.size(); i++) {
            if (PermissionOperationTypeEnum.SHOW.getCode().equals(oldSubFuncPermBeanList.get(i).getStr("PERM_OPERATE_CODE"))) {
                oldSubFuncPermBeanList.get(i).set("PERM_CODE", formatPcSubFuncRelationShowTemplate(funcCode, newSubFuncCode));
            } else if (PermissionOperationTypeEnum.UPDATE.getCode().equals(oldSubFuncPermBeanList.get(i).getStr("PERM_OPERATE_CODE"))) {
                oldSubFuncPermBeanList.get(i).set("PERM_CODE", formatPcSubFuncRelationUpdateTemplate(funcCode, newSubFuncCode));
            } else if (PermissionOperationTypeEnum.DELETE.getCode().equals(oldSubFuncPermBeanList.get(i).getStr("PERM_OPERATE_CODE"))) {
                oldSubFuncPermBeanList.get(i).set("PERM_CODE", formatPcSubFuncRelationDeleteTemplate(funcCode, newSubFuncCode));
            }
            metaService.update(oldSubFuncPermBeanList.get(i));
        }
    }
}
