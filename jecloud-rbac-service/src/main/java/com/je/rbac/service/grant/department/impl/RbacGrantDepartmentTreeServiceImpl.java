/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.department.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.company.RbacDepartmentService;
import com.je.rbac.service.grant.department.RbacGrantDepartmentTreeService;
import com.je.rbac.service.permission.GrantTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class RbacGrantDepartmentTreeServiceImpl implements RbacGrantDepartmentTreeService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private RbacDepartmentService rbacDepartmentService;

    /**
     * 部门类型，设置check
     *
     * @param grantType
     * @param departmentIds
     * @param rootNode
     */
    @Override
    public void checkWithDepartment(GrantTypeEnum grantType, String departmentIds, JSONTreeNode rootNode) {
        List<String> departmentIdList = Splitter.on(",").splitToList(departmentIds);
        Set<String> extendDepartmentIdSet = rbacDepartmentService.findExtendIds(departmentIdList);
        List<String> allDepartmentIdList = new ArrayList<>();
        allDepartmentIdList.addAll(departmentIdList);
        allDepartmentIdList.addAll(extendDepartmentIdSet);

        //查询授权类型是菜单授权的集合
        List<Map<String,Object>> rolePermMapList = metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VDEPTPERM")
                .eq("DEPTPERM_GRANTTYPE_CODE", grantType.name())
                .in("JE_RBAC_DEPARTMENT_ID", allDepartmentIdList)
                .eq("PERM_OPERATE_CODE", "show")
                .in("PERM_TYPE_CODE", Lists.newArrayList("MENU", "FUNC_PC_BASE", "BUTTON_PC","SUBFUNC_PC")));
        recursiveDepartmentSetTreeCheck(rootNode, rolePermMapList, departmentIdList, extendDepartmentIdSet);
    }

    private void recursiveDepartmentSetTreeCheck(JSONTreeNode root, List<Map<String,Object>> beanList, List<String> currentDeptIdList, Set<String> extendDeptIdList) {
        if (root.getChildren() == null || root.getChildren().isEmpty()) {
            return;
        }

        boolean flag = false;
        for (JSONTreeNode eachChildNode : root.getChildren()) {
            for (Map<String,Object> eachPermedBean : beanList) {
                if (!eachPermedBean.get("PERM_CODE").equals(eachChildNode.getBean().get("PERM_SHOW"))) {
                    continue;
                }

                //注释参照角色
                if (!currentDeptIdList.contains(eachPermedBean.get("JE_RBAC_DEPARTMENT_ID"))
                        && extendDeptIdList.contains(eachPermedBean.get("JE_RBAC_DEPARTMENT_ID"))) {
                    eachChildNode.getBean().put("extend", true);
                    eachChildNode.getBean().put("extendByDepartment", true);
                }

                //设置是否排他权限,请注意逻辑：如果是排他，则设置权限为排他权限ID
                if ("1".equals(eachPermedBean.get("DEPTPERM_EXCLUDE_CODE"))) {
                    eachChildNode.getBean().put("exclude", true);
                    eachChildNode.getBean().put("DEPT_ID", eachPermedBean.get("JE_RBAC_DEPARTMENT_ID"));
                    eachChildNode.getBean().put("DEPT_NAME", eachPermedBean.get("DEPARTMENT_NAME"));
                    eachChildNode.getBean().put("PERM_ID", eachPermedBean.get("JE_RBAC_DEPTPERM_ID"));
                    if ("1".equals(eachPermedBean.get("DEPTPERM_NOT_CHECKED"))) {
                        eachChildNode.setChecked(false);
                    } else {
                        eachChildNode.setChecked(true);
                    }
                }

                //如果不是排他和继承的，则设置权限ID
                if (!eachChildNode.getBean().containsKey("exclude")) {
                    eachChildNode.getBean().put("exclude", false);
                    eachChildNode.getBean().put("DEPT_ID", eachPermedBean.get("JE_RBAC_DEPARTMENT_ID"));
                    eachChildNode.getBean().put("DEPT_NAME", eachPermedBean.get("DEPARTMENT_NAME"));
                    eachChildNode.getBean().put("PERM_ID", eachPermedBean.get("JE_RBAC_DEPTPERM_ID"));
                    eachChildNode.setChecked(true);
                }

                //如果非继承，则设置继承属性
                if (!eachChildNode.getBean().containsKey("extend")) {
                    eachChildNode.getBean().put("extend", false);
                }

                //如果flag不是true，并且checked，则flag应该是ture
                if (!flag && eachChildNode.getChecked()) {
                    flag = true;
                }
            }
            recursiveDepartmentSetTreeCheck(eachChildNode, beanList, currentDeptIdList, extendDeptIdList);
        }
        if (!flag && root.getBean().containsKey("PERM_ID")) {
            root.setChecked(true);
        } else {
            root.setChecked(flag);
        }
    }

}
