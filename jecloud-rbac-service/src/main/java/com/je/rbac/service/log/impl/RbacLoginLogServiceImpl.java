/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.log.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.DateUtils;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.log.RbacLoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class RbacLoginLogServiceImpl implements RbacLoginLogService {

    @Autowired
    private MetaService metaService;

    @Override
    public Map<String, Object> findLoginLogList(int start, int limit) {
        List<Map<String, Object>> records = metaService.selectSql(start, limit, "SELECT log.*,ac.JE_RBAC_ACCOUNT_ID,ac.ACCOUNT_AVATAR FROM JE_RBAC_LOGINLOG log LEFT JOIN je_rbac_account ac ON log.SY_ACCOUNT_ID = ac.JE_RBAC_ACCOUNT_ID ORDER BY log.SY_CREATETIME DESC");
        long count = metaService.countBySql("SELECT JE_RBAC_LOGINLOG_ID FROM JE_RBAC_LOGINLOG");
        Map<String, Object> result = new HashMap<>();
        result.put("records", records);
        result.put("total", count);
        result.put("start", start);
        result.put("limit", limit);
        return result;
    }

    @Override
    public void saveLoginLog(String departmentId, DynaBean accountBean, String device, String operation) {
        DynaBean departmentBean = metaService.selectOne("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder()
                .eq("JE_RBAC_DEPARTMENT_ID", departmentId));
        DynaBean logBean = new DynaBean("JE_RBAC_LOGINLOG", false);
        logBean.set("SY_ACCOUNT_ID", accountBean.getStr("JE_RBAC_ACCOUNT_ID"));
        logBean.set("LOGINLOG_ACCOUNT_NAME", accountBean.getStr("ACCOUNT_NAME"));
        logBean.set("LOGINLOG_ACCOUNT_CODE", accountBean.getStr("ACCOUNT_CODE"));
        if ("1".equals(operation)) {
            //基础登录
            logBean.set("LOGINLOG_TYPE_CODE", "basicLogin");//登录方式
            logBean.set("LOGINLOG_TYPE_NAME", "基础登录");
            logBean.set("LOGINLOG_OPTYPE_CODE", "IN");//操作类型
            logBean.set("LOGINLOG_OPTYPE_NAME", "登录");
        } else if ("2".equals(operation)) {
            //短信登录
            logBean.set("LOGINLOG_TYPE_CODE", "msgLogin");//登录方式
            logBean.set("LOGINLOG_TYPE_NAME", "短信登录");
            logBean.set("LOGINLOG_OPTYPE_CODE", "IN");//操作类型
            logBean.set("LOGINLOG_OPTYPE_NAME", "登录");
        } else if ("3".equals(operation)) {
            //注销
            logBean.set("LOGINLOG_OPTYPE_CODE", "OUT");//操作类型
            logBean.set("LOGINLOG_OPTYPE_NAME", "注销");
        }
        if (departmentBean != null) {
            logBean.set("SY_COMPANY_ID", departmentBean.getStr("SY_COMPANY_ID"));
            logBean.set("SY_COMPANY_NAME", departmentBean.getStr("SY_COMPANY_NAME"));
            logBean.set("SY_GROUP_COMPANY_ID", departmentBean.getStr("SY_GROUP_COMPANY_ID"));
            logBean.set("SY_GROUP_COMPANY_NAME", departmentBean.getStr("SY_GROUP_COMPANY_NAME"));
            logBean.set("SY_CREATEORGID", departmentBean.getStr("JE_RBAC_DEPARTMENT_ID"));
            logBean.set("SY_CREATEORGNAME", departmentBean.getStr("DEPARTMENT_NAME"));
        }

        logBean.set("SY_CREATETIME", DateUtils.formatDateTime(new Date()));
        logBean.set("LOGINLOG_DEVICE", device);
        metaService.insert(logBean);
    }

}
