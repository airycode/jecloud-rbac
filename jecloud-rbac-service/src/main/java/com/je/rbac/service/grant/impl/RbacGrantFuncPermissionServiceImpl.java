/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.meta.rpc.func.MetaFuncRpcService;
import com.je.rbac.service.grant.RbacGrantFuncPermissionService;
import com.je.rbac.service.grant.department.RbacGrantDepartmentPcFuncButtonPermmisionService;
import com.je.rbac.service.grant.department.RbacGrantDepartmentPcFuncPermmisionService;
import com.je.rbac.service.grant.department.RbacGrantDepartmentPcSubFuncPermmisionService;
import com.je.rbac.service.grant.develop.RbacGrantDevelopPcFuncButtonPermmisionService;
import com.je.rbac.service.grant.develop.RbacGrantDevelopPcFuncPermmisionService;
import com.je.rbac.service.grant.develop.RbacGrantDevelopPcSubFuncPermmisionService;
import com.je.rbac.service.grant.org.RbacGrantOrgPcFuncButtonPermmisionService;
import com.je.rbac.service.grant.org.RbacGrantOrgPcFuncPermmisionService;
import com.je.rbac.service.grant.org.RbacGrantOrgPcSubFuncPermmisionService;
import com.je.rbac.service.grant.permgroup.RbacGrantPermGroupPcFuncButtonPermmisionService;
import com.je.rbac.service.grant.permgroup.RbacGrantPermGroupPcFuncPermmisionService;
import com.je.rbac.service.grant.permgroup.RbacGrantPermGroupPcSubFuncPermmisionService;
import com.je.rbac.service.grant.role.RbacGrantRolePcFuncButtonPermmisionService;
import com.je.rbac.service.grant.role.RbacGrantRolePcFuncPermmisionService;
import com.je.rbac.service.grant.role.RbacGrantRolePcSubFuncPermmisionService;
import com.je.rbac.service.permission.GrantMethodEnum;
import com.je.rbac.service.permission.GrantTypeEnum;
import com.je.rbac.service.permission.PermissionLoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RbacGrantFuncPermissionServiceImpl implements RbacGrantFuncPermissionService {

    @Autowired
    private MetaFuncRpcService metaFuncRpcService;

    //---------------功能---------------
    @Autowired
    private RbacGrantRolePcFuncPermmisionService rbacGrantRolePcFuncPermmisionService;
    @Autowired
    private RbacGrantDepartmentPcFuncPermmisionService rbacGrantDepartmentPcFuncPermmisionService;
    @Autowired
    private RbacGrantOrgPcFuncPermmisionService rbacGrantOrgPcFuncPermmisionService;
    @Autowired
    private RbacGrantPermGroupPcFuncPermmisionService rbacGrantPermGroupPcFuncPermmisionService;
    @Autowired
    private RbacGrantDevelopPcFuncPermmisionService rbacGrantDevelopPcFuncPermmisionService;

    //-----------------子功能-----------------
    @Autowired
    private RbacGrantRolePcSubFuncPermmisionService rbacGrantRolePcSubFuncPermmisionService;
    @Autowired
    private RbacGrantDepartmentPcSubFuncPermmisionService rbacGrantDepartmentPcSubFuncPermmisionService;
    @Autowired
    private RbacGrantOrgPcSubFuncPermmisionService rbacGrantOrgPcSubFuncPermmisionService;
    @Autowired
    private RbacGrantPermGroupPcSubFuncPermmisionService rbacGrantPermGroupPcSubFuncPermmisionService;
    @Autowired
    private RbacGrantDevelopPcSubFuncPermmisionService rbacGrantDevelopPcSubFuncPermmisionService;

    //---------------功能按钮---------------
    @Autowired
    private RbacGrantRolePcFuncButtonPermmisionService rbacGrantRolePcFuncButtonPermmisionService;
    @Autowired
    private RbacGrantDepartmentPcFuncButtonPermmisionService rbacGrantDepartmentPcFuncButtonPermmisionService;
    @Autowired
    private RbacGrantOrgPcFuncButtonPermmisionService rbacGrantOrgPcFuncButtonPermmisionService;
    @Autowired
    private RbacGrantPermGroupPcFuncButtonPermmisionService rbacGrantPermGroupPcFuncButtonPermmisionService;
    @Autowired
    private RbacGrantDevelopPcFuncButtonPermmisionService rbacGrantDevelopPcFuncButtonPermmisionService;
    @Autowired
    private PermissionLoadService permissionLoadService;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateRoleFuncPermissions(GrantTypeEnum grantType, String roleIds, String addedStrData, String deleteStrData) {
        List<String> roleIdList = Splitter.on(",").splitToList(roleIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addFuncPermissionStrData(grantType, GrantMethodEnum.ROLE, roleIdList, addedStrData);
        }

        //移除的权限
        if (!Strings.isNullOrEmpty(deleteStrData)) {
            deleteFuncPermissionStrData(grantType, GrantMethodEnum.ROLE, roleIdList, deleteStrData);
        }
        permissionLoadService.reloadRolePermCache(roleIdList, false);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateDepartmentFuncPermissions(GrantTypeEnum grantType, String deptIds, String addedStrData, String deleteStrData) {
        List<String> deptIdList = Splitter.on(",").splitToList(deptIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addFuncPermissionStrData(grantType, GrantMethodEnum.DEPARTMENT, deptIdList, addedStrData);
        }

        //移除的权限
        if (!Strings.isNullOrEmpty(deleteStrData)) {
            deleteFuncPermissionStrData(grantType, GrantMethodEnum.DEPARTMENT, deptIdList, deleteStrData);
        }
        permissionLoadService.reloadDepartmentPermCache(deptIdList);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateOrgFuncPermissions(GrantTypeEnum grantType, String orgIds, String addedStrData, String deleteStrData) {
        List<String> orgIdList = Splitter.on(",").splitToList(orgIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addFuncPermissionStrData(grantType, GrantMethodEnum.ORG, orgIdList, addedStrData);
        }

        //移除的权限
        if (!Strings.isNullOrEmpty(deleteStrData)) {
            deleteFuncPermissionStrData(grantType, GrantMethodEnum.ORG, orgIdList, deleteStrData);
        }
        permissionLoadService.reloadOrgPermCache(orgIdList);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updatePermGroupFuncPermissions(GrantTypeEnum grantType, String permGroupIds, String addedStrData, String deleteStrData) {
        List<String> permGroupIdList = Splitter.on(",").splitToList(permGroupIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addFuncPermissionStrData(grantType, GrantMethodEnum.PERM_GROUP, permGroupIdList, addedStrData);
        }

        //移除的权限
        if (!Strings.isNullOrEmpty(deleteStrData)) {
            deleteFuncPermissionStrData(grantType, GrantMethodEnum.PERM_GROUP, permGroupIdList, deleteStrData);
        }
        permissionLoadService.reloadPermGroupPermCache(permGroupIdList, true);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateDevelopFuncPermissions(GrantTypeEnum grantType, String roleIds, String addedStrData, String deleteStrData) {
        List<String> roleIdList = Splitter.on(",").splitToList(roleIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addFuncPermissionStrData(grantType, GrantMethodEnum.DEVELOP, roleIdList, addedStrData);
        }

        //移除的权限
        if (!Strings.isNullOrEmpty(deleteStrData)) {
            deleteFuncPermissionStrData(grantType, GrantMethodEnum.DEVELOP, roleIdList, deleteStrData);
        }
        permissionLoadService.reloadDevelopRolePermCache(roleIdList);
    }

    private String getFuncCode(DynaBean funcBean) {
        if (funcBean == null) {
            return null;
        }
        return funcBean.getStr("FUNCINFO_FUNCCODE");
    }

    private void addFuncPermissionStrData(GrantTypeEnum grantType, GrantMethodEnum grantMethod, List<String> idList, String strData) {
        Map<String, JSONObject> subFuncCodeMap = filterSubFuncsFromPermissionStrData(strData);
        Map<String, DynaBean> subFuncIdAndFuncMap = metaFuncRpcService.findSubFuncParentFuncInfos(subFuncCodeMap.keySet().stream().collect(Collectors.toList()));
        for (String eachId : idList) {
            if (GrantMethodEnum.ROLE.equals(grantMethod)) {
                Map<String, String> granFuncCodeMap = filterFuncsFromPermissionStrDataWithCheck(strData);
                Map<String, Map<String, String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrDataWithCheck(strData);
                //功能
                for (String eachFuncCode : granFuncCodeMap.keySet()) {
                    rbacGrantRolePcFuncPermmisionService.saveRoleFuncShowPermission(eachId, eachFuncCode, granFuncCodeMap.get(eachFuncCode), grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantRolePcSubFuncPermmisionService.saveRoleSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), eachEntry.getValue().getString("checked"), grantType);
                }
                //功能按钮
                for (Map.Entry<String, Map<String, String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    rbacGrantRolePcFuncButtonPermmisionService.saveRoleFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            } else if (GrantMethodEnum.DEPARTMENT.equals(grantMethod)) {
                Map<String, String> granFuncCodeMap = filterFuncsFromPermissionStrDataWithCheck(strData);
                Map<String, Map<String, String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrDataWithCheck(strData);
                //功能
                for (String eachFuncCode : granFuncCodeMap.keySet()) {
                    rbacGrantDepartmentPcFuncPermmisionService.saveDeptFuncShowPermission(eachId, eachFuncCode, granFuncCodeMap.get(eachFuncCode), grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantDepartmentPcSubFuncPermmisionService.saveDeptSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), eachEntry.getValue().getString("checked"), grantType);
                }
                //功能按钮
                for (Map.Entry<String, Map<String, String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    rbacGrantDepartmentPcFuncButtonPermmisionService.saveDeptFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            } else if (GrantMethodEnum.ORG.equals(grantMethod)) {
                List<String> funcCodeList = filterFuncsFromPermissionStrData(strData);
                Map<String, List<String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrData(strData);
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantOrgPcFuncPermmisionService.saveOrgFuncShowPermission(eachId, eachFuncCode, grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantOrgPcSubFuncPermmisionService.saveOrgSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType);
                }
                //功能按钮
                for (Map.Entry<String, List<String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    rbacGrantOrgPcFuncButtonPermmisionService.saveOrgFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            } else if (GrantMethodEnum.PERM_GROUP.equals(grantMethod)) {
                List<String> funcCodeList = filterFuncsFromPermissionStrData(strData);
                Map<String, List<String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrData(strData);
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantPermGroupPcFuncPermmisionService.savePermGroupFuncShowPermission(eachId, eachFuncCode, grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantPermGroupPcSubFuncPermmisionService.savePermGroupSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType);
                }
                //功能按钮
                for (Map.Entry<String, List<String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    rbacGrantPermGroupPcFuncButtonPermmisionService.savePermGroupFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            } else if (GrantMethodEnum.DEVELOP.equals(grantMethod)) {
                List<String> funcCodeList = filterFuncsFromPermissionStrData(strData);
                Map<String, List<String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrData(strData);
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantDevelopPcFuncPermmisionService.saveDevelopFuncShowPermission(eachId, eachFuncCode, grantType);
                    rbacGrantDevelopPcFuncPermmisionService.saveDevelopFuncConfigPermission(eachId, eachFuncCode, grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantDevelopPcSubFuncPermmisionService.saveDevelopSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType);
                }
                //功能按钮
                for (Map.Entry<String, List<String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    rbacGrantDevelopPcFuncButtonPermmisionService.saveDevelopFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            }

        }
    }

    /**
     * 移除菜单和功能权限
     *
     * @param grantMethod
     * @param idList
     * @param strData
     */
    private void deleteFuncPermissionStrData(GrantTypeEnum grantType, GrantMethodEnum grantMethod, List<String> idList, String strData) {
        List<String> funcCodeList = filterFuncsFromPermissionStrData(strData);
        Map<String, JSONObject> subFuncCodeMap = filterSubFuncsFromPermissionStrData(strData);
        Map<String, DynaBean> subFuncIdAndFuncMap = metaFuncRpcService.findSubFuncParentFuncInfos(subFuncCodeMap.keySet().stream().collect(Collectors.toList()));
        List<String> funcButtonPermIdList = filterDeleteFuncButtonsFromPermissionStrData(strData);
        for (String eachId : idList) {
            if (GrantMethodEnum.ROLE.equals(grantMethod)) {
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantRolePcFuncPermmisionService.removeRoleFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantRolePcSubFuncPermmisionService.removeRoleSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                rbacGrantRolePcFuncButtonPermmisionService.removeRoleFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            } else if (GrantMethodEnum.DEPARTMENT.equals(grantMethod)) {
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantDepartmentPcFuncPermmisionService.removeDeptFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantDepartmentPcSubFuncPermmisionService.removeDeptSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                rbacGrantDepartmentPcFuncButtonPermmisionService.removeDeptFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            } else if (GrantMethodEnum.ORG.equals(grantMethod)) {
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantOrgPcFuncPermmisionService.removeOrgFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantOrgPcSubFuncPermmisionService.removeOrgSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                rbacGrantOrgPcFuncButtonPermmisionService.removeOrgFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            } else if (GrantMethodEnum.PERM_GROUP.equals(grantMethod)) {
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantPermGroupPcFuncPermmisionService.removePermGroupFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantPermGroupPcSubFuncPermmisionService.removePermGroupSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                rbacGrantPermGroupPcFuncButtonPermmisionService.removePermGroupFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            } else if (GrantMethodEnum.DEVELOP.equals(grantMethod)) {
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantDevelopPcFuncPermmisionService.removeDevelopFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantDevelopPcSubFuncPermmisionService.removeDevelopSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                rbacGrantDevelopPcFuncButtonPermmisionService.removeDevelopFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            }
        }

    }

    /**
     * 获取功能按钮集合
     *
     * @param strData
     * @return
     */
    private Map<String, List<String>> filterFuncButtonsFromPermissionStrData(String strData) {
        Map<String, List<String>> resultMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        List<String> buttonCodeList;
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if (!"button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if (resultMap.containsKey(strJsonArray.getJSONObject(i).getString("funcCode"))) {
                buttonCodeList = resultMap.get(strJsonArray.getJSONObject(i).getString("funcCode"));
            } else {
                buttonCodeList = new ArrayList<>();
                resultMap.put(strJsonArray.getJSONObject(i).getString("funcCode"), buttonCodeList);
            }
            buttonCodeList.add(strJsonArray.getJSONObject(i).getString("nodeInfo"));
        }
        return resultMap;
    }

    /**
     * 获取删除功能按钮集合
     *
     * @param strData
     * @return
     */
    private List<String> filterDeleteFuncButtonsFromPermissionStrData(String strData) {
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        List<String> buttonPermIdList = new ArrayList<>();
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if (!"button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            buttonPermIdList.add(strJsonArray.getJSONObject(i).getString("permId"));
        }
        return buttonPermIdList;
    }

    private Map<String, Map<String, String>> filterFuncButtonsFromPermissionStrDataWithCheck(String strData) {
        Map<String, Map<String, String>> resultMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        Map<String, String> buttonCodeMap;
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if (!"button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if (resultMap.containsKey(strJsonArray.getJSONObject(i).getString("funcCode"))) {
                buttonCodeMap = resultMap.get(strJsonArray.getJSONObject(i).getString("funcCode"));
            } else {
                buttonCodeMap = new HashMap<>();
                resultMap.put(strJsonArray.getJSONObject(i).getString("funcCode"), buttonCodeMap);
            }
            buttonCodeMap.put(strJsonArray.getJSONObject(i).getString("nodeInfo"),
                    strJsonArray.getJSONObject(i).containsKey("checked") ? strJsonArray.getJSONObject(i).getString("checked") : null);
        }
        return resultMap;
    }

    /**
     * 获取功能集合
     *
     * @param strData
     * @return
     */
    private List<String> filterFuncsFromPermissionStrData(String strData) {
        List<String> funcCodeList = new ArrayList<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if ("button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if ("MT".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))
                    || "commonSubFunc".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                funcCodeList.add(strJsonArray.getJSONObject(i).getString("nodeInfo"));
            }
        }
        return funcCodeList;
    }

    private Map<String, String> filterFuncsFromPermissionStrDataWithCheck(String strData) {
        Map<String, String> funcCodeMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if ("button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if ("MT".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))
                    || "commonSubFunc".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                funcCodeMap.put(strJsonArray.getJSONObject(i).getString("nodeInfo"),
                        strJsonArray.getJSONObject(i).containsKey("checked") ? strJsonArray.getJSONObject(i).getString("checked") : null);
            }
        }
        return funcCodeMap;
    }

    /**
     * @param strData
     * @return Map<String, String> key为subFunc的id,value为当前json对象
     */
    private Map<String, JSONObject> filterSubFuncsFromPermissionStrData(String strData) {
        Map<String, JSONObject> funcCodeMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if (!"diySubFunc".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            funcCodeMap.put(strJsonArray.getJSONObject(i).getString("id"), strJsonArray.getJSONObject(i));
        }
        return funcCodeMap;
    }

}
