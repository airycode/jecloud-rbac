/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant;

import com.je.rbac.service.permission.GrantTypeEnum;

/**
 * 授权服务定义
 */
public interface RbacGrantFuncPermissionService {

    /**
     * 更新角色功能权限
     *
     * @param addedStrData  增加的权限
     * @param deleteStrData 移除的权限
     */
    void updateRoleFuncPermissions(GrantTypeEnum grantType, String roleIds, String addedStrData, String deleteStrData);

    /**
     * 更新部门功能权限
     *
     * @param deptIds       部门ID集合
     * @param addedStrData  增加的权限
     * @param deleteStrData 移除的权限
     */
    void updateDepartmentFuncPermissions(GrantTypeEnum grantType, String deptIds, String addedStrData, String deleteStrData);

    /**
     * 更新机构功能权限
     *
     * @param orgIds        机构ID集合
     * @param addedStrData  增加的权限
     * @param deleteStrData 移除的权限
     */
    void updateOrgFuncPermissions(GrantTypeEnum grantType, String orgIds, String addedStrData, String deleteStrData);

    /**
     * 更新权限组功能权限
     *
     * @param permGroupIds  权限组ID集合
     * @param addedStrData  增加的权限
     * @param deleteStrData 移除的权限
     */
    void updatePermGroupFuncPermissions(GrantTypeEnum grantType, String permGroupIds, String addedStrData, String deleteStrData);

    /**
     * 更新开发组功能权限
     *
     * @param addedStrData  增加的权限
     * @param deleteStrData 移除的权限
     */
    void updateDevelopFuncPermissions(GrantTypeEnum grantType, String roleIds, String addedStrData, String deleteStrData);

}
