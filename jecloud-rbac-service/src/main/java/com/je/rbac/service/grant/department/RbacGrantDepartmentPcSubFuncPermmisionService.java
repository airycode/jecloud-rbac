/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.department;

import com.je.common.base.DynaBean;
import com.je.rbac.exception.PermissionException;
import com.je.rbac.service.permission.GrantTypeEnum;

import java.util.List;

/**
 * 部门-子功能授权
 */
public interface RbacGrantDepartmentPcSubFuncPermmisionService {

    /**
     * 保存部门子功能权限
     *
     * @param deptId         部门ID
     * @param funcCode       功能编码
     * @param excludeChecked
     */
    void saveDeptSubFuncShowPermission(String deptId, String funcCode,String subFuncCode, String excludeChecked, GrantTypeEnum grantType);

    /**
     * 移除角色的子功能权限
     *
     * @param deptId         部门ID
     * @param funcCode       功能编码
     * @param update         是否包含更新权限
     * @param delete 是否包含删除权限
     */
    void removeDeptSubFuncPermission(String deptId, String funcCode,String subFuncCode,GrantTypeEnum grantType, boolean update,boolean delete);

    /**
     * 查找子功能权限
     *
     * @param deptId 部门ID
     * @param funcCode 功能编码
     * @param update 是否包含更新权限
     * @param delete 是否包含删除权限
     * @return
     */
    List<DynaBean> findDeptSubFuncPermission(String deptId, String funcCode,String subFuncCode, GrantTypeEnum grantType, boolean update, boolean delete);

    /**
     * 查找部门和父级部门的权限
     * @param deptId 部门ID
     * @param funcCode 功能编码
     * @param update 是否包含更新权限
     * @param delete 是否包含删除权限
     * @return
     */
    List<DynaBean> findExtendedDeptSubFuncPermission(String deptId, String funcCode,String subFuncCode,GrantTypeEnum grantType, boolean update,boolean delete) throws PermissionException;

}
