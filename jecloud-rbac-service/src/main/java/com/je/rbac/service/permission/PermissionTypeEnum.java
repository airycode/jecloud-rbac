/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission;

/**
 * 权限类型枚举
 */
public enum PermissionTypeEnum {

    /**
     * 菜单权限
     */
    MENU("菜单权限"),
    /**
     * PC端功能按钮权限
     */
    BUTTON_PC("PC按钮权限"),
    /**
     * PC端功能权限
     */
    FUNC_PC_BASE("PC功能基础权限"),
    /**
     * PC端子功能权限
     */
    SUBFUNC_PC("PC子功能基础权限"),
    /**
     * 功能数据权限
     */
    FUNC_DATAS("功能快速数据权限"),
    /**
     * 功能配置权限
     */
    FUNC_PC_CONFIG("功能配置权限"),
    /**
     * PC端功能权限
     */
    PLUGIN_PC_BASE("PC插件基础权限"),
    /**
     * 插件配置权限
     */
    PLUGIN_PC_CONFIG("插件权限"),
    /**
     * 插件数据权限
     */
    PLUGIN_DATA("插件数据权限"),
    /**
     * 顶部菜单数据权限
     */
    TOPMENU_DATA("顶部菜单数据权限");

    private String desc;

    public String getDesc() {
        return desc;
    }

    PermissionTypeEnum(String desc) {
        this.desc = desc;
    }
}
