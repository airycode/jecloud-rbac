/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.role;

public interface RbacAccountRoleService {

    /**
     * 更新账户角色
     *
     * @param accountId       账户ID
     * @param originalRoleIds 原始的角色id集合
     * @param currentRoleIds  现在的角色id集合
     * @param deptId       关联部门id
     * @param deptName       关联部门名称
     * @param deptUserCode       人员部门标识：主分标识
     */
    void updateAccountRoles(String accountId, String originalRoleIds, String currentRoleIds,String deptId,String deptName,String deptUserCode);

    /**
     * 增加用户角色
     *
     * @param accountId
     * @param roleIds
     * @param deptId       关联部门id
     * @param deptName       关联部门名称
     */
    void addAccountRoles(String accountId,String deptId,String deptName, String roleIds,String deptUserCode);

    /**
     * 基础部门人员角色【在更改部门人员角色时，要同步更新账号的角色】
     *
     * @param accountId
     * @param roleIds
     * @param deptId       关联部门id
     * @param deptName       关联部门名称
     */
    long removeAccountRoles(String accountId,String deptId,String deptName, String roleIds);

}
