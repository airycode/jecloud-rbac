/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.account.accountLoginCheckFilter;
import com.je.rbac.service.company.userCheckFilter.Response;
import javax.servlet.http.HttpServletRequest;

public abstract class AbstractCommonAcountHandler {

    private AbstractCommonAcountHandler nextHandler;

    //账号--》人员--》部门--》公司

    //4.校验账号状态是否正常
    //5.校验账号是否锁定
    //6.校验账号有效期
    //8.获取部门异常
    //9.该人员部门未开通访问权限
    //10.获取人员部门异常

    /**
     * 责任链的下一个对象
     */
    public void setNextHandler(AbstractCommonAcountHandler nextHandler){
        this.nextHandler = nextHandler;
    }

    /**
     * 具体参数拦截逻辑,给子类去实现
     */
    public void filter(HttpServletRequest request, Response response) {
        doFilter(request, response);
        if (!response.isSuccess()){
            return;
        }
        if (getNextHandler() != null) {
            getNextHandler().filter(request, response);
        }
    }

    abstract void doFilter(HttpServletRequest request, Response response);

    public AbstractCommonAcountHandler getNextHandler() {
        return nextHandler;
    }


}
