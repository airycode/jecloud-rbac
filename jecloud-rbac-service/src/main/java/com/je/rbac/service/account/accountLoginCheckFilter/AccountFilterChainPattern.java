/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.account.accountLoginCheckFilter;

import com.je.rbac.service.company.userCheckFilter.Response;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class AccountFilterChainPattern {

    private List<AbstractCommonAcountHandler> filters = new ArrayList<>();

    private AbstractCommonAcountHandler abstractHandler;

    public AccountFilterChainPattern add(AbstractCommonAcountHandler filter) {
        filters.add(filter);
        return this;
    }

    //先调用这个方法 将责任链的对象连接起来
    public AccountFilterChainPattern initializeChainFilter() {
        for (int i = 0; i < filters.size(); i++) {
            if (i == 0) {
                abstractHandler = filters.get(0);
            } else {
                AbstractCommonAcountHandler currentHander = filters.get(i - 1);
                AbstractCommonAcountHandler nextHander = filters.get(i);
                currentHander.setNextHandler(nextHander);
            }
        }
        return this;
    }

    //直接调用这个方法使用
    public Response exec(HttpServletRequest request, Response response) {
        abstractHandler.filter(request, response);
        return response;
    }
}
