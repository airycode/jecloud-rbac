/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template;

import com.je.common.base.DynaBean;

/**
 * 插件基础权限
 */
public interface PcPluginBaseService {

    /**
     * 格式化PC插件权限模板
     * @param pluginCode
     * @return
     */
    String formatPcPluginConfigTemplate(String pluginCode);

    /**
     * 写入功能加载权限
     * @param pluginCode
     * @param checkDb 是否校验数据库
     * @return
     */
    DynaBean writePcPluginConfigPermission(String pluginCode, boolean checkDb);

    /**
     * 查找功能展示权限
     * @param pluginCode
     * @return
     */
    DynaBean findPcPluginConfigPermission(String pluginCode);

    /**
     * 格式化PC插件权限模板
     * @param pluginCode
     * @return
     */
    String formatPcPluginShowTemplate(String pluginCode);

    /**
     * 写入功能加载权限
     * @param pluginCode
     * @param checkDb 是否校验数据库
     * @return
     */
    DynaBean writePcPluginShowPermission(String pluginCode, boolean checkDb);

    /**
     * 查找功能展示权限
     * @param pluginCode
     * @return
     */
    DynaBean findPcPluginShowPermission(String pluginCode);

}
