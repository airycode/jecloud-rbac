/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.role;

import com.google.common.base.Strings;
import com.je.common.auth.impl.role.Role;
import com.je.common.base.DynaBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 角色服务定义
 */
public interface RbacRoleService {

    /**
     * 查找角色
     * @param id
     * @return
     */
    DynaBean findById(String id);

    /**
     * 根据ID查找并构建角色
     * @param id
     * @return
     */
    default Role buildRoleById(String id){
        if(Strings.isNullOrEmpty(id)){
            return null;
        }
        return buildRole(findById(id));
    }

    /**
     * 查找角色
     * @param roleIds
     * @return
     */
    List<DynaBean> findByIds(List<String> roleIds);

    /**
     * 根据角色ID集合构建角色集合
     * @param roleIds
     * @return
     */
    default List<Role> buildRoleByIds(List<String> roleIds){
        if(roleIds == null || roleIds.isEmpty()){
            return null;
        }
        return buildRoles(findByIds(roleIds));
    }

    /**
     * 批量构建角色
     * @param roleBeans
     * @return
     */
    default List<Role> buildRoles(List<DynaBean> roleBeans){
        if(roleBeans == null || roleBeans.isEmpty()){
            return null;
        }
        List<Role> roles = new ArrayList<>();
        for (DynaBean eachBean : roleBeans) {
            roles.add(buildRole(eachBean));
        }
        return roles;
    }

    /**
     * 根据角色Bean构建角色
     * @param roleBean
     * @return
     */
    default Role buildRole(DynaBean roleBean){
        if(roleBean == null){
            return null;
        }
        Role role = new Role();
        role.parse(roleBean.getValues());
        return role;
    }

    /**
     * 查找角色集成的所有角色
     * @param roleIds
     * @return
     */
    Set<String> findExtendRoleIds(List<String> roleIds);

    /**
     * 查找角色集成的所有角色
     * @param roleIds
     * @return
     */
    List<DynaBean> findExtendRoles(List<String> roleIds);

    /**
     * 查找子角色
     * @param roleIds
     * @return
     */
    List<String> findChildrenRoleIds(List<String> roleIds);

    /**
     * 查找角色用户
     * @param roleIds
     * @return
     */
    List<String> findRoleUserIds(List<String> roleIds);

}
