/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.org.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.impl.CommonServiceImpl;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.grant.org.RbacGrantOrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RbacGrantOrgServiceImpl implements RbacGrantOrgService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonServiceImpl commonService;

    @Override
    public JSONTreeNode findOrgTree() {
        return null;
    }

    @Override
    public void updateUserRoleName(String roleId, String roleName) {
        List<DynaBean> orgs = metaService.select("JE_RBAC_VACCOUNTROLE", ConditionsWrapper.builder()
                        .eq("ACCOUNTROLE_ROLE_ID", roleId)
                        .ne("SY_ORG_ID", "systemdepartment")
                        .groupBy("SY_ORG_ID")
                , "SY_ORG_ID"
        );
        for (DynaBean dynaBean : orgs) {
            DynaBean orgInfo = metaService.selectOneByPk("JE_RBAC_ORG", dynaBean.getStr("SY_ORG_ID"));
            String tableCode = orgInfo.getStr("ORG_RESOURCETABLE_CODE");
            String roleNameFiledCode = orgInfo.getStr("ORG_ROLEFIELD_NAME");
            String roleIdFiledCode = orgInfo.getStr("ORG_ROLEFIELD_ID");
            String pkCode = orgInfo.getStr("ORG_FIELD_PK");
            commonService.updateDictionaryItemNameById(roleId, roleName, tableCode, roleNameFiledCode, roleIdFiledCode, pkCode);
        }

    }

}
