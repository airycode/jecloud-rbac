/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.threemember.impl;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.StringUtil;
import com.je.common.base.util.TreeUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.func.MetaFuncRpcService;
import com.je.rbac.service.grant.role.RbacGrantRoleTreeService;
import com.je.rbac.service.permission.GrantTypeEnum;
import com.je.rbac.service.permission.template.PcFuncBaseService;
import com.je.rbac.service.permission.template.PcFuncButtonService;
import com.je.rbac.service.permission.template.PcMenuDataService;
import com.je.rbac.service.permission.template.PcSubFuncRelationService;
import com.je.rbac.service.threemember.ThreeMemberAuthorizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ThreeMemberAuthorizeServiceImpl implements ThreeMemberAuthorizeService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private MetaFuncRpcService metaFuncRpcService;
    @Autowired
    private BeanService beanService;
    @Autowired
    private PcFuncBaseService pcFuncBaseService;
    @Autowired
    private PcFuncButtonService pcFuncButtonService;
    @Autowired
    private PcSubFuncRelationService pcSubFuncRelationService;
    @Autowired
    private RbacGrantRoleTreeService rbacGrantRoleTreeService;
    @Autowired
    private PcMenuDataService pcMenuDataService;

    @Override
    public List<Map<String, Object>> loadTopMenus(String ids) {
        List<DynaBean> roleResourceBeans = metaService.select("JE_RBAC_RESOURCESCOPE", ConditionsWrapper.builder()
                .in("RESOURCESCOPE_ROLE_ID", Splitter.on(",").splitToList(ids)));
        List<String> menuIdList = new ArrayList<>();
        for (int i = 0; roleResourceBeans != null && i < roleResourceBeans.size(); i++) {
            menuIdList.addAll(Splitter.on(",").splitToList(roleResourceBeans.get(i).getStr("RESOURCESCOPE_MENU_ID")));
        }

        if (menuIdList == null || menuIdList.isEmpty()) {
            return new ArrayList<>();
        }

        List<DynaBean> menuBeanList = metaService.select("JE_CORE_MENU",ConditionsWrapper.builder().in("JE_CORE_MENU_ID",menuIdList));
        List<String> newMenuIdList = new ArrayList<>();

        for (DynaBean eachMenuBean : menuBeanList) {
            if(Strings.isNullOrEmpty(eachMenuBean.getStr("SY_PATH"))){
                continue;
            }
            newMenuIdList.addAll(Splitter.on("/").splitToList(eachMenuBean.getStr("SY_PATH")));
        }

        ConditionsWrapper wrapper = ConditionsWrapper.builder().table("JE_CORE_MENU").eq("SY_PARENT", "ROOT");
        wrapper.in("JE_CORE_MENU_ID",newMenuIdList);
        wrapper.orderByAsc("SY_TREEORDERINDEX");
        List<Map<String, Object>> scopedMenuList = metaService.selectSql(wrapper);
        return scopedMenuList;
    }

    @Override
    public JSONTreeNode loadMenuPermissions(String ids, String topMenuId) {
        List<DynaBean> resourceScopeBeanList = metaService.select("JE_RBAC_RESOURCESCOPE", ConditionsWrapper.builder().in("RESOURCESCOPE_ROLE_ID", Splitter.on(",").splitToList(ids)));
        List<String> menuIdList = new ArrayList<>();
        for (int i = 0; resourceScopeBeanList != null && i < resourceScopeBeanList.size(); i++) {
            menuIdList.addAll(Splitter.on(",").splitToList(resourceScopeBeanList.get(i).getStr("RESOURCESCOPE_MENU_ID")));
        }

        if (menuIdList == null || menuIdList.isEmpty()) {
            return TreeUtil.buildRootNode();
        }

        List<DynaBean> tempMenuBeanList = metaService.select("JE_CORE_MENU",ConditionsWrapper.builder().in("JE_CORE_MENU_ID",menuIdList));
        List<String> newMenuIdList = new ArrayList<>();
        for (DynaBean eachMenuBean : tempMenuBeanList) {
            if(Strings.isNullOrEmpty(eachMenuBean.getStr("SY_PATH"))){
                continue;
            }
            newMenuIdList.addAll(Splitter.on("/").splitToList(eachMenuBean.getStr("SY_PATH")));
        }

        List<DynaBean> menuBeanList = metaService.select("JE_CORE_MENU", ConditionsWrapper.builder()
                .like("SY_PATH", topMenuId)
                .in("JE_CORE_MENU_ID", newMenuIdList)
                .orderByAsc("SY_TREEORDERINDEX"));
        List<String> funcCodeList = new ArrayList<>();
        for (DynaBean eachBean : menuBeanList) {
            if ("MT".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                funcCodeList.add(eachBean.getStr("MENU_NODEINFO"));
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->功能");
            } else if ("MENU".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->菜单");
            } else if ("IFRAME".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->IFRAME");
            } else if ("IDDT".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->自定义");
            } else if ("PORTAL".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->门户");
            } else if ("REPORT".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->报表");
            } else if ("URL".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->超链接");
            } else if ("CHART".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->图表");
            } else if ("DIC".equals(eachBean.getStr("MENU_NODEINFOTYPE"))) {
                eachBean.set("MENU_MENUNAME", eachBean.getStr("MENU_MENUNAME") + "->字典功能");
            }
        }

        Map<String, List<DynaBean>> funcResult = metaFuncRpcService.findPermedFuncsAndChildFuncsWithButtonsByFuncCodes(funcCodeList);
        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        DynaBean table = beanService.getResourceTable("JE_CORE_MENU");
        List<DynaBean> columns = (List<DynaBean>) table.get(BeanService.KEY_TABLE_COLUMNS);
        JSONTreeNode template = beanService.buildJSONTreeNodeTemplate(columns);
        JSONTreeNode node;
        for (DynaBean eachMenuBean : menuBeanList) {
            if (topMenuId.equals(eachMenuBean.getStr("JE_CORE_MENU_ID"))) {
                node = buildTreeNode(template, "ROOT", eachMenuBean);
                if ("MT".equals(node.getNodeInfoType())) {
                    node.setIcon("jeicon jeicon-function");
                }
                node.setNodeInfo(topMenuId);
                rootNode.getChildren().add(node);
            }
        }

        //递归形成树形结构
        for (JSONTreeNode eachNode : rootNode.getChildren()) {
            recursiveJsonTreeNode(template, eachNode, menuBeanList);
        }

        //功能
        List<DynaBean> mainFuncList = funcResult.get("main");
        //子功能
        List<DynaBean> childFuncRelationList = funcResult.get("child");
        //按钮
        List<DynaBean> funcButtonList = funcResult.get("button") == null ? new ArrayList<>() : funcResult.get("button");
        //子功能所属功能
        List<String> subFuncRelationIdList = new ArrayList<>();
        if (childFuncRelationList != null && !childFuncRelationList.isEmpty()) {
            childFuncRelationList.forEach(each -> {
                subFuncRelationIdList.add(each.getStr("JE_CORE_FUNCRELATION_ID"));
            });
        }
        Map<String, DynaBean> funcIdCodeMap = metaFuncRpcService.findSubFuncParentFuncInfos(subFuncRelationIdList);

        //主功能
        for (DynaBean eachFuncBean : mainFuncList) {
            //此处设置功能展示权限，用于check校验
            eachFuncBean.set("PERM_SHOW", pcFuncBaseService.formatPcFuncShowTemplate(eachFuncBean.getStr("FUNCINFO_FUNCCODE")));
            //设置功能按钮的功能属性，用于构建树形
            for (DynaBean eachButtonBean : funcButtonList) {
                if (eachFuncBean.getStr("JE_CORE_FUNCINFO_ID").equals(eachButtonBean.getStr("RESOURCEBUTTON_FUNCINFO_ID"))) {
                    eachButtonBean.set("FUNCINFO_FUNCCODE", eachFuncBean.getStr("FUNCINFO_FUNCCODE"));
                    //此处生成展示权限编码，用于check校验
                    eachButtonBean.set("PERM_SHOW", pcFuncButtonService.formatPcFuncButtonShowTemplate(eachFuncBean.getStr("FUNCINFO_FUNCCODE"),
                            eachButtonBean.getStr("RESOURCEBUTTON_CODE")));
                }
            }
        }

        //子功能
        for (int i = 0; childFuncRelationList != null && !childFuncRelationList.isEmpty() && i < childFuncRelationList.size(); i++) {
            //此处设置子功能展示权限，用于check校验
            if ("func".equals(childFuncRelationList.get(i).getStr("FUNCRELATION_RELYONTYPE"))) {
                childFuncRelationList.get(i).set("PERM_SHOW", pcSubFuncRelationService.formatPcSubFuncRelationShowTemplate(getFuncCode(funcIdCodeMap.get(childFuncRelationList.get(i).getStr("JE_CORE_FUNCRELATION_ID"))), childFuncRelationList.get(i).getStr("FUNCRELATION_CODE")));
                //设置功能按钮的功能属性，用于构建树形
                for (DynaBean eachButtonBean : funcButtonList) {
                    if (childFuncRelationList.get(i).getStr("FUNCRELATION_FUNCID").equals(eachButtonBean.getStr("RESOURCEBUTTON_FUNCINFO_ID"))) {
                        eachButtonBean.set("FUNCINFO_FUNCCODE", childFuncRelationList.get(i).getStr("FUNCRELATION_CODE"));
                        //此处生成展示权限编码，用于check校验
                        eachButtonBean.set("PERM_SHOW", pcFuncButtonService.formatPcFuncButtonShowTemplate(childFuncRelationList.get(i).getStr("FUNCRELATION_CODE"),
                                eachButtonBean.getStr("RESOURCEBUTTON_CODE")));
                    }
                }
            } else {
                childFuncRelationList.get(i).set("PERM_SHOW", pcSubFuncRelationService.formatPcSubFuncRelationShowTemplate(getFuncCode(funcIdCodeMap.get(childFuncRelationList.get(i).getStr("JE_CORE_FUNCRELATION_ID"))), childFuncRelationList.get(i).getStr("FUNCRELATION_CODE")));
            }
        }

        //递归设置功能按钮和子功能
        recursiveSetMenuFuncInfo(rootNode, mainFuncList, childFuncRelationList, funcButtonList);
        //权限校验check
        rbacGrantRoleTreeService.checkWithRole(GrantTypeEnum.MENU, ids, rootNode);
        return rootNode;
    }

    private String getFuncCode(DynaBean funcBean) {
        if (funcBean == null) {
            return null;
        }
        return funcBean.getStr("FUNCINFO_FUNCCODE");
    }

    private void recursiveSetMenuFuncInfo(JSONTreeNode rootNode, List<DynaBean> mainFuncInfoBeanList, List<DynaBean> childFuncRelationBeanList, List<DynaBean> funcButtonBeanList) {
        if (rootNode.getChildren().isEmpty()) {
            return;
        }

        //由于一直在追加，递归会重新递归已经设置好的功能和子功能，此处的逻辑是已经递归设置的不再重新递归，放置重复计算
        if (!"ROOT".equals(rootNode.getId())
                && "MT".equals(rootNode.getNodeInfoType())
                && !rootNode.getBean().containsKey("JE_CORE_MENU_ID")) {
            return;
        }

        for (JSONTreeNode eachChildNode : rootNode.getChildren()) {
            //如果是功能，则进行递归设置
            if ("MT".equals(eachChildNode.getNodeInfoType())) {
                for (DynaBean eachMainFuncBean : mainFuncInfoBeanList) {
                    if (!eachMainFuncBean.getStr("FUNCINFO_FUNCCODE").equals(eachChildNode.getNodeInfo())) {
                        continue;
                    }
                    eachChildNode.setBean(eachMainFuncBean.getValues());
                    eachChildNode.setIcon("jeicon jeicon-function");
                    for (DynaBean eachButtonBean : funcButtonBeanList) {
                        if (eachButtonBean.getStr("FUNCINFO_FUNCCODE").equals(eachChildNode.getNodeInfo())) {
                            eachChildNode.getChildren().add(buildButtonTreeNode(eachButtonBean, eachChildNode.getId()));
                        }
                    }
                    buildSubFuncTreeNode(eachChildNode, childFuncRelationBeanList, funcButtonBeanList);
                }
            }
            recursiveSetMenuFuncInfo(eachChildNode, mainFuncInfoBeanList, childFuncRelationBeanList, funcButtonBeanList);
        }
    }

    private void buildSubFuncTreeNode(JSONTreeNode parentNode, List<DynaBean> childFuncRelationBeanList, List<DynaBean> funcButtonBeanList) {
        String parentBeanId;
        for (int i = 0; childFuncRelationBeanList != null && !childFuncRelationBeanList.isEmpty() && i < childFuncRelationBeanList.size(); i++) {
            if ("commonSubFunc".equals(parentNode.getNodeInfoType())) {
                parentBeanId = (String) parentNode.getBean().get("JE_CORE_FUNCRELATION_ID");
            } else {
                parentBeanId = parentNode.getBean().get("JE_CORE_FUNCINFO_ID") == null ? null : (String) parentNode.getBean().get("JE_CORE_FUNCINFO_ID");
            }
            if (!Strings.isNullOrEmpty(parentBeanId) && childFuncRelationBeanList.get(i).getStr("FUNCRELATION_FUNCINFO_ID").equals(parentBeanId)) {
                JSONTreeNode node = new JSONTreeNode();
                node.setId(childFuncRelationBeanList.get(i).getStr("JE_CORE_FUNCRELATION_ID"));
                node.setParent(parentNode.getId());
                node.setLeaf(true);
                node.setText(childFuncRelationBeanList.get(i).getStr("FUNCRELATION_NAME") + "->功能");
                node.setCode(childFuncRelationBeanList.get(i).getStr("FUNCRELATION_CODE"));
                node.setIcon("jeicon jeicon-function");
                node.setNodeType("GENERAL");
                node.setNodeInfo(childFuncRelationBeanList.get(i).getStr("FUNCRELATION_CODE"));
                node.setBean(childFuncRelationBeanList.get(i).getValues());
                node.setChecked(false);
                if ("func".equals(childFuncRelationBeanList.get(i).getStr("FUNCRELATION_RELYONTYPE"))) {
                    node.setNodeInfoType("commonSubFunc");
                    for (DynaBean eachButtonBean : funcButtonBeanList) {
                        if (eachButtonBean.getStr("FUNCINFO_FUNCCODE").equals(node.getNodeInfo())) {
                            node.getChildren().add(buildButtonTreeNode(eachButtonBean, childFuncRelationBeanList.get(i).getStr("FUNCRELATION_FUNCINFO_ID")));
                        }
                    }
                } else {
                    node.setNodeInfoType("diySubFunc");
                }
                parentNode.getChildren().add(node);
                if (!"diySubFunc".equals(node.getNodeInfoType())) {
                    buildSubFuncTreeNode(node, childFuncRelationBeanList, funcButtonBeanList);
                }
            }
        }
    }

    private JSONTreeNode buildButtonTreeNode(DynaBean buttonBean, String parentId) {
        JSONTreeNode node = new JSONTreeNode();
        node.setId(buttonBean.getStr("JE_CORE_RESOURCEBUTTON_ID"));
        node.setParent(parentId);
        node.setLeaf(true);
        if ("GRID".equals(buttonBean.getStr("RESOURCEBUTTON_TYPE"))) {
            node.setText(buttonBean.getStr("RESOURCEBUTTON_NAME") + "->列表");
        } else if ("FORM".equals(buttonBean.getStr("RESOURCEBUTTON_TYPE"))) {
            node.setText(buttonBean.getStr("RESOURCEBUTTON_NAME") + "->表单");
        } else if ("ACTION".equals(buttonBean.getStr("RESOURCEBUTTON_TYPE"))) {
            node.setText(buttonBean.getStr("RESOURCEBUTTON_NAME") + "->ACTION");
        }
        node.setCode(buttonBean.getStr("RESOURCEBUTTON_CODE"));
        node.setIcon("fal fa-mouse");
        node.setNodeType("LEAF");
        node.setNodeInfo(buttonBean.getStr("RESOURCEBUTTON_CODE"));
        node.setNodeInfoType("button");
        node.setBean(buttonBean.getValues());
        node.setChecked(false);
        return node;
    }

    private void recursiveJsonTreeNode(JSONTreeNode template, JSONTreeNode rootNode, List<DynaBean> list) {
        if ("MT".equals(rootNode.getNodeInfoType())) {
            rootNode.setIcon("jeicon jeicon-function");
        }

        if (list == null || list.isEmpty()) {
            return;
        }

        JSONTreeNode treeNode;
        for (DynaBean eachBean : list) {
            if (rootNode.getId().equals(eachBean.getStr("SY_PARENT"))) {
                treeNode = buildTreeNode(template, rootNode.getId(), eachBean);
                if ("MENU".equals(treeNode.getNodeInfoType())) {
                    treeNode.setNodeInfo(treeNode.getId());
                }
                rootNode.getChildren().add(treeNode);
            }
        }

        if (rootNode.getChildren() == null || rootNode.getChildren().isEmpty()) {
            rootNode.setLeaf(true);
            return;
        }

        rootNode.setLeaf(false);

        for (JSONTreeNode eachNode : rootNode.getChildren()) {
            recursiveJsonTreeNode(template, eachNode, list);
        }
    }

    /**
     * 构建树形节点
     *
     * @param template     模板
     * @param parentNodeId 父节点ID
     * @param bean         当前bean
     * @return
     */
    private JSONTreeNode buildTreeNode(JSONTreeNode template, String parentNodeId, DynaBean bean) {
        JSONTreeNode node = new JSONTreeNode();
        node.setId(bean.getPkValue());
        node.setParent(parentNodeId);
        node.setLeaf(true);
        if (StringUtil.isNotEmpty(template.getText())) {
            node.setText(bean.getStr(template.getText()));
        }
        if (StringUtil.isNotEmpty(template.getCode())) {
            node.setCode(bean.getStr(template.getCode()));
        }
        //节点信息
        if (StringUtil.isNotEmpty(template.getNodeInfo())) {
            node.setNodeInfo(bean.getStr(template.getNodeInfo()));
        }
        //节点信息类型
        if (StringUtil.isNotEmpty(template.getNodeInfoType())) {
            node.setNodeInfoType(bean.getStr(template.getNodeInfoType()));
        }
        //节点类型
        if (StringUtil.isNotEmpty(template.getNodeType())) {
            node.setNodeType(bean.getStr(template.getNodeType()));
        }
        //节点路径
        if (StringUtil.isNotEmpty(template.getNodePath())) {
            node.setNodePath(bean.getStr(template.getNodePath()));
        }
        //是否禁用
        if (StringUtil.isNotEmpty(template.getDisabled())) {
            node.setDisabled(bean.getStr(template.getDisabled()));
        }
        //树形排序
        if (StringUtil.isNotEmpty(template.getTreeOrderIndex())) {
            node.setTreeOrderIndex(bean.getStr(template.getTreeOrderIndex()));
        }
        //节点类型
        if (StringUtil.isNotEmpty(template.getNodeType())) {
            node.setNodeType(bean.getStr(template.getNodeType()));
        }
        //图标样式
        if (StringUtil.isNotEmpty(template.getIcon())) {
            node.setIcon(bean.getStr(template.getIcon()));
        }
        //是否禁用
        if (StringUtil.isNotEmpty(template.getDisabled())) {
            node.setDisabled(bean.getStr(template.getDisabled()));
        } else {
            node.setDisabled("0");
        }
        //描述
        if (StringUtil.isNotEmpty(template.getDescription())) {
            node.setDescription(bean.getStr(template.getDescription()));
        }
        //排序
        if (StringUtil.isNotEmpty(template.getOrderIndex())) {
            node.setOrderIndex(bean.getStr(template.getOrderIndex()) + "");
        }
        node.setChecked(false);
        if (!"MT".equals(node.getNodeInfoType())) {
            bean.set("PERM_SHOW", pcMenuDataService.formatMenuDataShowTemplate(bean.getStr("JE_CORE_MENU_ID")));
        }
        node.setBean(bean.getValues());
        return node;
    }

}
