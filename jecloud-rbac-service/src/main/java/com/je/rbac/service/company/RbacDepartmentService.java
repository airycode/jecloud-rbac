/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.company;

import com.je.common.auth.impl.Department;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.meta.model.dd.DicInfoVo;
import com.je.rbac.exception.DepartmentException;
import com.je.rbac.exception.DepartmentUserException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

public interface RbacDepartmentService {

    List<String> findAllPathDepartments(String departmentIds);


    /***
     * 同步修改部门人员表主管标识
     * @param dynaBean
     */
    void updateSyncMajorUser(DynaBean dynaBean);

    /***
     * 校验部门所属公司，上级信息是否发生变化; 若发生变化则移动该部门
     * @param dynaBean  原数据
     * @param dynaBean  新数据
     * @return
     */
    void checkAndMoveDeptCompanyInfo(DynaBean sourceDeptBean , DynaBean dynaBean) throws DepartmentException;

    /**
     * 导入已有员工同时在本部门任职
     * @param departmentId
     * @param userIds
     */
    void addOwnDepartment(String departmentId,String userIds) throws DepartmentUserException;


    /***
     * 查询部门员工
     * @param param
     * @param request
     */
    Page loadDepartmentUser(BaseMethodArgument param, HttpServletRequest request);

    /***
     * 保存
     * @param dynaBean
     */
    DynaBean doSave(DynaBean dynaBean) throws DepartmentUserException;
    /**
     * 检查机构名称是否唯一
     * @param deptBean
     * @return
     */
    boolean checkDeptNameUnique(DynaBean deptBean);

    /**
     * 检查机构编码是否唯一
     * @param deptBean
     * @return
     */
    boolean checkDeptCodeUnique(DynaBean deptBean);

    /**
     * 修改部门名称
     * @param deptBean
     * @return
     */
    void updateDeptName(DynaBean deptBean);

    /**
     * 修改时检查是否唯一
     * @param deptBean
     * @return
     */
    boolean updateCheckUnique(DynaBean deptBean);

    /**
     * 判断上级是否被禁用
     * @param departmentIds
     * @return
     */
    Boolean findUpDeptStatus(String departmentIds) throws DepartmentException;

    /**
     * 判断上级公司是否被禁用
     * @param departmentIds
     * @return
     */
    Boolean findUpCompanyStatus(String departmentIds) throws DepartmentException;



    /**
     * 检查部门下是否有子部门
     * @param deptIds
     * @return
     */
    boolean checkChildDept(String deptIds);

    /**
     * 检查部门下是否有关联人员
     * @param deptIds
     * @return
     */
    boolean checkChildUser(String deptIds);

    /**
     * 启用部门需校验所属公司状态是否启用
     * @param departmentIds
     * @return
     */
    boolean checkCompanyStatus(String departmentIds);

    /**
     * 查找所有部门
     * @return
     */
    List<DynaBean> findAll();

    /**
     * 根据ID查找
     * @param id
     * @return
     */
    DynaBean findById(String id);


    /**
     * 根据ID查找Model
     * @param id
     * @return
     */
    default Department findDepartmentModelById(String id){
        DynaBean deptBean = findById(id);
        Department department = new Department();
        department.parse(deptBean.getValues());
        return department;
    }

    /**
     * 根据Code查找
     * @param code
     * @return
     */
    DynaBean findByCode(String code);

    /**
     * 根据Code查找Model
     * @param code
     * @return
     */
    default Department findDepartmentModelByCode(String code){
        DynaBean deptBean = findByCode(code);
        Department department = new Department();
        department.parse(deptBean.getValues());
        return department;
    }

    /**
     * 根据Name查找
     * @param name
     * @return
     */
    DynaBean findByName(String name);

    /**
     * 根据Name查找Model
     * @param name
     * @return
     */
    default Department findDepartmentModelByName(String name){
        DynaBean deptBean = findByName(name);
        Department department = new Department();
        department.parse(deptBean.getValues());
        return department;
    }

    /**
     * 查找父级部门
     * @param id
     * @return
     */
    DynaBean findParent(String id);

    /**
     * 根据parent查找Model
     * @param parent
     * @return
     */
    default Department findDepartmentModelByParent(String parent){
        DynaBean deptBean = findParent(parent);
        Department department = new Department();
        department.parse(deptBean.getValues());
        return department;
    }

    /**
     * 查找祖先部门
     * @param id
     * @return
     */
    DynaBean findAncestors(String id);

    /**
     * 查找祖先部门Model
     * @param id
     * @return
     */
    default Department findDepartmentAncestorsModel(String id){
        DynaBean deptBean = findAncestors(id);
        Department department = new Department();
        department.parse(deptBean.getValues());
        return department;
    }

    /**
     * 根据租户ID查找
     * @param tenantId
     * @return
     */
    List<DynaBean> findByTenantId(String tenantId);

    /**
     * 根据租户Name查找
     * @param tenantName
     * @return
     */
    List<DynaBean> findByTenantName(String tenantName);

    /**
     * 查找下一层的部门
     * @param id
     * @return
     */
    List<DynaBean> findNextChildren(String id,boolean containMe);

    /**
     * 查找所有子部门
     * @param id
     * @param containMe
     * @return
     */
    List<DynaBean> findAllChildren(String id,boolean containMe);

    /**
     * 根据主管查找
     * @param majorId
     * @return
     */
    List<DynaBean> findByMajorId(String majorId);

    /**
     * 查找公司部门
     * @param companyIds
     * @return
     */
    List<DynaBean> findCompanyAllDepartments(String... companyIds);

    /**
     * 查找公司部门
     * @param companyId
     * @return
     */
    List<DynaBean> findCompanyAllDepartments(String companyId);

    /**
     * 查找公司下顶级部门
     * @param companyId
     * @return
     */
    List<DynaBean> findCompanyFirstLevelDepartements(String companyId);

    /**
     * 查找集团公司所有部门
     * @param groupCompanyId
     * @return
     */
    List<DynaBean> findGroupCompanyAllDepartments(String groupCompanyId);

    /**
     * 查找集团公司下公司顶级部门
     * @param groupCompanyId
     * @return
     */
    List<DynaBean> findCompanyFirstLevelDepartments(String groupCompanyId);

    /**
     * 查找继承的部门
     * @param departmentIds
     * @return
     */
    Set<String> findExtendIds(List<String> departmentIds);

    /**
     * 禁用部门
     * @param departmentIds
     */
    void disableDepartments(String departmentIds);

    /**
     * 启用部门
     * @param departmentIds
     */
    void enableDepartments(String departmentIds);

    /**
     * 删除部门集合
     * @param departmentIds
     * @param withUsers 是否同时删除部门人员
     * @return
     */
    long removeDepartments(String departmentIds,boolean withUsers);

    /**
     * 部门移除人员
     * @param userDeptIds 用户部门集合
     */
    void removeDepartmentUser(String userDeptIds);

    /**
     * 查找部门领导
     * @param departmentId
     * @return
     */
    String findDeptLeader(String departmentId);

    /**
     * 获取公司部门树
     * @return
     */
    JSONTreeNode buildCompanyTreeData(Boolean colorFlag, String... companyIds);

    /**
     * 通过条件获取公司部门树
     * @return
     */
    JSONTreeNode buildCompanyTreeDataByQuery(Boolean colorFlag, BaseMethodArgument param);


    /***
     * 获取流程使用
     * @param companyIds
     * @return
     */
    JSONTreeNode buildOneCompanyTreeData(String... companyIds);

    /**
     * 获取公司部门树
     * @return
     */
//    JSONTreeNode loadCompanyTreeData(String... companyIds);

    /**
     * 获取公司树
     * @return
     */
    JSONTreeNode buildOnlyCompanyTreeData(Boolean colorFlag);
    /**
     * 获取公司树（是否显示禁用的）
     * forbidden 是否显示禁用的公司
     * @return
     */
    JSONTreeNode buildForbiddenCompanyTreeData(Boolean colorFlag, Boolean forbidden);

    JSONTreeNode buildDeptTreeForDictionary(DicInfoVo dicInfoVo);
    /**
     * 获取公司部门树
     * @return
     */
    JSONTreeNode buildCompanyDeptTreeData(DicInfoVo dicInfoVo);

    /**
     * 获取公司部门人员树
     * @return
     */
    JSONTreeNode buildCompanyDeptUserTreeData(DicInfoVo dicInfoVo);

    /**
     * 角色人员树
     */
    JSONTreeNode buildRoleUserTree(DicInfoVo dicInfoVo);
}
