/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template;

import com.je.common.base.DynaBean;
import java.util.List;

/**
 * 子功能自定义权限
 */
public interface PcSubFuncRelationService {

    /**
     * 格式化PC子功能权限模板
     * @param funcCode
     * @return
     */
    String formatPcSubFuncRelationShowTemplate(String funcCode,String subFuncCode);

    /**
     * 格式化PC子功能权限模板
     * @param funcCode
     * @return
     */
    String formatPcSubFuncRelationUpdateTemplate(String funcCode,String subFuncCode);

    /**
     * 格式化子功能权限模板
     * @param funcCode
     * @return
     */
    String formatPcSubFuncRelationDeleteTemplate(String funcCode,String subFuncCode);

    /**
     * 写入子功能加载权限
     * @param funcCode
     * @param checkDb 是否校验数据库
     * @return
     */
    DynaBean writePcSubFuncRelationShowPermission(String funcCode,String subFuncCode, boolean checkDb);

    /**
     * 查找子功能展示权限
     * @param funcCode
     * @return
     */
    DynaBean findPcSubFuncRelationShowPermission(String funcCode,String subFuncCode);

    /**
     * 写入子功能更新权限
     * @param funcCode
     * @return
     */
    DynaBean writePcSubFuncRelationUpdatePermission(String funcCode,String subFuncCode,boolean checkDb);

    /**
     * 查找子功能展示权限
     * @param funcCode
     * @return
     */
    DynaBean findPcSubFuncRelationUpdatePermission(String funcCode,String subFuncCode);

    /**
     * 写入子功能删除权限
     * @param funcCode
     * @return
     */
    DynaBean writePcSubFuncRelationDeletePermission(String funcCode,String subFuncCode,boolean checkDb);

    /**
     * 查找子功能展示权限
     * @param funcCode
     * @return
     */
    DynaBean findPcSubFuncRelationDeletePermission(String funcCode,String subFuncCode);

    /**
     * 写入子功能本身权限
     * @param funcCode
     * @param update 是否包含更新权限
     * @param delete 是否包含删除权限
     * @return
     */
    List<DynaBean> writePcSubFuncRelationPermission(String funcCode,String subFuncCode, boolean update, boolean delete);

    /**
     * 查找子功能功能本身权限
     * @param funcCode
     * @param update 是否包含更新权限
     * @param delete 是否包含删除权限
     * @return
     */
    List<DynaBean> findPcSubFuncRelationPermission(String funcCode,String subFuncCode,boolean update,boolean delete);

    /**
     * 更改功能编码
     * @param funcCode
     * @param oldSubFuncCode
     * @param newSubFuncCode
     */
    void modifyPermCode(String funcCode,String oldSubFuncCode,String newSubFuncCode);
}
