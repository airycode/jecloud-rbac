/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.develop.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.JEUUID;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.exception.RoleException;
import com.je.rbac.service.grant.develop.RbacGrantDevelopService;
import com.je.rbac.service.grant.role.RbacGrantRoleService;
import com.je.rbac.service.grant.role.impl.RbacGrantRoleServiceImpl;
import com.je.rbac.service.role.RbacRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.je.rbac.service.role.impl.RbacRoleServiceImpl.*;

@Service
public class RbacGrantDevelopServiceImpl implements RbacGrantDevelopService {

    @Autowired
    private MetaService metaService;

    @Override
    @Transactional(rollbackFor = {RuntimeException.class, RoleException.class})
    public DynaBean insertFolder(String folderName, String iconCls, String remark) {
        List<Map<String, Object>> countList = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) AS MAX_COUNT FROM JE_RBAC_ROLE");
        int count = (int) countList.get(0).get("MAX_COUNT");
        String folderId = JEUUID.uuid();
        DynaBean folderBean = new DynaBean("JE_RBAC_ROLE", false);
        folderBean.set("JE_RBAC_ROLE_ID", folderId);
        folderBean.set("ROLE_NAME", folderName);
        folderBean.set("ROLE_CODE", "folder-develop-" + String.format("%06d", count + 1));
        folderBean.set("ROLE_TYPE_CODE", FOLDER_TYPE_CODE);
        folderBean.set("ROLE_TYPE_NAME", FOLDER_TYPE_NAME);
        folderBean.set("ROLE_REMARK", remark);
        //树形节点值
        folderBean.set("SY_PARENT", "ROOT");
        folderBean.set("SY_PARENTPATH", "/ROOT");
        folderBean.set("SY_LAYER", 1);
        folderBean.set("SY_STATUS", "1");
        folderBean.set("SY_NODETYPE", "GENERAL");
        folderBean.set("SY_PATH", "/ROOT/" + folderId);
        folderBean.set("SY_TREEORDERINDEX", "000001" + String.format("%06d", count + 1));
        //创建时间
        folderBean.set("SY_CREATETIME", DateUtils.formatDateTime(new Date()));
        folderBean.set("ROLE_ICONCLS", iconCls);
        metaService.insert(folderBean);
        return folderBean;
    }

    @Override
    @Transactional(rollbackFor = {RuntimeException.class, RoleException.class})
    public void updateFolder(String folderId, String folderName) {
        metaService.executeSql("UPDATE JE_RBAC_ROLE SET ROLE_NAME = {0} WHERE JE_RBAC_ROLE_ID = {1}", folderName, folderId);
    }

    @Override
    @Transactional(rollbackFor = {RuntimeException.class, RoleException.class})
    public void removeFolder(String folderId) throws RoleException {
        long count = metaService.countBySql("SELECT JE_RBAC_ROLE_ID FROM JE_RBAC_ROLE WHERE SY_PARENT = {0}", folderId);
        if (count > 0) {
            throw new RoleException("文件夹下存在角色，请先移除角色！");
        }
        metaService.delete("JE_RBAC_ROLE", ConditionsWrapper.builder().eq("JE_RBAC_ROLE_ID", folderId));
    }

}
