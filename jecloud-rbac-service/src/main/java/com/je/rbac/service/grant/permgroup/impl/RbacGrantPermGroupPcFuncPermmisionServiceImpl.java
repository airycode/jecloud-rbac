/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.permgroup.impl;

import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.grant.permgroup.RbacGrantPermGroupPcFuncPermmisionService;
import com.je.rbac.service.permission.GrantMethodEnum;
import com.je.rbac.service.permission.GrantTypeEnum;
import com.je.rbac.service.permission.PermissionOperationTypeEnum;
import com.je.rbac.service.permission.PermissionTypeEnum;
import com.je.rbac.service.permission.template.PcFuncBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RbacGrantPermGroupPcFuncPermmisionServiceImpl implements RbacGrantPermGroupPcFuncPermmisionService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private PcFuncBaseService pcFuncBaseService;
    @Autowired
    private CommonService commonService;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void savePermGroupFuncShowPermission(String permGroupId, String funcCode, GrantTypeEnum grantType) {
        //写入基础权限
        List<DynaBean> funcBasePermissionList = pcFuncBaseService.writePcFuncBasePermission(funcCode, false, false,false);
        //查询已存在的关联关系
        List<String> permissionIdList = new ArrayList<>();
        for (DynaBean eachPermissionBean : funcBasePermissionList) {
            permissionIdList.add(eachPermissionBean.getStr("JE_RBAC_PERM_ID"));
        }
        List<DynaBean> associationList = metaService.select("JE_RBAC_PERMGROUPPERM", ConditionsWrapper.builder()
                .eq("PERMGROUPPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_PERMGROUP_ID", permGroupId)
                .in("JE_RBAC_PERM_ID", permissionIdList));
        List<String> havedRolePermissionList = new ArrayList<>();
        for (DynaBean eachAssociationBean : associationList) {
            havedRolePermissionList.add(eachAssociationBean.getStr("JE_RBAC_PERM_ID"));
        }
        //写入关联关系
        DynaBean rolePermBean;
        for (DynaBean eachPermissionBean : funcBasePermissionList) {
            //如果已包含从关联关系，则不做任何操作
            if (havedRolePermissionList.contains(eachPermissionBean.getStr("JE_RBAC_PERM_ID"))) {
                continue;
            }
            rolePermBean = new DynaBean("JE_RBAC_PERMGROUPPERM", false);
            rolePermBean.set("JE_RBAC_PERMGROUP_ID", permGroupId);
            rolePermBean.set("JE_RBAC_PERM_ID", eachPermissionBean.getStr("JE_RBAC_PERM_ID"));
            //授权类型
            rolePermBean.set("PERMGROUPPERM_GRANTTYPE_CODE", grantType.name());
            rolePermBean.set("PERMGROUPPERM_GRANTTYPE_NAME", grantType.getDesc());
            //todo 设置租户信息

            commonService.buildModelCreateInfo(eachPermissionBean);
            metaService.insert(rolePermBean);
        }
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void removePermGroupFuncPermission(String permGroupId, String funcCode, GrantTypeEnum grantType, boolean update, boolean delete) {
        List<DynaBean> permissionBaseBeanList = pcFuncBaseService.findPcFuncBasePermission(funcCode, update, delete,false);
        List<String> permIdList = new ArrayList<>();
        for (DynaBean eachBean : permissionBaseBeanList) {
            permIdList.add(eachBean.getStr("JE_RBAC_PERM_ID"));
        }
        metaService.delete("JE_RBAC_PERMGROUPPERM", ConditionsWrapper.builder()
                .eq("PERMGROUPPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_PERMGROUP_ID", permGroupId)
                .in("JE_RBAC_PERM_ID", permIdList));
    }

    @Override
    public List<DynaBean> findPermGroupFuncPermission(String permGroupId, String funcCode, GrantTypeEnum grantType, boolean update, boolean delete) {
        List<String> operatorList = Lists.newArrayList(PermissionOperationTypeEnum.SHOW.getCode());
        if (update) {
            operatorList.add(PermissionOperationTypeEnum.UPDATE.getCode());
        }
        if (delete) {
            operatorList.add(PermissionOperationTypeEnum.DELETE.getCode());
        }
        return metaService.select("JE_RBAC_VPERMGROUP", ConditionsWrapper.builder()
                .eq("PERMGROUPPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_PERMGROUP_ID", permGroupId)
                .eq("PERM_OBJECT", funcCode)
                .in("PERM_TYPE_CODE", Lists.newArrayList(
                        PermissionTypeEnum.FUNC_PC_BASE.name(),
                        PermissionTypeEnum.BUTTON_PC.name()
                )).in("PERM_OPERATE_CODE", operatorList));
    }

}
