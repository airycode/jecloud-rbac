/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.remove.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.impl.move.AbstractMoveService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.MessageUtils;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.remove.CompanyDeptMoveService;
import com.je.rbac.service.remove.MoveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class CompanyDeptMoveServiceImpl implements CompanyDeptMoveService {

    @Autowired
    private MetaService metaService;

    @Override
    @Transactional(rollbackFor = {RuntimeException.class})
    public DynaBean move(DynaBean parameter) {
        String tableCode = "JE_RBAC_VCOMPANYDEPT";
        String id = parameter.getStr("id");
        String toId = parameter.getStr("toId");
        String place = parameter.getStr("place");
        String beanName = "";
        DynaBean resourceBean = metaService.selectOne(tableCode, ConditionsWrapper.builder().eq("ID",id));
        DynaBean targetBean = metaService.selectOne(tableCode,ConditionsWrapper.builder().eq("ID",toId));
        if (null == targetBean) {
            throw new PlatformException(MessageUtils.getMessage("table.move.notNode"), PlatformExceptionEnum.JE_CORE_TABLE_UPDATE_ERROR);
        }
        if (place.equals(com.je.common.base.service.impl.move.AbstractMoveService.ABOVE)) {
            beanName = "rmoveAboveService";
        }

        if (place.equals(com.je.common.base.service.impl.move.AbstractMoveService.BELOW)) {
            beanName = "rmoveBelowService";
        }
        //获取最后一个，变成放到最后一个资源的下面
        if (place.equals(AbstractMoveService.INSIDE)) {
            beanName = "rmoveInsideService";
            String parent = targetBean.getPkValue();
            //查公司部门视图同等级最大treeOrderIndex值
            List<DynaBean> list = metaService.select("JE_RBAC_VCOMPANYDEPT", 0, 1,
                    ConditionsWrapper.builder().eq("SY_PARENT", parent).orderByDesc("SY_ORDERINDEX"));
            if (list.size() > 0) {
                targetBean = list.get(0);
                beanName = "rmoveBelowService";
            }
        }

        MoveService moveService = SpringContextHolder.getBean(beanName);
        moveService.move(tableCode, id, toId, resourceBean, targetBean);
        return null;
    }
}