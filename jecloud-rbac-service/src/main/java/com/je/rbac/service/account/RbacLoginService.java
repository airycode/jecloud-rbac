/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.account;

import com.je.common.base.DynaBean;
import com.je.rbac.exception.AccountException;
import com.je.rbac.exception.LoginException;
import java.util.List;
import java.util.Map;

/**
 * 登录服务定义
 */
public interface RbacLoginService {

    /**
     * 检查部门
     * @param accountBean
     * @return
     */
    List<Map<String,Object>> checkDepartment(DynaBean accountBean) throws LoginException;

    /**
     * 检查该用户部门是否允许访问系统
     * @param accountId
     * @param deptId
     * @return
     */
    DynaBean findAccountDept(String accountId,String deptId);

    /**
     * 检查用户是否登录
     * @return
     */
    boolean checkLogin();

    /**
     * 发送验证码
     * @param accountBean
     * @return
     */
    String sendRandom(DynaBean accountBean,String device) throws LoginException;

    /**
     * 校验验证码
     * @param accountBean
     * @param captchaNum
     * @return
     */
    boolean checkRandom(DynaBean accountBean,String captchaNum,String device);

    /**
     * 登录
     * @param departmentId 部门ID
     * @param accountBean 账户bean
     * @param device 设备类型
     * @return token
     */
    String login(String tenantId,String departmentId, DynaBean accountBean, String device,String operation) throws AccountException;

    /**
     * 注销
     * @return
     */
    boolean logout();

    /**
     * 根据token进行注销
     * @param token
     * @return
     */
    void logoutByToken(String token);

    /**
     * 根据账号id踢人
     * @param userId
     * @return
     */
    boolean kickout(String userId,String device);
}
