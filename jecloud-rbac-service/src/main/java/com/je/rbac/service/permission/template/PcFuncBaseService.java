/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template;

import com.je.common.base.DynaBean;
import java.util.List;
import java.util.Map;

/**
 * PC功能基础权限模板服务
 */
public interface PcFuncBaseService {

    /**
     * 格式化PC功能权限模板
     * @param funcCode
     * @return
     */
    String formatPcFuncConfigTemplate(String funcCode);

    /**
     * 格式化PC功能权限模板
     * @param funcCode
     * @return
     */
    String formatPcFuncShowTemplate(String funcCode);

    /**
     * 格式化PC功能权限模板
     * @param funcCode
     * @return
     */
    String formatPcFuncUpdateTemplate(String funcCode);

    /**
     * 格式化PC功能权限模板
     * @param funcCode
     * @return
     */
    String formatPcFuncDeleteTemplate(String funcCode);

    /**
     * 写入功能加载权限
     * @param funcCode
     * @param checkDb 是否校验数据库
     * @return
     */
    DynaBean writePcFuncConfigPermission(String funcCode,boolean checkDb);

    /**
     * 查找功能展示权限
     * @param funcCode
     * @return
     */
    DynaBean findPcFuncConfigPermission(String funcCode);

    /**
     * 写入功能加载权限
     * @param funcCode
     * @param checkDb 是否校验数据库
     * @return
     */
    DynaBean writePcFuncShowPermission(String funcCode,boolean checkDb);

    /**
     * 查找功能展示权限
     * @param funcCode
     * @return
     */
    DynaBean findPcFuncShowPermission(String funcCode);

    /**
     * 写入功能更新权限
     * @param funcCode
     * @return
     */
    DynaBean writePcFuncUpdatePermission(String funcCode,boolean checkDb);

    /**
     * 查找功能展示权限
     * @param funcCode
     * @return
     */
    DynaBean findPcFuncUpdatePermission(String funcCode);

    /**
     * 写入功能删除权限
     * @param funcCode
     * @return
     */
    DynaBean writePcFuncDeletePermission(String funcCode,boolean checkDb);

    /**
     * 查找功能展示权限
     * @param funcCode
     * @return
     */
    DynaBean findPcFuncDeletePermission(String funcCode);

    /**
     * 写入功能本身权限
     * @param funcCode
     * @param update 是否包含更新权限
     * @param delete 是否包含删除权限
     * @return
     */
    List<DynaBean> writePcFuncBasePermission(String funcCode,boolean update,boolean delete,boolean config);

    /**
     * 查找功能本身权限
     * @param funcCode
     * @param update 是否包含更新权限
     * @param delete 是否包含删除权限
     * @return
     */
    List<DynaBean> findPcFuncBasePermission(String funcCode,boolean update,boolean delete,boolean config);
    
    /**
     * 更改权限编码
     * @param oldFuncCode
     * @param newFuncCode
     */
    void modifyPermCode(String oldFuncCode,String newFuncCode);
}
