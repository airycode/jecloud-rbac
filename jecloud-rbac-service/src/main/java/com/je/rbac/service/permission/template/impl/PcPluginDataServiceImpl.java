/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template.impl;

import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.permission.PermissionOperationTypeEnum;
import com.je.rbac.service.permission.PermissionTypeEnum;
import com.je.rbac.service.permission.template.PcPluginDataService;
import com.je.rbac.service.permission.template.PermissionPluginDataTemplateEnum;
import org.springframework.stereotype.Service;

@Service
public class PcPluginDataServiceImpl extends AbstractPermissionTemplateService implements PcPluginDataService {

    @Override
    public String formatProductDataPermissionTemplate(String pluginCode, String productCode) {
        return formatTemplate(PermissionPluginDataTemplateEnum.PLUGIN_DATA_PRODUCT.getTemplate(),
                Entry.build("pluginCode", pluginCode), Entry.build("productCode", productCode));
    }

    @Override
    public DynaBean writeProductDataPermission(String pluginCode, String productCode, boolean checkDb) {
        if (checkDb) {
            DynaBean bean = metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatProductDataPermissionTemplate(pluginCode,productCode)));
            if (bean != null) {
                return bean;
            }
        }
        return writeDataPermission(PermissionTypeEnum.PLUGIN_DATA, PermissionPluginDataTemplateEnum.PLUGIN_DATA_PRODUCT.getName(),
                formatProductDataPermissionTemplate(pluginCode,productCode),pluginCode,
                true,
                PermissionPluginDataTemplateEnum.PLUGIN_DATA_PRODUCT.getOutputTemplate(),
                PermissionOperationTypeEnum.DATA);
    }

    @Override
    public DynaBean findProductDataPermission(String pluginCode, String productCode) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder()
                .eq("PERM_CODE", formatProductDataPermissionTemplate(pluginCode, productCode)));
    }

}
