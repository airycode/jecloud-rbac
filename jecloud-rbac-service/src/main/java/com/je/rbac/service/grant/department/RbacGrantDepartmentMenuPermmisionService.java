/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.department;

import com.je.rbac.service.permission.GrantTypeEnum;

import java.util.List;
import java.util.Map;

public interface RbacGrantDepartmentMenuPermmisionService {

    /**
     * 保存部门菜单权限
     *
     * @param deptId
     * @param menuId
     */
    void saveDeptMenuPermission(String deptId, String menuId,String excludeChecked, GrantTypeEnum grantType);

    /**
     * 移除部门菜单权限
     *
     * @param deptId
     * @param menuId
     */
    void removeDeptMenuPermission(String deptId, String menuId, GrantTypeEnum grantType, boolean update, boolean delete);

    /**
     * 保存部门菜单权限
     *
     * @param deptId
     * @param menuIdMap
     */
    void saveDeptMenuPermission(String deptId, Map<String,String> menuIdMap, GrantTypeEnum grantType);

    /**
     * 移除部门菜单权限
     *
     * @param deptId
     * @param menuIdList
     */
    void removeDeptMenuPermission(String deptId, List<String> menuIdList, GrantTypeEnum grantType, boolean update, boolean delete);

}
