/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.menu;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.rpc.PermissionRpcService;
import com.je.rbac.service.permission.template.PcMenuDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/rbac/menu/history")
public class HistoryController extends AbstractPlatformController {

    @Autowired
    private PermissionRpcService permissionRpcService;
    @Autowired
    private PcMenuDataService pcMenuDataService;
    @Autowired
    private MetaResourceService metaResourceService;

    @RequestMapping(value = "/loadUserMenuHistory", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadUserMenuHistory(HttpServletRequest request) {
        String departmentId = getStringParameter(request, "departmentId");
        String userId = getStringParameter(request, "userId");
        String tenantId = getStringParameter(request, "tenantId");
        String plan = getStringParameter(request, "plan");
        if (Strings.isNullOrEmpty(userId)) {
            userId = SecurityUserHolder.getCurrentAccountId();
            departmentId = SecurityUserHolder.getCurrentAccountDepartment().getId();
            tenantId = SecurityUserHolder.getCurrentAccountTenantId();
        }

        ConditionsWrapper wrapper = ConditionsWrapper.builder();
        wrapper.table("JE_RBAC_MENUHISTORY");
        wrapper.eq("MENUHISTORY_USER_ID", userId);
        if (!Strings.isNullOrEmpty(plan)) {
            //过滤，必须有一级菜单才加载一级菜单下的历史菜单
            DynaBean planBean = metaResourceService.selectOneByNativeQuery("JE_CORE_SETPLAN", NativeQuery.build().eq("JE_SYS_PATHSCHEME", plan));
            String topMenuId = planBean.getStr("SETPLAN_ASSOCIATE_TOP_MENU_ID");
            String isAssociateAllTopMenu = planBean.getStr("SETPLAN_ASSOCIATE_ALL_TOP_MENU", "1");
            if (!Strings.isNullOrEmpty(topMenuId) && !isAssociateAllTopMenu.equals("1")) {
                List<DynaBean> headmenu_relation = metaService.select("JE_RBAC_HEADMENU_RELATION",
                        ConditionsWrapper.builder().in("JE_RBAC_HEADMENU_ID", Splitter.on(",").splitToList(topMenuId)), "RELATION_MENUMODULE_ID");
                List<DynaBean> menuIds = metaService.select("JE_CORE_MENU", ConditionsWrapper.builder()
                        .apply(String.format("JE_CORE_MENU_ID IN ( SELECT JE_CORE_MENU_ID FROM JE_RBAC_MENUHISTORY WHERE MENUHISTORY_USER_ID = '%s' )", userId))
                        .and(inWrapper -> {
                            for (int i = 0; i < headmenu_relation.size(); i++) {
                                if (i > 0) {
                                    inWrapper.or();
                                }
                                inWrapper.like("SY_PATH", headmenu_relation.get(i).getStr("RELATION_MENUMODULE_ID"));
                            }
                        }), "JE_CORE_MENU_ID"
                );
                if (menuIds.size() > 0) {
                    String[] menuIdArr = new String[menuIds.size()];
                    for (int i = 0; i < menuIdArr.length; i++) {
                        menuIdArr[i] = menuIds.get(i).getStr("JE_CORE_MENU_ID");
                    }
                    wrapper.in("JE_CORE_MENU_ID", menuIdArr);
                }
            }
        }
        wrapper.orderByDesc("MENUHISTORY_CZSJ");
        List<Map<String, Object>> resultList = metaService.selectSql(wrapper);
        List<String> menuPermissionList = permissionRpcService.findAccountMenuPermissions(departmentId, userId, tenantId);
        List<Map<String, Object>> permdList = new ArrayList<>();
        for (Map<String, Object> eachMap : resultList) {
            if (!menuPermissionList.contains(pcMenuDataService.formatMenuDataShowTemplate(eachMap.get("JE_CORE_MENU_ID").toString()))) {
                continue;
            }
            permdList.add(eachMap);
        }
        return BaseRespResult.successResult(permdList);
    }

    @RequestMapping(value = "/recordMenuHistory", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult recordMenuHistory(HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String userName = getStringParameter(request, "userId");
        String menuId = getStringParameter(request, "menuId");

        if (Strings.isNullOrEmpty(menuId)) {
            return BaseRespResult.errorResult("请选择要记录的菜单！");
        }

        if (Strings.isNullOrEmpty(userId)) {
            userId = SecurityUserHolder.getCurrentAccountId();
            userName = SecurityUserHolder.getCurrentAccountName();
        }

        DynaBean recordBean = metaService.selectOne("JE_RBAC_MENUHISTORY", ConditionsWrapper.builder()
                .eq("JE_CORE_MENU_ID", menuId)
                .eq("MENUHISTORY_USER_ID", userId));
        if (recordBean == null) {
            DynaBean menuBean = metaService.selectOneByPk("JE_CORE_MENU", menuId);
            recordBean = new DynaBean("JE_RBAC_MENUHISTORY", false);
            recordBean.set("JE_CORE_MENU_ID", menuId);
            recordBean.set("MENUHISTORY_USER_ID", userId);
            recordBean.set("MENUHISTORY_USER_NAME", userName);
            recordBean.set("MENUHISTORY_CZSJ", DateUtils.formatDate(new Date(), DateUtils.DAFAULT_DATETIME_FORMAT));
            recordBean.set("SY_PRODUCT_ID", menuBean.getStr("SY_PRODUCT_ID"));
            recordBean.set("SY_PRODUCT_CODE", menuBean.getStr("SY_PRODUCT_CODE"));
            recordBean.set("SY_PRODUCT_NAME", menuBean.getStr("SY_PRODUCT_NAME"));
            commonService.buildModelCreateInfo(recordBean);
            metaService.insert(recordBean);
        } else {
            recordBean.set("MENUHISTORY_CZSJ", DateUtils.formatDate(new Date(), DateUtils.DAFAULT_DATETIME_FORMAT));
            commonService.buildModelModifyInfo(recordBean);
            metaService.update(recordBean);
        }
        //只保存最近使用的20条
        List<DynaBean> dynaBeanList = metaService.select("JE_RBAC_MENUHISTORY", ConditionsWrapper.builder()
                .eq("MENUHISTORY_USER_ID", userId)
                .orderByDesc("SY_CREATETIME")
                .apply("LIMIT 20"));
        List<String> ids = new ArrayList<>();
        for (DynaBean dynaBean : dynaBeanList) {
            if (!Strings.isNullOrEmpty(dynaBean.getStr("JE_RBAC_MENUHISTORY_ID"))) {
                ids.add(dynaBean.getStr("JE_RBAC_MENUHISTORY_ID"));
            }
        }
        if (ids.size() > 0) {
            metaService.executeSql("delete from JE_RBAC_MENUHISTORY where MENUHISTORY_USER_ID = {0} and JE_RBAC_MENUHISTORY_ID not in ({1})", userId, ids);
        }
        return BaseRespResult.successResult(recordBean);
    }

    @RequestMapping(value = "/removeMenuHistory", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult removeMenuHistory(HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String menuIds = getStringParameter(request, "menuIds");
        if (Strings.isNullOrEmpty(menuIds)) {
            return BaseRespResult.errorResult("请选择要删除的菜单历史！");
        }
        if (Strings.isNullOrEmpty(userId)) {
            userId = SecurityUserHolder.getCurrentAccountId();
        }
        metaService.delete("JE_RBAC_MENUHISTORY", ConditionsWrapper.builder()
                .eq("MENUHISTORY_USER_ID", userId).in("JE_CORE_MENU_ID", Splitter.on(",").splitToList(menuIds)));
        return BaseRespResult.successResult("删除成功！");
    }

}
