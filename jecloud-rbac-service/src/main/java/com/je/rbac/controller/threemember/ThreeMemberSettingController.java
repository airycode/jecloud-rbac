/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.threemember;

import com.je.common.base.DynaBean;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.threemember.ThreeMemberSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/je/rbac/threemember/setting")
public class ThreeMemberSettingController {

    @Autowired
    private MetaService metaService;
    @Autowired
    private ThreeMemberSettingService threeMemberSettingService;

    @RequestMapping(value = "/installThreeMember", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult installThreeMember() {
        List<DynaBean> settingBeanList = metaService.select("JE_RBAC_THREEMSETTING", ConditionsWrapper.builder().orderByDesc("SY_CREATETIME"));
        if (settingBeanList == null || settingBeanList.isEmpty()) {
            return BaseRespResult.errorResult("请先设置三员管理再执行此操作！");
        }
        if (!"1".equals(settingBeanList.get(0).getStr("THREEMSETTING_OPEN_CODE"))) {
            return BaseRespResult.errorResult("请先开启三员管理！");
        }
        if ("1".equals(settingBeanList.get(0).getStr("THREEMSETTING_INIT_CODE"))) {
            return BaseRespResult.errorResult("三员管理已初始化完成！");
        }
        threeMemberSettingService.installThreeMemberManage(settingBeanList.get(0));
        return BaseRespResult.successResult("已成功初始化三员上下文，并创建三元管理员，请进入资源范围管理功能设置各管理员可操作的功能！");
    }

    @RequestMapping(value = "/uninstallThreeMember", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult uninstallThreeMember() {
        List<DynaBean> settingBeanList = metaService.select("JE_RBAC_THREEMSETTING", ConditionsWrapper.builder().orderByDesc("SY_CREATETIME"));
        if (settingBeanList == null || settingBeanList.isEmpty()) {
            return BaseRespResult.errorResult("请先设置三员管理再执行此操作！");
        }
        if (!"1".equals(settingBeanList.get(0).getStr("THREEMSETTING_INIT_CODE"))) {
            return BaseRespResult.errorResult("三员管理未初始化完成，请勿重复卸载！");
        }
        threeMemberSettingService.unInstallThreeMemberManage(settingBeanList.get(0));
        return BaseRespResult.successResult("已成功卸载三员上下文，并删除三员管理员！");
    }

}
