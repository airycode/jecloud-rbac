/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.log;

import com.google.common.base.Strings;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.rbac.exception.LoginException;
import com.je.rbac.service.log.RbacBuryLogService;
import com.je.rbac.service.log.RbacLoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/rbac/cloud/log")
public class RbacLogController extends AbstractPlatformController {

    @Autowired
    private RbacBuryLogService buryLogService;
    @Autowired
    private RbacLoginLogService rbacLoginLogService;

    @ResponseBody
    @RequestMapping(value = {"/loginLogList"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult<Object> loginLogList(HttpServletRequest request) {
        String start = getStringParameter(request, "start");
        String limit = getStringParameter(request, "limit");
        if (Strings.isNullOrEmpty(start)) {
            start = "0";
        }
        if (Strings.isNullOrEmpty(limit)) {
            limit = "50";
        }
        Map<String, Object> result = rbacLoginLogService.findLoginLogList(Integer.valueOf(start), Integer.valueOf(limit));
        return BaseRespResult.successResult(result);
    }

    @ResponseBody
    @RequestMapping(value = {"/bury"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult<Object> bury(HttpServletRequest request) {
        //操作对象编码
        String objCode = getStringParameter(request, "objCode");
        //操作对象名称
        String objName = getStringParameter(request, "objName");
        //操作类型编码
        String typeCode = getStringParameter(request, "typeCode");
        //操作类型名称
        String typeName = getStringParameter(request, "typeName");

        if (Strings.isNullOrEmpty(objCode) || Strings.isNullOrEmpty(objName)) {
            return BaseRespResult.errorResult("请写入操作对象编码和名称");
        }

        try {
            buryLogService.bury(typeCode, typeName, objCode, objName);
        } catch (LoginException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }

        return BaseRespResult.successResult("写入成功");
    }

    @ResponseBody
    @RequestMapping(value = {"/buryList"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult<Object> buryList(HttpServletRequest request) {
        String start = getStringParameter(request, "start");
        String limit = getStringParameter(request, "limit");
        if (Strings.isNullOrEmpty(start)) {
            start = "0";
        }
        if (Strings.isNullOrEmpty(limit)) {
            limit = "50";
        }

        Map<String, Object> result = buryLogService.findBuryList(Integer.valueOf(start), Integer.valueOf(limit),false);
        return BaseRespResult.successResult(result);
    }

    @ResponseBody
    @RequestMapping(value = {"/twoHoursActive"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult<Object> twoHoursActive(HttpServletRequest request) {
        String start = getStringParameter(request, "start");
        String limit = getStringParameter(request, "limit");
        if (Strings.isNullOrEmpty(start)) {
            start = "0";
        }
        if (Strings.isNullOrEmpty(limit)) {
            limit = "50";
        }

        Map<String, Object> result = buryLogService.findBuryList(Integer.valueOf(start), Integer.valueOf(limit),true);
        return BaseRespResult.successResult(result);
    }

}
