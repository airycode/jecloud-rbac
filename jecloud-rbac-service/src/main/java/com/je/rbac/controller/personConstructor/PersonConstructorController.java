/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.personConstructor;

import com.alibaba.fastjson2.JSONArray;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.StringUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.service.personconstructor.PersonConstructorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @ClassName 人员构造器
 * @Author wangchao
 * @Date 2022/6/14 0014 11:40
 * @Version V1.0
 */
@RestController
@RequestMapping(value = "/je/rbac/personConstructor")
public class PersonConstructorController extends AbstractPlatformController {

    @Autowired
    private PersonConstructorService personConstructorService;


    @RequestMapping(value = "/loadPersonInfo", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadPersonInfo(BaseMethodArgument param, HttpServletRequest request) {
        String type = getStringParameter(request, "type");
        String showDeveloper = getStringParameter(request, "showDeveloper");
        if (StringUtil.isEmpty(type)) {
            return BaseRespResult.errorResult("请传入相应查询类型！");
        }
        JSONTreeNode rootNode = personConstructorService.loadPersonInfo(type, showDeveloper, param);
        return BaseRespResult.successResult(rootNode);
    }

    @RequestMapping(value = "/addCommonUser", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult addCommonUser(HttpServletRequest request) {
        String accountDeptIds = getStringParameter(request, "accountDeptIds");

        if (StringUtil.isEmpty(accountDeptIds)) {
            return BaseRespResult.errorResult("请传入相应参数！");
        }
        personConstructorService.addCommonUser(accountDeptIds);
        return BaseRespResult.successResult("成功！");
    }

    @RequestMapping(value = "/getPersonInfoEcho", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getPersonInfoEcho(HttpServletRequest request) {
        String type = getStringParameter(request, "type");
        String jsonArray = getStringParameter(request, "jsonArray");

        if (StringUtil.isEmpty(type) || StringUtil.isEmpty(jsonArray)) {
            return BaseRespResult.errorResult("请传入相应参数！");
        }
        JSONArray parseArray = null;
        try {
            parseArray = JSONArray.parseArray(jsonArray);
        } catch (Exception e) {
            return BaseRespResult.errorResult("数据格式错误！");
        }

        return personConstructorService.getPersonInfoEcho(type, parseArray);
    }
}
