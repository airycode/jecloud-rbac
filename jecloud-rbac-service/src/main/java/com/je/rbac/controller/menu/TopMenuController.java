/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.menu;

import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.ArrayUtils;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.exception.MenuException;
import com.je.rbac.rpc.PermissionRpcService;
import com.je.rbac.service.menu.TopMenuService;
import com.je.rbac.service.permission.PermissionLoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/je/rbac/topMenu")
public class TopMenuController extends AbstractPlatformController {

    @Autowired
    private TopMenuService topMenuService;

    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean resultBean = topMenuService.doSave(param,request);
        return BaseRespResult.successResult(resultBean);
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean resultBean = topMenuService.doUpdate(param,request);
        return BaseRespResult.successResult(resultBean);
    }

    @Override
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        List<String> topMenuIds = Arrays.asList(getStringParameter(request,"ids").split(ArrayUtils.SPLIT));
        try{
            topMenuService.checkChildMenu(topMenuIds);
        }catch (MenuException e){
            return BaseRespResult.errorResult(e.getMessage());
        }
        //删除顶部菜单关联菜单 关联表数据；更新菜单表
        topMenuService.updateMenuRelation(topMenuIds);

        List<DynaBean> list= metaService.select("JE_RBAC_HEADMENU",
                ConditionsWrapper.builder().in("JE_RBAC_HEADMENU_ID",topMenuIds));
        int count =manager.doRemove(param, request);
        //记录日志
        topMenuService.saveDeleteLog(list);
        return BaseRespResult.successResult(String.format("%s 条记录被删除", count));
    }


}
