/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.account;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import com.je.auth.AuthLoginManager;
import com.je.auth.AuthSessionTemplate;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.annotation.ControllerAuditLog;
import com.je.rbac.exception.AccountException;
import com.je.rbac.service.account.RbacAccountService;
import com.je.rbac.service.account.RbacLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.je.rbac.controller.account.RbacLoginController.USER_NEED_MODIFY_PASSWORD_TEMPALTE;

/**
 * 账号管理
 */
@RestController
@RequestMapping(value = "/je/rbac/cloud/account")
public class RbacAccountController extends AbstractPlatformController {

    /**
     * 默认账号永久禁用
     */
    private static final int DEFAULT_DISABLE_TIME = -1;

    @Autowired
    private RbacLoginService rbacLoginService;
    @Autowired
    private AuthLoginManager authLoginManager;
    @Autowired
    private AuthSessionTemplate authSessionTemplate;
    @Autowired
    private RbacAccountService rbacAccountService;
    @Autowired
    private SystemSettingRpcService systemSettingRpcService;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    @RequestMapping(value = "/load", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "loadAccount",operateTypeName = "查看账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        return super.load(param, request);
    }

    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "saveAccount",operateTypeName = "保存账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        return super.doSave(param, request);
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "updateAccount",operateTypeName = "更新账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        return super.doUpdate(param, request);
    }

    @Override
    @RequestMapping(value = "/doUpdateList", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "updateAccountList",operateTypeName = "更新账号集合",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult doUpdateList(BaseMethodArgument param, HttpServletRequest request) {
        return super.doUpdateList(param, request);
    }

    @Override
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "removeAccount",operateTypeName = "移除账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        return super.doRemove(param, request);
    }

    /**
     * 获取在线用户
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/online"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult online(HttpServletRequest request) {
        String keyword = getStringParameter(request, "keyword");
        Integer start = getStringParameter(request, "start") == null ? null : Integer.valueOf(getStringParameter(request, "start"));
        Integer limit = getStringParameter(request, "limit") == null ? null : Integer.valueOf(getStringParameter(request, "limit"));
        if (start == null || limit == null) {
            return BaseRespResult.errorResult("参数异常！");
        }
        return BaseRespResult.successResult(rbacAccountService.requireOnlineUsers(keyword, start, limit));
    }

    @RequestMapping(value = {"/checkAccount"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult checkAccount(HttpServletRequest request) throws JsonProcessingException {
        String accountId = authLoginManager.getLoginIdAsString();
        if (Strings.isNullOrEmpty(accountId)) {
            return BaseRespResult.errorResult("请先登录！");
        }
        DynaBean accountBean = metaService.selectOne("JE_RBAC_ACCOUNT", ConditionsWrapper.builder().eq("JE_RBAC_ACCOUNT_ID", accountId));
        if (accountBean == null) {
            return BaseRespResult.errorResult("不存在的账号!");
        }
        Map<String, Object> result = new HashMap<>();
        String needInitedPassword = systemSettingRpcService.findSettingValue("INITPASSWORD");
        String defaultPassword = systemSettingRpcService.findSettingValue("JE_SYS_PASSWORD");
        //是否强制修改默认密码
        result.put("inited", false);
        if ("1".equals(needInitedPassword)) {
            //判断和默认密码是否一致
            if (accountBean.getStr("ACCOUNT_PASSWORD").equals(SecureUtil.md5(defaultPassword))) {
                result.put("inited", true);
            }
        }
        //是否符合密码安全策略
        result.put("passwdStrategy", false);
        if (redisTemplate.hasKey(String.format(USER_NEED_MODIFY_PASSWORD_TEMPALTE, accountId))) {
            result.put("passwdStrategy", true);
        }
        return BaseRespResult.successResult(result);
    }

    @RequestMapping(value = {"/currentUser"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult currentUser(HttpServletRequest request) throws JsonProcessingException {
        if (!rbacLoginService.checkLogin()) {
            return BaseRespResult.errorResult("未登录，请执行登录！");
        }
        Object account = authSessionTemplate.get(authLoginManager.getSession(), "account");
        if (account == null) {
            return BaseRespResult.errorResult("未登录，请执行登录！");
        }
        JSONObject accountObj = (JSONObject) JSON.toJSON(account);
        DynaBean accountBean = metaService.selectOne("JE_RBAC_ACCOUNT", ConditionsWrapper.builder().eq("JE_RBAC_ACCOUNT_ID", accountObj.get("id")));
        accountObj.put("avatar", accountBean.getStr("ACCOUNT_AVATAR"));

        JSONObject realUserObj = (JSONObject) JSON.toJSON(accountObj.get("realUser"));
        realUserObj.put("avatar", accountBean.getStr("ACCOUNT_AVATAR"));
        accountObj.put("realUser", realUserObj);
        return BaseRespResult.successResult(accountObj);
    }

    /**
     * 忘记密码-取回密码
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/modifyPassword"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "modifyPassword",operateTypeName = "修改账号密码",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult modifyPassword(HttpServletRequest request) {
        String oldPassword = getStringParameter(request, "oldPassword");
        String newPassword = getStringParameter(request, "newPassword");
        String accountId = authLoginManager.getLoginIdAsString();
        if (Strings.isNullOrEmpty(accountId)) {
            return BaseRespResult.errorResult("请先登录！");
        }
        if (Strings.isNullOrEmpty(oldPassword) || Strings.isNullOrEmpty(newPassword)) {
            return BaseRespResult.errorResult("密码格式不正确，请重新输入密码！");
        }
        //查库校验原始密码是否正确
        DynaBean accountBean = metaService.selectOne("JE_RBAC_ACCOUNT", ConditionsWrapper.builder().eq("JE_RBAC_ACCOUNT_ID", accountId));
        if (accountBean == null) {
            return BaseRespResult.errorResult("当前账号不存在，请确认！");
        }
        if (!SecureUtil.md5(oldPassword).equals(accountBean.getStr("ACCOUNT_PASSWORD"))) {
            return BaseRespResult.errorResult("原始密码不正确，请重新输入密码！");
        }

        //移除校验策略key
        redisTemplate.unlink(String.format(USER_NEED_MODIFY_PASSWORD_TEMPALTE, accountId));

        String validResult = rbacAccountService.validPassword(newPassword);
        if (!Strings.isNullOrEmpty(validResult)) {
            return BaseRespResult.errorResult(validResult);
        }

        String encrpedPassword = SecureUtil.md5(newPassword);
        metaService.executeSql("UPDATE JE_RBAC_ACCOUNT SET ACCOUNT_PASSWORD={0},ACCOUNT_INITED='0' WHERE JE_RBAC_ACCOUNT_ID={1}", encrpedPassword, accountId);
        return BaseRespResult.successResult("更新成功！");
    }

    /**
     * 校验验证码
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/checkCaptcha"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult checkCaptcha(HttpServletRequest request) {
        String phone = getStringParameter(request, "phone");
        String device = getStringParameter(request, "device");
        String captcha = getStringParameter(request, "captcha");

        if (Strings.isNullOrEmpty(phone) || Strings.isNullOrEmpty(device) || Strings.isNullOrEmpty(captcha)) {
            return BaseRespResult.errorResult("非法的验证参数！");
        }

        DynaBean accountBean = metaService.selectOne("JE_RBAC_ACCOUNT", ConditionsWrapper.builder().eq("ACCOUNT_PHONE", phone));
        if (accountBean == null) {
            return BaseRespResult.errorResult("系统不允许找回密码，请联系管理员！");
        }

        if (!rbacLoginService.checkRandom(accountBean, captcha, device)) {
            return BaseRespResult.errorResult("失效的验证码，请重新发送验证码！");
        }
        return BaseRespResult.successResult("校验成功！");

    }

    /**
     * 忘记密码-找回密码
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/resetPassword"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "findbackPassword",operateTypeName = "找回账号密码",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult resetPassword(HttpServletRequest request) {
        String newPassword = getStringParameter(request, "newPassword");
        String confirmPassword = getStringParameter(request, "confirmPassword");
        String phone = getStringParameter(request, "phone");
        String device = getStringParameter(request, "device");
        String captcha = getStringParameter(request, "captcha");

        String enableFetchbackPassword = systemSettingRpcService.findSettingValue("JE_CORE_PHONEREPASSWORD");
        if (!"1".equals(enableFetchbackPassword)) {
            return BaseRespResult.errorResult("系统不允许找回密码，请联系管理员！");
        }

        DynaBean accountBean = metaService.selectOne("JE_RBAC_ACCOUNT", ConditionsWrapper.builder().eq("ACCOUNT_PHONE", phone));
        if (accountBean == null) {
            return BaseRespResult.errorResult("系统不允许找回密码，请联系管理员！");
        }

        if (!rbacLoginService.checkRandom(accountBean, captcha, device)) {
            return BaseRespResult.errorResult("失效的验证码，请重新发送验证码！");
        }

        try {
            if (Strings.isNullOrEmpty(newPassword) || Strings.isNullOrEmpty(confirmPassword)) {
                return BaseRespResult.errorResult("密码格式不正确，请重新输入密码！");
            }
            if (!newPassword.equals(confirmPassword)) {
                return BaseRespResult.errorResult("密码不一致，请重新输入密码！");
            }
            String validResult = rbacAccountService.validPassword(newPassword);
            if (!Strings.isNullOrEmpty(validResult)) {
                return BaseRespResult.errorResult(validResult);
            }
            String encrpedPassword = SecureUtil.md5(newPassword);
            metaService.executeSql("UPDATE JE_RBAC_ACCOUNT SET ACCOUNT_PASSWORD={0} WHERE JE_RBAC_ACCOUNT_ID={1}",
                    encrpedPassword, accountBean.getStr("JE_RBAC_ACCOUNT_ID"));
        } catch (Throwable e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult("更新成功！");
    }

    /**
     * 重置密码
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/recoverPassword"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "resetPassword",operateTypeName = "重置账号密码",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult recoverPassword(HttpServletRequest request) throws AccountException {
        String accountIds = getStringParameter(request, "accountIds");
        if (Strings.isNullOrEmpty(accountIds)) {
            return BaseRespResult.errorResult("请选择要重置的账号！");
        }
        String defaultPassword = systemSettingRpcService.findSettingValue("JE_SYS_PASSWORD");
        if (Strings.isNullOrEmpty(defaultPassword)) {
            return BaseRespResult.errorResult("系统设置默认密码为空！");
        }
        try {
            rbacAccountService.recoverPassword(accountIds);
        } catch (Exception e) {
            return BaseRespResult.errorResult("重置密码失败！" + e.getMessage());
        }
        return BaseRespResult.successResult("重置成功，密码为：" + defaultPassword);
    }

    /***
     * 启用账号
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/enable"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "enableAccount",operateTypeName = "启用账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult enable(HttpServletRequest request) {
        String accountIds = getStringParameter(request, "accountIds");
        if (Strings.isNullOrEmpty(accountIds)) {
            return BaseRespResult.errorResult("请选择要启用的账号！");
        }
        try {
            rbacAccountService.enableAccount(accountIds);
        } catch (AccountException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult("操作成功");
    }

    /***
     * 禁用账号
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/disable"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "disableAccount",operateTypeName = "禁用账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult disable(HttpServletRequest request) {
        String accountIds = getStringParameter(request, "accountIds");
        if (Strings.isNullOrEmpty(accountIds)) {
            return BaseRespResult.errorResult("请选择要禁用的账号！");
        }
        try {
            rbacAccountService.disableAccount(accountIds);
        } catch (AccountException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult("操作成功");
    }

    /***
     * 账号锁定
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/lock"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "lockAccount",operateTypeName = "锁定账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult lock(HttpServletRequest request) {
        String accountIds = getStringParameter(request, "accountIds");
        String disableTime = getStringParameter(request, "disableTime");
        if (Strings.isNullOrEmpty(accountIds)) {
            return BaseRespResult.errorResult("请选择要锁定的账号！");
        }
        if (Strings.isNullOrEmpty(disableTime)) {
            disableTime = String.valueOf(DEFAULT_DISABLE_TIME);
        }
        rbacAccountService.lockAccount(accountIds, Long.valueOf(disableTime));
        return BaseRespResult.successResult("锁定成功！");
    }

    /***
     * 账号解除锁定
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/unlock"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "unlockAccount",operateTypeName = "解除账号锁定",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult unlock(HttpServletRequest request) {
        String accountIds = getStringParameter(request, "accountIds");
        if (Strings.isNullOrEmpty(accountIds)) {
            return BaseRespResult.errorResult("请选择要解除锁定的账号！");
        }
        rbacAccountService.unlockAccount(accountIds);
        return BaseRespResult.successResult("操作成功");
    }

    /***
     * 账号删除
     * @param param
     * @param request
     * @return
     */
    @RequestMapping(value = "/remove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "removeAccount",operateTypeName = "移除账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult remove(BaseMethodArgument param, HttpServletRequest request) {
        String ids = getStringParameter(request, "ids");
        if (Strings.isNullOrEmpty(ids)) {
            return BaseRespResult.errorResult("请重新选择要删除的账号！");
        }
        int result = rbacAccountService.doRemove(ids);
        if (result <= 0) {
            return BaseRespResult.errorResult("删除失败！");
        }
        return BaseRespResult.successResult("删除成功！");
    }

    /***
     * 账号数据同步
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/dataSync"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "accountSync",operateTypeName = "账号数据同步",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult dataSync(HttpServletRequest request) {
        try {
            rbacAccountService.dataSync();
        } catch (AccountException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult("操作成功");
    }

    /***
     * 列表数据保存
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/dataSave"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "saveAccountList",operateTypeName = "保存账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult dataSave(HttpServletRequest request) {
        String beanList = getStringParameter(request, "dynaBeans");
        if (Strings.isNullOrEmpty(beanList)) {
            return BaseRespResult.errorResult("请选择要保存的账号！");
        }
        try {
            rbacAccountService.dataSave(request);
        } catch (AccountException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult("保存成功！");
    }


    /***
     * 修改账号头像
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/updateUserAvatar"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "modifyAccountAvatar",operateTypeName = "修改账号头像",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult updateUserAvatar(HttpServletRequest request) throws AccountException {
        String accountId = getStringParameter(request, "accountId");
        String avatar = getStringParameter(request, "avatar");
        if (Strings.isNullOrEmpty(accountId)) {
            return BaseRespResult.errorResult("请选择要修改的人员！");
        }
        if (Strings.isNullOrEmpty(avatar)) {
            return BaseRespResult.errorResult("请设置要修改的人员头像！");
        }
        rbacAccountService.updateUserAvatar(accountId, avatar);
        return BaseRespResult.successResult("修改成功！");
    }


    @ResponseBody
    @RequestMapping(value = {"/kickout"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "账号模块",operateTypeCode = "kickAccount",operateTypeName = "踢出账号",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult kickout(HttpServletRequest request) {
        String accountId = getStringParameter(request, "accountId");
        String device = getStringParameter(request, "device");
        if (Strings.isNullOrEmpty(accountId)) {
            return BaseRespResult.errorResult("请选择要Kickout的账号！");
        }
        boolean success = rbacLoginService.kickout(accountId, device);
        if (!success) {
            return BaseRespResult.errorResult("踢出失败！");
        }
        return BaseRespResult.successResult("踢出成功！");
    }

    /***
     * 登录人次统计（按日，按照每日登录算做一次）
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/loginStatistic"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult loginStatistic(HttpServletRequest request) {
        String year = getStringParameter(request, "year");
        String month = getStringParameter(request, "month");
        try {
            List<Map<String, Object>> resultListMap = rbacAccountService.queryLoginStatistic(year, month);
            return BaseRespResult.successResult(resultListMap);
        } catch (AccountException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
    }

    /***
     * 组织信息统计人数（按月1-12月）
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/staffStatistic"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult staffStatistic(HttpServletRequest request) {
        String year = getStringParameter(request, "year");
        try {
            Map<String, Object> resultMap = rbacAccountService.staffStatistic(year);
            return BaseRespResult.successResult(resultMap);
        } catch (AccountException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
    }

    /**
     * 账户管理列表保存
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/doSaveList"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSaveList(HttpServletRequest request) {
        Object obj = getObjectParamter(request, "jsonArray");
        if(obj == null){
            return BaseRespResult.successResult("数据没有变动！");
        }

        try {
            rbacAccountService.doSaveList(obj);
            return BaseRespResult.successResult("保存成功！");
        } catch (AccountException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }

    }
}
