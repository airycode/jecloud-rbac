/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.listener;

import com.je.common.base.spring.SpringContextHolder;
import com.je.rbac.service.permission.PermissionLoadService;
import org.apache.servicecomb.core.BootListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 用户权限加载缓存
 */
public class PermissionCacheInitListener implements BootListener {

    private static final Logger logger = LoggerFactory.getLogger(PermissionCacheInitListener.class);
    private static boolean flag = false;

    @Override
    public int getOrder() {
        return 100001;
    }

    @Override
    public void onAfterRegistry(BootEvent event) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!flag) {
                    try {
                        PermissionLoadService permissionLoadService = SpringContextHolder.getBean(PermissionLoadService.class);
                        Future<Boolean> rolePermFuture = permissionLoadService.reloadAllRolePermCache(false);
                        Future<Boolean> departmentPermFuture = permissionLoadService.reloadAllDepartmentPermCache();
                        Future<Boolean> orgPermFuture = permissionLoadService.reloadAllOrgPermCache();
                        Future<Boolean> developRolePermFuture = permissionLoadService.reloadAllDevelopRolePermCache();
                        Future<Boolean> permGroupPermFuture = permissionLoadService.reloadAllPermGroupPermCache(false);
                        permissionLoadService.reloadAllUserPermCache();
                        if (rolePermFuture.get() && departmentPermFuture.get() && orgPermFuture.get()
                                && developRolePermFuture.get() && permGroupPermFuture.get()) {
                            flag = true;
                            logger.info("Init the permission cache success!");
                        }
                    } catch (Throwable e) {
                        logger.error("Init the permission cache failure,will retry in 2 seconds! the expression is {}", e.getMessage());
                        try {
                            Thread.sleep(1000 * 2);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                logger.info("The permission cache init process success, will be finished!");
            }
        }).start();

    }

}
