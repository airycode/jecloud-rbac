/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.ProductResult;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BaseDevelopRpcService;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.cache.AccountProductCache;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.ArrayList;
import java.util.List;
import static com.je.rbac.service.grant.impl.DevelopPlatformServiceImpl.DEV_KEY;

@RpcSchema(schemaId = "baseDevelopRpcService")
public class BaseDevelopRpcServiceImpl implements BaseDevelopRpcService {

    @Autowired
    private SystemSettingRpcService systemSettingRpcService;
    @Autowired
    private MetaService metaService;
    @Autowired
    private AccountProductCache accountProductCache;

    @Override
    public ProductResult platform(String accountId) {
        ProductResult result = new ProductResult();
        String devRoleId = systemSettingRpcService.findSettingValue(DEV_KEY);
        if (Strings.isNullOrEmpty(devRoleId)) {
            result.setDevelop(false);
        }

        List<DynaBean> beanList = metaService.select("JE_RBAC_ACCOUNTROLE", ConditionsWrapper.builder()
                .eq("ACCOUNTROLE_ACCOUNT_ID", accountId));
        if (beanList == null || beanList.isEmpty()) {
            result.setDevelop(false);
            return result;
        }

        boolean flag = false;
        List<String> roleIdList = new ArrayList<>();
        for (DynaBean eachBean : beanList) {
            if (devRoleId!=null && devRoleId.equals(eachBean.getStr("ACCOUNTROLE_ROLE_ID"))) {
                flag = true;
            } else {
                roleIdList.add(eachBean.getStr("ACCOUNTROLE_ROLE_ID"));
            }
        }

        List<String> productList = accountProductCache.getCacheValue(accountId);
        if (productList != null && !productList.isEmpty()) {
            result.getProducts().addAll(productList);
        } else {
            productList = new ArrayList<>();
            List<DynaBean> roleBeanList = metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder()
                    .in("JE_RBAC_ROLE_ID", roleIdList));
            for (DynaBean eachRoleBean : roleBeanList) {
                if (!Strings.isNullOrEmpty(eachRoleBean.getStr("SY_PRODUCT_ID"))) {
                    productList.add(eachRoleBean.getStr("SY_PRODUCT_ID"));
                }
            }
            accountProductCache.putCache(accountId, productList);
            result.setProducts(productList);
        }

        result.setDevelop(flag);
        return result;
    }

}
