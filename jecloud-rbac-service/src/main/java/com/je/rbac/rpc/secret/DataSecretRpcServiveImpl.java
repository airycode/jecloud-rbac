/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.secret;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.DataSecretRpcServive;
import com.je.common.base.util.SystemSecretUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.secret.SecretSettingService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@RpcSchema(schemaId = "dataSecretRpcServive")
public class DataSecretRpcServiveImpl implements DataSecretRpcServive {

    @Autowired
    private SecretSettingService secretSettingService;
    @Autowired
    private MetaService metaService;

    @Override
    public List<String> requireContextDataSecretLevel() {
        return secretSettingService.requireContextDataSecretInfo(SystemSecretUtil.contextSecretCode());
    }

    @Override
    public Boolean validUserAndFlowNodeSecret(String accountDeptId, String flowNodeSecretCode) {
        List<DynaBean> accountDeptBeanList = metaService.select("JE_RBAC_VACCOUNTDEPT", ConditionsWrapper.builder()
                .eq("JE_RBAC_ACCOUNTDEPT_ID",accountDeptId));
        if(accountDeptBeanList == null || accountDeptBeanList.isEmpty()){
            return true;
        }

        int accountSecretCode = accountDeptBeanList.get(0).getInt("SY_SECRET_CODE");
        if(Integer.valueOf(flowNodeSecretCode) > accountSecretCode){
            return false;
        }

        return true;
    }

}
