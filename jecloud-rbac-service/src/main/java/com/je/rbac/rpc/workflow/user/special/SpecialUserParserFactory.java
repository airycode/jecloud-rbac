/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.user.special;

import java.util.HashMap;
import java.util.Map;

/**
 * 特殊人员处理
 */
public class SpecialUserParserFactory {

    public static final Map<String, AbstractSpecialUserParser> map = new HashMap();

    static {
        map.put(SpecialUserParserEnum.DIRECT_LEADER.toString(), new DirectLeadershipParser());
        map.put(SpecialUserParserEnum.TASK_ASSGINE_HEAD.toString(), new DirectLeadershipParser());
        map.put(SpecialUserParserEnum.PREV_ASSIGN_USER_DIRECT_LEADER.toString(), new DirectLeadershipParser());
        map.put(SpecialUserParserEnum.DEPT_LEADER.toString(), new DeptLeadershipParser());
        map.put(SpecialUserParserEnum.DEPT_MONITOR_LEADER.toString(), new SupervisionDeptLeadershipParser());
        map.put(SpecialUserParserEnum.LOGINED_USER.toString(), new UserParser());
        map.put(SpecialUserParserEnum.STARTER_USER.toString(), new UserParser());
        map.put(SpecialUserParserEnum.TASK_ASSGINE.toString(), new UserParser());
        map.put(SpecialUserParserEnum.PREV_ASSIGN_USER.toString(), new UserParser());
        map.put(SpecialUserParserEnum.DEPT_USERS.toString(), new DeptUserParser());
        map.put(SpecialUserParserEnum.DEPT_ALL_USERS.toString(), new DeptAllUserParser());
        map.put(SpecialUserParserEnum.DEPT_MONITOR_USERS.toString(), new DeptMonitorUserParser());
        map.put(SpecialUserParserEnum.COMPANY_LEADERS.toString(), new CompanyLeadersUserParser());
        map.put(SpecialUserParserEnum.COMPANY_MONITOR_LEADERS.toString(), new CompanyMonitorLeadersUserParser());
        map.put(SpecialUserParserEnum.SUBSIDIARY.toString(), new SubsidiaryUserParser());
    }

    public static AbstractSpecialUserParser getParserByCode(String type) {
        return map.get(type);
    }
}
