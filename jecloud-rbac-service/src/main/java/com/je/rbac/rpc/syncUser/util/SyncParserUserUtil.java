/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.syncUser.util;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.rbac.exception.CompanyException;
import com.je.rbac.exception.DepartmentException;
import com.je.rbac.model.syncdata.SyncCompany;
import com.je.rbac.model.syncdata.SyncDepartment;
import com.je.rbac.model.syncdata.SyncUser;
import com.je.rbac.service.company.OrgUserType;
import com.je.rbac.service.organization.OrgType;

public class SyncParserUserUtil {

    public static String checkIsNullOrEmpty(String field , String value){
        if(Strings.isNullOrEmpty(value)){
            throw new RuntimeException("该数据字段："+field+" 参数不允许为空！");
        }
        return value;
    }

    /***
     *构建集团，公司数据 基本信息
     * @param companyMap
     * @return
     * @throws CompanyException
     */
    public static DynaBean buildCompanyBean(SyncCompany companyMap) throws Exception {
        DynaBean companyBean = new DynaBean("JE_RBAC_COMPANY",false);
        companyBean.setStr("JE_RBAC_COMPANY_ID", SyncParserUserUtil.checkIsNullOrEmpty("JE_RBAC_COMPANY_ID",companyMap.getCompanyId()));
        companyBean.setStr("SY_PARENT",SyncParserUserUtil.checkIsNullOrEmpty("SY_PARENT",companyMap.getSyParent()));
        companyBean.setStr("COMPANY_NAME", SyncParserUserUtil.checkIsNullOrEmpty("COMPANY_NAME",companyMap.getCompanyName()));
        companyBean.setStr("COMPANY_CODE", SyncParserUserUtil.checkIsNullOrEmpty("COMPANY_CODE",companyMap.getCompanyCode()));
        String companyLevelCode = companyMap.getCompanyLevelCode();
        if(Strings.isNullOrEmpty(companyLevelCode) || (!"COMPANY".equals(OrgUserType.getCode(companyLevelCode)) && !"GROUP_COMPANY".equals(OrgUserType.getCode(companyLevelCode)))){
            throw new CompanyException("该数据："+companyMap.getCompanyName()+" COMPANY_LEVEL_CODE 参数设置异常！");
        }
        companyBean.setStr("COMPANY_LEVEL_CODE", companyLevelCode);
        companyBean.setStr("COMPANY_LEVEL_NAME", OrgUserType.getCodeName(companyLevelCode));

        if(!Strings.isNullOrEmpty(companyMap.getCompanySimplename())){
            companyBean.setStr("COMPANY_SIMPLENAME", companyMap.getCompanySimplename());
        }
        if(!Strings.isNullOrEmpty(companyMap.getCompanyAddress())){
            companyBean.setStr("COMPANY_ADDRESS", companyMap.getCompanyAddress());
        }
        if(!Strings.isNullOrEmpty(companyMap.getCompanyTelephone())){
            companyBean.setStr("COMPANY_TELEPHONE", companyMap.getCompanyTelephone());
        }
        if(!Strings.isNullOrEmpty(companyMap.getCompanyRemark())){
            companyBean.setStr("COMPANY_REMARK", companyMap.getCompanyRemark());
        }
        if(!Strings.isNullOrEmpty(companyMap.getCompanySimplename())){
            companyBean.setStr("COMPANY_FUNC_DESC", companyMap.getCompanySimplename());
        }
        if(!Strings.isNullOrEmpty(companyMap.getCompanyMajorId())){
            companyBean.setStr("COMPANY_MAJOR_ID", companyMap.getCompanyMajorId());
            companyBean.setStr("COMPANY_MAJOR_NAME", companyMap.getCompanyMajorName());
        }
        if(!Strings.isNullOrEmpty(companyMap.getCompanyManagerId())){
            companyBean.setStr("COMPANY_MANAGER_ID", companyMap.getCompanyManagerId());
            companyBean.setStr("COMPANY_MANAGER_NAME", companyMap.getCompanyManagerName());
        }
        if(!Strings.isNullOrEmpty(companyMap.getCompanyJgcompanyId())){
            companyBean.setStr("COMPANY_JGCOMPANY_ID", companyMap.getCompanyJgcompanyId());
            companyBean.setStr("COMPANY_JGCOMPANY_NAME", companyMap.getCompanyJgcompanyName());
        }
        return companyBean;
    }

    /***
     *构建部门数据 基本信息
     * @param beanMap
     * @return
     * @throws DepartmentException
     */
    public static DynaBean buildDynaBean(SyncDepartment beanMap) throws Exception {
        DynaBean dynaBean = new DynaBean("JE_RBAC_DEPARTMENT",false);
        dynaBean.setStr("JE_RBAC_DEPARTMENT_ID", SyncParserUserUtil.checkIsNullOrEmpty("JE_RBAC_DEPARTMENT_ID",beanMap.getDepartmentId()));
        dynaBean.setStr("SY_PARENT",SyncParserUserUtil.checkIsNullOrEmpty("SY_PARENT",beanMap.getSyParent()));
        dynaBean.setStr("DEPARTMENT_NAME", SyncParserUserUtil.checkIsNullOrEmpty("DEPARTMENT_NAME",beanMap.getDepartmentName()));
        dynaBean.setStr("DEPARTMENT_CODE", SyncParserUserUtil.checkIsNullOrEmpty("DEPARTMENT_CODE",beanMap.getDepartmentCode()));
        String deptLevelCode = beanMap.getDepartmentLevelCode();
        if(Strings.isNullOrEmpty(deptLevelCode) || (!"DEPT".equals(deptLevelCode) && !"WORKSHOP".equals(deptLevelCode) && !"TEAM".equals(deptLevelCode))){
            throw new DepartmentException("该数据："+beanMap.getDepartmentName()+" DEPARTMENT_LEVEL_CODE 参数设置异常！");
        }
        dynaBean.setStr("DEPARTMENT_LEVEL_CODE", deptLevelCode);
        dynaBean.setStr("DEPARTMENT_LEVEL_NAME", OrgUserType.getCodeName(deptLevelCode));

        if(!Strings.isNullOrEmpty(beanMap.getDepartmentSimpleName())){
            dynaBean.setStr("DEPARTMENT_SIMPLE_NAME", beanMap.getDepartmentSimpleName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getDepartmentTelephone())){
            dynaBean.setStr("DEPARTMENT_TELEPHONE", beanMap.getDepartmentTelephone());
        }
        if(!Strings.isNullOrEmpty(beanMap.getDepartmentAddress())){
            dynaBean.setStr("DEPARTMENT_ADDRESS", beanMap.getDepartmentAddress());
        }
        if(!Strings.isNullOrEmpty(beanMap.getDepartmentRemark())){
            dynaBean.setStr("DEPARTMENT_REMARK", beanMap.getDepartmentRemark());
        }
        if(!Strings.isNullOrEmpty(beanMap.getDepartmentFuncDesc())){
            dynaBean.setStr("DEPARTMENT_FUNC_DESC", beanMap.getDepartmentFuncDesc());
        }
        if(!Strings.isNullOrEmpty(beanMap.getDepartmentMajorId())){
            dynaBean.setStr("DEPARTMENT_MAJOR_ID", beanMap.getDepartmentMajorId());
            dynaBean.setStr("DEPARTMENT_MAJOR_NAME", beanMap.getDepartmentMajorName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getDepartmentMonitordeptId())){
            dynaBean.setStr("DEPARTMENT_MONITORDEPT_ID", beanMap.getDepartmentMonitordeptId());
            dynaBean.setStr("DEPARTMENT_MONITORDEPT_NAME", beanMap.getDepartmentMonitordeptName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getSuperiorDepartment())){
            dynaBean.setStr("SUPERIOR_DEPARTMENT", beanMap.getSuperiorDepartment());
        }
        dynaBean.setStr("SY_COMPANY_ID", SyncParserUserUtil.checkIsNullOrEmpty("SY_COMPANY_ID",beanMap.getSyCompanyId()));
        dynaBean.setStr("SY_COMPANY_NAME", SyncParserUserUtil.checkIsNullOrEmpty("SY_COMPANY_NAME",beanMap.getSyCompanyName()));
        return dynaBean;
    }

    /***
     *构建用户数据 基本信息
     * @param beanMap
     * @return
     * @throws DepartmentException
     */
    public static DynaBean buildDynaBean(SyncUser beanMap){
        DynaBean dynaBean = new DynaBean("JE_RBAC_DEPTUSER",false);
        dynaBean.set("USER_EMPLOYEE_STATUS", "1");
        dynaBean.setStr("JE_RBAC_USER_ID", SyncParserUserUtil.checkIsNullOrEmpty("JE_RBAC_USER_ID",beanMap.getUserId()));
        dynaBean.setStr("USER_NAME",SyncParserUserUtil.checkIsNullOrEmpty("USER_NAME",beanMap.getUserName()));
        dynaBean.setStr("USER_CODE", SyncParserUserUtil.checkIsNullOrEmpty("USER_CODE",beanMap.getUserCode()));
        if(!Strings.isNullOrEmpty(beanMap.getUserSexCode())){
            dynaBean.setStr("USER_SEX_CODE", beanMap.getUserSexCode());
            dynaBean.setStr("USER_SEX_NAME", beanMap.getUserSexCode().equals("MAN")?"男":"女");
        }
        dynaBean.setStr("USER_PHONE", beanMap.getUserPhone());
        dynaBean.setStr("USER_MAIL", beanMap.getUserMail());
        if(!Strings.isNullOrEmpty(beanMap.getUserAge())){
            dynaBean.setStr("USER_AGE", beanMap.getUserAge());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserBirth())){
            dynaBean.setStr("USER_BIRTH", beanMap.getUserBirth());
        }
//        dynaBean.setStr("USER_ENTRY_DATE", SyncParserUserUtil.checkIsNullOrEmpty("USER_ENTRY_DATE",beanMap.getUserEntryDate()));
        if(!Strings.isNullOrEmpty(beanMap.getUserEntryDate())){
            dynaBean.setStr("USER_ENTRY_DATE", beanMap.getUserEntryDate());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserLeaveDate())){
            dynaBean.setStr("USER_LEAVE_DATE", beanMap.getUserLeaveDate());
        }
        if(!Strings.isNullOrEmpty( beanMap.getUserPhoto())){
            dynaBean.setStr("USER_PHOTO", beanMap.getUserPhoto());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserAvatar())){
            dynaBean.setStr("USER_AVATAR", beanMap.getUserAvatar());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserCardnum())){
            dynaBean.setStr("USER_CARDNUM", beanMap.getUserCardnum());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserNativePlace())){
            dynaBean.setStr("USER_NATIVE_PLACE", beanMap.getUserNativePlace());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserEducationCode())){
            dynaBean.setStr("USER_EDUCATION_CODE", beanMap.getUserEducationCode());
            dynaBean.setStr("USER_EDUCATION_NAME", beanMap.getUserEducationName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserPostCode())){
            dynaBean.setStr("USER_POST_CODE", beanMap.getUserPostCode());
            dynaBean.setStr("USER_POST_NAME", beanMap.getUserPostName());
        }
        dynaBean.setStr("SY_ORG_ID", OrgType.DEVELOP_ORG_ID.getCode());
        if(!Strings.isNullOrEmpty(beanMap.getUserMonitordeptId())){
            dynaBean.setStr("USER_MONITORDEPT_ID", beanMap.getUserMonitordeptId());
            dynaBean.setStr("USER_MONITORDEPT_NAME", beanMap.getUserMonitordeptName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserNowaddress())){
            dynaBean.setStr("USER_ADDRESS", beanMap.getUserNowaddress());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserStation())){
            dynaBean.setStr("USER_STATION", beanMap.getUserStation());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserJobnum())){
            dynaBean.setStr("USER_JOBNUM", beanMap.getUserJobnum());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserNowaddress())){
            dynaBean.setStr("USER_NOWADDRESS", beanMap.getUserNowaddress());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserPoliticalCode())){
            dynaBean.setStr("USER_POLITICAL_CODE", beanMap.getUserPoliticalCode());
            dynaBean.setStr("USER_POLITICAL_NAME", beanMap.getUserPoliticalName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserContactorName())){
            dynaBean.setStr("USER_CONTACTOR_NAME", beanMap.getUserContactorName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserContactorPhone())){
            dynaBean.setStr("USER_CONTACTOR_PHONE", beanMap.getUserContactorPhone());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserTelephone())){
            dynaBean.setStr("USER_TELEPHONE", beanMap.getUserTelephone());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserHouseholdCode())){
            dynaBean.setStr("USER_HOUSEHOLD_CODE", beanMap.getUserHouseholdCode());
            dynaBean.setStr("USER_HOUSEHOLD_NAME", beanMap.getUserHouseholdName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserNationCode())){
            dynaBean.setStr("USER_NATION_CODE", beanMap.getUserNationCode());
            dynaBean.setStr("USER_NATION__NAME", beanMap.getUserNationName());
        }
        if(!Strings.isNullOrEmpty(beanMap.getUserNationalityCode())) {
            dynaBean.setStr("USER_NATIONALITY_CODE", beanMap.getUserNationalityCode());
            dynaBean.setStr("USER_NATIONALITY_NAME", beanMap.getUserNationalityName());
        }

        //部门用户关联表
        dynaBean.setStr("JE_RBAC_DEPARTMENT_ID",  SyncParserUserUtil.checkIsNullOrEmpty("JE_RBAC_DEPARTMENT_ID",beanMap.getDepartmentId()));
        dynaBean.setStr("SY_COMPANY_ID",  SyncParserUserUtil.checkIsNullOrEmpty("SY_COMPANY_ID",beanMap.getSyCompanyId()));
        dynaBean.setStr("SY_COMPANY_NAME",  SyncParserUserUtil.checkIsNullOrEmpty("SY_COMPANY_NAME",beanMap.getSyCompanyName()));
        dynaBean.setStr("SY_COMPANY_CODE",  SyncParserUserUtil.checkIsNullOrEmpty("SY_COMPANY_CODE",beanMap.getSyCompanyCode()));
        dynaBean.setStr("SY_GROUP_COMPANY_ID", beanMap.getSyGroupCompanyId());
        dynaBean.setStr("SY_GROUP_COMPANY_NAME", beanMap.getSyGroupCompanyName());
        if(!Strings.isNullOrEmpty(beanMap.getDeptuserDirectleaderId())){
            dynaBean.setStr("DEPTUSER_DIRECTLEADER_ID", beanMap.getDeptuserDirectleaderId());
            dynaBean.setStr("DEPTUSER_DIRECTLEADER_NAME", beanMap.getDeptuserDirectleaderName());
        }
        String sfzgCode = beanMap.getDeptuserSfzgCode();
        if(Strings.isNullOrEmpty(sfzgCode) || (!"1".equals(sfzgCode) && !"0".equals(sfzgCode))){
            throw new CompanyException("该数据："+beanMap.getUserName()+" DEPTUSER_SFZG_CODE 参数设置异常！");
        }
        dynaBean.setStr("DEPTUSER_SFZG_CODE", sfzgCode);
        dynaBean.setStr("DEPTUSER_SFZG_NAME", sfzgCode.equals("1")? OrgUserType.YESORNO_TRUE.getName():OrgUserType.YESORNO_FALSE.getName());
        return dynaBean;
    }
}
