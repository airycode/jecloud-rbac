/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.user.special;

public enum SpecialUserParserEnum {

    /**
     * 当前登录人
     */
    LOGINED_USER("当前登录人", "1"),
    /**
     * 直接领导
     */
    DIRECT_LEADER("直接领导", "2"),
    /**
     * 部门领导
     */
    DEPT_LEADER("部门领导", "3"),
    /**
     * 部门监管领导
     */
    DEPT_MONITOR_LEADER("监管部门领导", "4"),
    /**
     * 流程启动人
     */
    STARTER_USER("流程启动人", "5"),
    /**
     * 指派人
     */
    TASK_ASSGINE("指派人", "6"),
    /**
     * 指派人直属领导
     */
    TASK_ASSGINE_HEAD("指派人直属领导", "7"),
    /**
     * 前置任务指派人
     */
    PREV_ASSIGN_USER("前置任务指派人", "8"),
    /**
     * 前置任务指派人领导
     */
    PREV_ASSIGN_USER_DIRECT_LEADER("前置任务指派人直接领导", "9"),
    /**
     * 本部门内人员
     */
    DEPT_USERS("本部门人员", "10"),
    /**
     * 本部门含子部门人员
     */
    DEPT_ALL_USERS("本部门（包含子部门）人员", "11"),
    /**
     * 监管部门内人员
     */
    DEPT_MONITOR_USERS("监管部门内人员", "12"),
    /**
     * 本公司领导
     */
    COMPANY_LEADERS("本公司领导", "13"),
    /**
     * 本公司监管领导
     */
    COMPANY_MONITOR_LEADERS("本公司监管领导", "14"),
    /**
     * 所在子公司（展示组织结构）
     */
    SUBSIDIARY("所在子公司（展示组织结构）", "15");


    private String desc;

    private String code;

    public String getDesc() {
        return desc;
    }

    public String getCode() {
        return code;
    }

    SpecialUserParserEnum(String desc, String code) {
        this.desc = desc;
        this.code = code;
    }

    public static SpecialUserParserEnum getType(String type) {
        if (LOGINED_USER.name().equalsIgnoreCase(type)) {
            return LOGINED_USER;
        } else if (DIRECT_LEADER.name().equalsIgnoreCase(type)) {
            return DIRECT_LEADER;
        } else if (DEPT_LEADER.name().equalsIgnoreCase(type)) {
            return DEPT_LEADER;
        } else if (DEPT_MONITOR_LEADER.name().equalsIgnoreCase(type)) {
            return DEPT_MONITOR_LEADER;
        } else if (STARTER_USER.name().equalsIgnoreCase(type)) {
            return STARTER_USER;
        } else if (TASK_ASSGINE.name().equalsIgnoreCase(type)) {
            return TASK_ASSGINE;
        } else if (TASK_ASSGINE_HEAD.name().equalsIgnoreCase(type)) {
            return TASK_ASSGINE_HEAD;
        } else if (PREV_ASSIGN_USER.name().equalsIgnoreCase(type)) {
            return PREV_ASSIGN_USER;
        } else if (PREV_ASSIGN_USER_DIRECT_LEADER.name().equalsIgnoreCase(type)) {
            return PREV_ASSIGN_USER_DIRECT_LEADER;
        } else if (DEPT_USERS.name().equalsIgnoreCase(type)) {
            return DEPT_USERS;
        } else if (DEPT_ALL_USERS.name().equalsIgnoreCase(type)) {
            return DEPT_ALL_USERS;
        } else if (DEPT_MONITOR_USERS.name().equalsIgnoreCase(type)) {
            return DEPT_MONITOR_USERS;
        } else if (COMPANY_LEADERS.name().equalsIgnoreCase(type)) {
            return COMPANY_LEADERS;
        } else if (COMPANY_MONITOR_LEADERS.name().equalsIgnoreCase(type)) {
            return COMPANY_MONITOR_LEADERS;
        } else if (SUBSIDIARY.name().equalsIgnoreCase(type)) {
            return SUBSIDIARY;
        }
        return null;
    }


}
