/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.user.special;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.util.SecurityUserHolder;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.rpc.workflow.util.WorkFlowParserUserUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 直属领导解析（完成）
 */
public class DirectLeadershipParser extends AbstractSpecialUserParser {

    @Override
    public Boolean checkUser(DynaBean dynaBean, String accountDeptId) {
        String orgId = dynaBean.getStr("SY_ORG_ID");
        //不是部门直接返回
        if (!Strings.isNullOrEmpty(orgId) && orgId.equals("systemdepartment")) {
            String deptId = dynaBean.getStr("ACCOUNTDEPT_DEPT_ID");
            String userId = dynaBean.getStr("USER_ASSOCIATION_ID");
            //账号部门信息 唯一信息
            DynaBean user = findDeptUserByUserIdAndDeptId(userId, deptId);

            String leaderId = user.getStr("DEPTUSER_DIRECTLEADER_ID");
            List<DynaBean> leaders = findUserByUserId(leaderId);
            if (leaders == null || leaders.size() == 0) {
                return false;
            }
            //直属领导有且只有一个
            DynaBean leader = leaders.get(0);
            DynaBean leaderAccount = findVacCountDeptUserByUserIdAndDpeId(leader.getStr("JE_RBAC_USER_ID"),
                    leader.getStr("JE_RBAC_DEPARTMENT_ID"));
            if (userId.equals(leaderAccount.getStr("JE_RBAC_ACCOUNTDEPT_ID"))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<DynaBean> getUserInfo(String userId, Boolean addOwn) {
        DynaBean dynaBean = findVacCountDeptUserById(userId);
        if(dynaBean == null ){
            return null;
        }
        String orgId = dynaBean.getStr("SY_ORG_ID");
        //不是部门直接返回
        if (!Strings.isNullOrEmpty(orgId) && orgId.equals("systemdepartment")) {
            String deptId = dynaBean.getStr("ACCOUNTDEPT_DEPT_ID");
            String associationId = dynaBean.getStr("USER_ASSOCIATION_ID");
            DynaBean user = findDeptUserByUserIdAndDeptId(associationId, deptId);
            String leaderId = user.getStr("DEPTUSER_DIRECTLEADER_ID");
            List<DynaBean> leaders = findUserByUserId(leaderId);
            if (leaders == null || leaders.size() == 0) {
                return null;
            }
            DynaBean leader = leaders.get(0);
            DynaBean leaderAccount = findVacCountDeptUserByUserIdAndDpeId(leader.getStr("JE_RBAC_USER_ID"),
                    leader.getStr("JE_RBAC_DEPARTMENT_ID"));
            if (!addOwn) {
                StringBuilder sb = new StringBuilder();
                sb.append(" and ").append("(").append(" USER_ASSOCIATION_ID = '").append(leader.getStr("JE_RBAC_USER_ID")).append("'")
                        .append(" and ").append("ACCOUNTDEPT_DEPT_ID = '" + leader.getStr("JE_RBAC_DEPARTMENT_ID") + "'")
                        .append(")");
                sb.append(" and ").append("(").append(" JE_RBAC_ACCOUNTDEPT_ID != '").append(SecurityUserHolder.getCurrentAccount().getDeptId()).append("'").append(")");
                List<DynaBean> accountDeptByUserIdAndDpeId = findAccountDeptByUserIdAndDpeId(sb.toString());
                if (!accountDeptByUserIdAndDpeId.isEmpty()) {
                    leaderAccount = accountDeptByUserIdAndDpeId.get(0);
                }
            }
            List<DynaBean> list = new ArrayList<>();
            list.add(leaderAccount);
            return list;
        }
        return null;
    }
}
