/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.syncUser.util;

import com.je.rbac.model.syncdata.SyncCompany;
import com.je.rbac.model.syncdata.SyncDepartment;
import com.je.rbac.model.syncdata.SyncRole;
import com.je.rbac.model.syncdata.SyncUser;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamEx{
    //过滤重复数据
    public static  Predicate distinctByKey (Function keyExtractor){
        Map seen = new ConcurrentHashMap<>();
        return t->seen.putIfAbsent(keyExtractor.apply(t),Boolean.TRUE)==null;
    }
    //获取重复数据
    public static  Predicate getdistinctByKey (Function keyExtractor){
        Map seen = new ConcurrentHashMap<>();
        return t->seen.putIfAbsent(keyExtractor.apply(t),Boolean.TRUE)!=null;
    }

    /** List<Map<String,String>> 根据单个属性获取重复值*/
    public static long checkCFNum(List<Map<String,String>> list, String key){
        long count = (Long) list.stream().filter(StreamEx.getdistinctByKey(map->((Map) map).get(key))).collect(Collectors.counting());
        return count;
    }

    /** List<SyncCompany> 根据单个属性获取重复值*/
    public static long checkCFNumByCompanyId(List<SyncCompany> list){
        List repeatCode = list.stream().collect(Collectors.groupingBy(SyncCompany::getCompanyId, Collectors.counting())).entrySet().stream().filter(entry -> entry.getValue() > 1).map(Map.Entry::getKey).collect(Collectors.toList());
        return repeatCode.size();
    }

    /** List<SyncDepartment> 根据单个属性获取重复值*/
    public static long checkCFNumByDeptId(List<SyncDepartment> list){
        List repeatCode = list.stream().collect(Collectors.groupingBy(SyncDepartment::getDepartmentId, Collectors.counting())).entrySet().stream().filter(entry -> entry.getValue() > 1).map(Map.Entry::getKey).collect(Collectors.toList());
        return repeatCode.size();
    }

    /** List<SyncRole> 根据单个属性获取重复值*/
    public static long checkCFNumByRoleId(List<SyncRole> list){
        List repeatCode = list.stream().collect(Collectors.groupingBy(SyncRole::getRoleId, Collectors.counting())).entrySet().stream().filter(entry -> entry.getValue() > 1).map(Map.Entry::getKey).collect(Collectors.toList());
        return repeatCode.size();
    }

    /** List<SyncUser> 根据单个属性获取重复值*/
    public static long checkCFNumByUserId(List<SyncUser> list){
        List repeatCode = list.stream().collect(Collectors.groupingBy(SyncUser::getUserId, Collectors.counting())).entrySet().stream().filter(entry -> entry.getValue() > 1).map(Map.Entry::getKey).collect(Collectors.toList());
        return repeatCode.size();
    }
}