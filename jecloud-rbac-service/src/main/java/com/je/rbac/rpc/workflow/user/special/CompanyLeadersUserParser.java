/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.user.special;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.SecurityUserHolder;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * 公司领导
 */
public class CompanyLeadersUserParser extends AbstractSpecialUserParser {

    @Override
    public Boolean checkUser(DynaBean dynaBean, String userId) {
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        String orgId = dynaBean.getStr("SY_ORG_ID");
        if (!Strings.isNullOrEmpty(orgId) && orgId.equals("systemdepartment")) {
            //公司信息
            DynaBean companyBean = metaService.selectOne("JE_RBAC_COMPANY", ConditionsWrapper.builder().
                    eq("JE_RBAC_COMPANY_ID", dynaBean.get("SY_COMPANY_ID")));
            //公司领导人员信息 用户id
            String major = companyBean.getStr("COMPANY_MAJOR_ID");
            if (Strings.isNullOrEmpty(major)) {
                return false;
            }

            String[] majorArray = major.split(",");
            List<DynaBean> userBeanList = metaService.select("JE_RBAC_VDEPTUSER", ConditionsWrapper.builder().
                    in("JE_RBAC_USER_ID", majorArray));
            StringBuilder sb = new StringBuilder();
            for (DynaBean user : userBeanList) {
                if (sb.length() > 0) {
                    sb.append(" or ");
                } else {
                    sb.append(" and ");
                }
                sb.append("(").append(" USER_ASSOCIATION_ID = '" + user.getStr("JE_RBAC_USER_ID") + "'").append(" and ").append("ACCOUNTDEPT_DEPT_ID = '" + user.getStr("JE_RBAC_DEPARTMENT_ID") + "'")
                        .append(")");
            }
            //账号部门视图信息  唯一信息
            if (sb.length() > 0) {
                List<DynaBean> accountDept = findAccountDeptByUserIdAndDpeId(sb.toString());
                for (DynaBean bean : accountDept) {
                    if (userId.equals(bean.getStr("JE_RBAC_ACCOUNTDEPT_ID"))) {
                        return true;
                    }
                }
            }

        }
        return false;
    }

    @Override
    public List<DynaBean> getUserInfo(String userId, Boolean addOwn) {
        DynaBean dynaBean = findVacCountDeptUserById(userId);
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        String orgId = dynaBean.getStr("SY_ORG_ID");
        if (!Strings.isNullOrEmpty(orgId) && orgId.equals("systemdepartment")) {
            List<DynaBean> companyBeanList = new ArrayList<>();
            //公司信息
            DynaBean companyBean = metaService.selectOne("JE_RBAC_COMPANY", ConditionsWrapper.builder().
                    eq("JE_RBAC_COMPANY_ID", dynaBean.get("SY_COMPANY_ID")));
            companyBeanList.add(companyBean);
            //公司领导人员信息
            String major = companyBean.getStr("COMPANY_MAJOR_ID");
            if (Strings.isNullOrEmpty(major)) {
                return null;
            }

            String[] majorArray = major.split(",");
            List<DynaBean> userBeanList = metaService.select("JE_RBAC_VDEPTUSER", ConditionsWrapper.builder().
                    in("JE_RBAC_USER_ID", majorArray));
            List<DynaBean> accUsers = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            for (DynaBean user : userBeanList) {
                if (sb.length() > 0) {
                    sb.append(" or ");
                } else {
                    sb.append(" and ");
                }
                sb.append("(").append(" USER_ASSOCIATION_ID = '").append(user.getStr("JE_RBAC_USER_ID")).append("'")
                        .append(" and ").append("ACCOUNTDEPT_DEPT_ID = '" + user.getStr("JE_RBAC_DEPARTMENT_ID") + "'")
                        .append(")");
            }
            if (!addOwn && sb.length() > 0){
                sb.append(" and ").append("(").append(" JE_RBAC_ACCOUNTDEPT_ID != '").append(SecurityUserHolder.getCurrentAccount().getDeptId()).append("'").append(")");
            }
            //账号部门视图信息  唯一信息
            if (sb.length() > 0) {
                accUsers = findAccountDeptByUserIdAndDpeId(sb.toString());
            }
            return accUsers;
        }
        return null;
    }


}
