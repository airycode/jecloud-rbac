/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.user.special;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.SecurityUserHolder;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.rpc.workflow.util.WorkFlowParserUserUtil;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门领导解析（完成）
 */
public class DeptLeadershipParser extends AbstractSpecialUserParser {

    @Override
    public Boolean checkUser(DynaBean dynaBean, String userId) {
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        String orgId = dynaBean.getStr("SY_ORG_ID");
        if (!Strings.isNullOrEmpty(orgId) && orgId.equals("systemdepartment")) {
            String deptId = dynaBean.getStr("ACCOUNTDEPT_DEPT_ID");
            List<DynaBean> list = metaService.select("JE_RBAC_VDEPTUSER", ConditionsWrapper.builder()
                    .eq("JE_RBAC_DEPARTMENT_ID", deptId).eq("DEPTUSER_SFZG_CODE", "1"));
            if (list.size() == 0) {
                return false;
            }
            List<String> leaderIds = list.stream().map(leader -> leader.getStr("JE_RBAC_USER_ID")).collect(Collectors.toList());
            List<DynaBean> leaderAccounts = metaService.select("JE_RBAC_VACCOUNTDEPT", ConditionsWrapper.builder()
                    .in("USER_ASSOCIATION_ID", leaderIds).eq("ACCOUNTDEPT_DEPT_ID", deptId));
            for (DynaBean leaderAccount : leaderAccounts) {
                if (userId.equals(leaderAccount.getStr("JE_RBAC_ACCOUNTDEPT_ID"))) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<DynaBean> getUserInfo(String userId, Boolean addOwn) {
        DynaBean dynaBean = findVacCountDeptUserById(userId);
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        String orgId = dynaBean.getStr("SY_ORG_ID");
        if (!Strings.isNullOrEmpty(orgId) && orgId.equals("systemdepartment")) {
            String deptId = dynaBean.getStr("ACCOUNTDEPT_DEPT_ID");
            List<DynaBean> list = metaService.select("JE_RBAC_VDEPTUSER", ConditionsWrapper.builder()
                    .eq("JE_RBAC_DEPARTMENT_ID", deptId).eq("DEPTUSER_SFZG_CODE", "1"));
            if (list.size() == 0) {
                return null;
            }
            List<String> leaderIds = list.stream().map(leader -> leader.getStr("JE_RBAC_USER_ID")).collect(Collectors.toList());
            List<DynaBean> leaderAccounts = metaService.select("JE_RBAC_VACCOUNTDEPT", ConditionsWrapper.builder()
                    .in("USER_ASSOCIATION_ID", leaderIds).eq("ACCOUNTDEPT_DEPT_ID", deptId));
            if (!addOwn) {
                leaderAccounts = metaService.select("JE_RBAC_VACCOUNTDEPT", ConditionsWrapper.builder()
                        .in("USER_ASSOCIATION_ID", leaderIds).eq("ACCOUNTDEPT_DEPT_ID", deptId)
                        .ne("JE_RBAC_ACCOUNTDEPT_ID", SecurityUserHolder.getCurrentAccount().getDeptId()));
            }
            return leaderAccounts;
        }
        return null;
    }
}
