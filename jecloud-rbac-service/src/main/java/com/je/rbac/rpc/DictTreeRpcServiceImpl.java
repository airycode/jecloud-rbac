/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.service.CommonService;
import com.je.common.base.util.StringUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.model.dd.DicInfoVo;
import com.je.rbac.service.company.RbacDepartmentService;
import com.je.rbac.service.grant.role.RbacGrantRoleService;
import com.je.rbac.service.menu.MenuService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/***
 * 数据字典自定义树形
 */
@Component("dictTreeRpcService")
public class DictTreeRpcServiceImpl {

    @Autowired
    private RbacGrantRoleService rbacGrantRoleService;

    @Autowired
    private RbacDepartmentService rbacDepartmentService;

    @Autowired
    private OrgRpcService orgRpcService;

    @Autowired
    private MenuRpcService menuRpcService;

    @Autowired
    protected CommonService commonService;
    /***
     * 角色树形
     * @param dicInfoVo
     * @return
     */
    public JSONTreeNode buildRoleTree(DicInfoVo dicInfoVo) {
        return rbacGrantRoleService.buildRoleTree(false);
    }

    /**
     * 公司部门树
     */
    public JSONTreeNode buildCompanyDeptTree(DicInfoVo dicInfoVo){
        return rbacDepartmentService.buildCompanyDeptTreeData(dicInfoVo);
    }
    /**
     * 公司部门人员树
     */
    public JSONTreeNode buildCompanyDeptUserTree(DicInfoVo dicInfoVo){
        return rbacDepartmentService.buildCompanyDeptUserTreeData(dicInfoVo);
    }
    /**
     * 角色人员树
     */
    public JSONTreeNode buildRoleUserTree(DicInfoVo dicInfoVo){
        return rbacDepartmentService.buildRoleUserTree(dicInfoVo);
    }

    /***
     * 部门树形
     * @param dicInfoVo
     * @return
     */
    public JSONTreeNode buildDeptTree(DicInfoVo dicInfoVo) {
        return  rbacDepartmentService.buildDeptTreeForDictionary(dicInfoVo);
           // return rbacDepartmentService.buildCompanyTreeData(false);
//        }
    }

    /***
     * 机构树形
     * @param dicInfoVo
     * @return
     */
    public JSONTreeNode buildOrgTree(DicInfoVo dicInfoVo) {
        return orgRpcService.buildOrgTree();
    }


    /**
     * 菜单树
     * @param dicInfoVo
     * @return
     */
    public JSONTreeNode buildMenuTree(DicInfoVo dicInfoVo) {
        Query query = new Query();
        if(StringUtil.isNotEmpty(dicInfoVo.getWhereSql())){
            query.addCustoms(dicInfoVo.getWhereSql());
        }
        if(StringUtil.isNotEmpty(dicInfoVo.getOrderSql())){
            JSONArray jsonArray = JSON.parseArray(dicInfoVo.getOrderSql());
            if(jsonArray!=null&&jsonArray.size()>0){
                for(int i=0;i<jsonArray.size();i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    query.addOrder(jsonObject.getString("code"),jsonObject.getString("type"));
                }
            }
        }
        ConditionsWrapper conditionsWrapper = query.buildWrapper();
        if(StringUtil.isNotEmpty(query.buildOrder())){
            if(conditionsWrapper.getSql().toUpperCase().contains("ORDER BY")){
                conditionsWrapper.apply(","+query.buildOrder());
            }else{
                conditionsWrapper.apply(" ORDER BY "+query.buildOrder());
            }
        }
        String node =ConstantVars.TREE_ROOT;
        commonService.buildProductQuery(query);
        return menuRpcService.buildMenuTree(query,node);
    }
}
