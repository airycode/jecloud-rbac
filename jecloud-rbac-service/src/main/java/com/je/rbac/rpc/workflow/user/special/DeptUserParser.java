/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.user.special;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.SecurityUserHolder;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.rpc.workflow.util.WorkFlowParserUserUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 本部门内人员解析
 */
public class DeptUserParser extends AbstractSpecialUserParser {

    @Override
    public Boolean checkUser(DynaBean dynaBean, String userId) {
        return true;
    }

    @Override
    public List<DynaBean> getUserInfo(String userId, Boolean addOwn) {
        DynaBean dynaBean = findVacCountDeptUserById(userId);
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        String orgId = dynaBean.getStr("SY_ORG_ID");
        if (!Strings.isNullOrEmpty(orgId) && orgId.equals("systemdepartment")) {
            List<DynaBean> userBeanList = metaService.select("JE_RBAC_VACCOUNTDEPT",
                    ConditionsWrapper.builder().eq("ACCOUNTDEPT_DEPT_ID",
                            dynaBean.get("ACCOUNTDEPT_DEPT_ID")));
            if (!addOwn) {
                userBeanList = metaService.select("JE_RBAC_VACCOUNTDEPT",
                        ConditionsWrapper.builder().eq("ACCOUNTDEPT_DEPT_ID", dynaBean.get("ACCOUNTDEPT_DEPT_ID"))
                                .ne("JE_RBAC_ACCOUNTDEPT_ID", SecurityUserHolder.getCurrentAccount().getDeptId()));
            }
            return userBeanList;
        }
        return null;
    }
}
