/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.StringUtil;
import com.je.common.base.util.TreeUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.exception.RoleException;
import com.je.rbac.service.IconType;
import com.je.rbac.service.grant.DevelopProductService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RpcSchema(schemaId = "orgRpcService")
public class OrgRpcServiceImpl implements OrgRpcService {

    @Autowired
    private MetaService metaService;

    @Autowired
    private BeanService beanService;

    @Override
    public JSONTreeNode buildOrgTree() {
        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        DynaBean table = beanService.getResourceTable("JE_RBAC_ORG");
        List<DynaBean> columns = (List<DynaBean>) table.get(BeanService.KEY_TABLE_COLUMNS);
        JSONTreeNode template = beanService.buildJSONTreeNodeTemplate(columns);
        List<DynaBean> orgListBean = metaService.select("JE_RBAC_ORG",ConditionsWrapper.builder().eq("SY_STATUS", "1"));
        if(null == orgListBean){
            return rootNode;
        }
        for (DynaBean eachBean : orgListBean) {
            JSONTreeNode node = new JSONTreeNode();
                node.setId(eachBean.getStr("JE_RBAC_ORG_ID"));
                node.setCode(eachBean.getStr("ORG_CODE"));
                node.setText(eachBean.getStr("ORG_NAME"));
                node.setNodeType("LEAF");
                node.setNodeInfo(eachBean.getStr("ORG_CODE"));
                node.setNodeInfoType("org");
                node.setIcon(IconType.TCONTYPE_ORG.getVal());
                node.setParent(rootNode.getId());
                node.setChecked(false);
                node.setLayer(rootNode.getLayer() + 1);
                node.setBean(eachBean.getValues());
            rootNode.getChildren().add(node);
        }
        return rootNode;
    }
}